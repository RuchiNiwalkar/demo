package Company;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import Group.TestBase;

public class CompanyPage extends TestBase {

	public CompanyPage(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}


	//XPaths for  Company LOGIN BDO
	/* Locator for Username for company */
	@FindBy(how = How.XPATH, using="//div[@class='row wrapper pos-rel']/div/div/div[@class='col-md-10']/form/div[2]/div[1]/div/input[@name='LoginId']")
	WebElement UserNameBDO;


	/* Locator for Password for company */
	@FindBy(how = How.XPATH, using=".//*[@id='Password']")
	WebElement PasswordBDO;


	/* Locator for LOGIN */
	@FindBy(how = How.XPATH, using="//div[@class='col-md-12 col-12 l-h-2 text-cent']/input[@class='k-button k-danger login_button float-right' and  @value='Log In']")
	WebElement LoginBDO;

	/* Locator for New Close Functionality for company */
	@FindBy(how = How.XPATH, using="html/body/div[13]/div[1]/div/a")
	WebElement NewCloseFunctionality;
	

	//Xpaths for Company

	/* Locator for sidebar icon on the dashboard page  */
	@FindBy(how = How.XPATH, using="//span[@id='menu-toggle' and @class='toggle-left toggleSidebar']/i")
	WebElement sidebaricon;

	/* Locator for Company from the list on the dashboard page  */
	@FindBy(how = How.XPATH, using=".//*[@id='menu']/li/a[contains(text(),'Company')]")
	WebElement Company;

	/* Locator for Company page on Company page  */
	@FindBy(how = How.XPATH, using="//span[contains(text(),'Company')]")
	WebElement Verifycompany;

	/* Locator for Add Company on Company page  */
	@FindBy(how = How.XPATH, using="//button[@trigger-id='addcomP1']")
	WebElement Addcompany;


	//Xpaths for ADDING a New Company data from excel



	/* Locator for Account/Group on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addComp_account_name' and @readonly='']")
	WebElement VerifyAcc_GroupNameComp;

	/* Locator for CompanyName on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addComp_name']")
	WebElement AddCompanyName;


	/* Locator for TurnOver16_17  on Company page  */
	@FindBy(how = How.XPATH, using="//input[@id='addComp_avg_turnover' and  @class='k-textbox']")
	WebElement TurnOver16_17;


	/* Locator for Registered Email-ID  on Company page  */
	@FindBy(how = How.XPATH, using="//input[@id='addComp_email' and @class='k-textbox' and @type='email']")
	WebElement Registered_Email_id;

	/* Locator for TradeName on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addComp_tradename' and @type='text']")
	WebElement TradeName;

	/* Locator for Select State Dropwdown on Company page  */
	@FindBy(how = How.XPATH, using="//div[@id='addcompP1']/div/div/div/div/span/span/span[@aria-label='select']")
	WebElement  State_Dropdown;


	/* Locator for Select List of States from the Dropdowm State  on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addComp_main_state_listbox']/li")
	List <WebElement>  ListofStates;


	/* Locator for GSTIN on Company page  */
	@FindBy(how = How.XPATH, using="//input[@id='addComp_gstin' and @class='k-textbox' and @type='text']")
	WebElement GSTIN;


	/* Locator for PAN on  Company page  */
	@FindBy(how = How.XPATH, using="//input[@id='addComp_pan' and @class='k-textbox' and  @type='text']")
	WebElement PAN;

	/* Locator for TurnOver_April - June 2017  on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addComp_curr_turnover' and @name='addComp_curr_turnover']")
	WebElement TurnOver_AprilJune2017;

	/* Locator for Mobile  on Company page  */
	@FindBy(how = How.XPATH, using="//input[@id='addComp_mobile' and @class='k-textbox' and @type='text']")
	WebElement Mobile;


	/* Locator for Select Constitution of Business  Dropwdown on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addcompP1']/div[5]/div[2]/div/div/div/div/span/span/span[2]")
	WebElement  ConstOfBussiness;


	/* Locator for Select List of Select Constitution of Business  from Dropdowm State  on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addComp_constitution_listbox']/li")
	List <WebElement>  ListConstOfBussiness;

	/* Locator for Select City Dropwdown on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addcompP1']/div[6]/div[2]/div/div/span/span/span[2]")
	WebElement  City_District_Dropdown;

	/* Locator for Select City Dropwdown on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addcompP1']/div[6]/div[2]/div/div/span/span[@class='k-dropdown-wrap k-state-disabled']")
	WebElement  City_District_Dropdown_stateinactive;


	/* Locator for Select List of City from the Dropdowm State  on Company page  */
	@FindBy(how = How.XPATH, using="//div[@id='addComp_main_city-list']/div[@class='k-list-scroller']/ul/li")
	List <WebElement>  ListofCity_District;


	/* Locator for Select City Dropwdown on Company page  */
	@FindBy(how = How.XPATH, using="//div[@id='addComp_main_city-list']/div[@class='k-list-scroller']/ul/li[contains(text(),'Aheri')]")
	WebElement  City_Aheri;


	/* Locator for Select List of City from the Dropdowm State  on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='clientuserGrid']/tbody/tr/td[7]/div/a[2]/i[@clear-id='edit_client_sel_gstin']")
	List <WebElement>  ListofUsers;

	/* Locator for Select List of City from the Dropdowm State  on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='clientuserGrid']/tbody/tr/td[4]")
	List <WebElement>  Listofmailsid;

	/* Locator for Select List of City from the Dropdowm State  on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='clientuserGrid']/tbody/tr[7]/td")
	List <WebElement>  ListofrequiredRow;

	/* Locator for Select List of City from the Dropdowm State  on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='clientuserGrid']/tbody/tr/td[7]/div/a/i[@class='k-icon k-i-eye k-icon-20 col-1 view_client_admin_user tabsToggle' and @hide-id='deactivate']")
	List <WebElement>  ListofClientadmins;

	/* Locator for Next on Company page  */
	@FindBy(how = How.XPATH, using="//a[@id='next3' and contains(text(),'Next')]")
	WebElement  Next;

	//Xpaths for ADDRESS in ADD COMPANY

	/* Locator for Next on Company page  */
	@FindBy(how = How.XPATH, using="//a[@id='next4' and contains(text(),'Next')]")
	WebElement  NextRegistration ;


	/* Locator Add address line 1 on add address Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addComp_add_line_1' and @class='clearEdit k-textbox']")
	WebElement  address1;

	/* Locator Add address line 2 on add address Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addComp_add_line_2' and @class='clearEdit k-textbox']")
	WebElement  address2;

	/* Locator Add pincode on add address Company page  */
	@FindBy(how = How.XPATH, using="//input[@id='addComp_pincode']")
	WebElement  pincode_address;


	/* Locator Address type dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addcompP3']/div/div/div[1]/div[1]/div/div/span/span/span[@class='k-select']")
	WebElement  dropdown_address;


	/* Locator  Address type List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addComp_address_type_listbox']/li")
	List <WebElement>  ListddlAddress;

	/* Locator Premises  dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addcompP3']/div/div/div[1]/div[2]/div/div/span/span/span[@class='k-select']")
	WebElement  dropdown_Premises;


	/* Locator  Premises  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addComp_premises_listbox']/li")
	List <WebElement>  ListddlPremises;


	/* Locator States  dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addcompP3']/div/div/div[4]/div[1]/div/div/span/span/span[@class='k-select']")
	WebElement  dropdown_States;


	/* Locator  States  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addComp_state_listbox']/li")
	List <WebElement>  ListddlStates;

	/* Locator City  dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addcompP3']/div/div/div[4]/div[2]/div/div/span/span/span[@class='k-select']")
	WebElement  dropdown_City;


	/* Locator  City  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addComp_city_listbox']/li")
	List <WebElement>  ListddlCity;

	/* Locator BussinessActvty  dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addcompP3']/div/div[2]/span/span/span[@class='k-select']")
	WebElement  dropdown_BussinessActvty;


	/* Locator  BussinessActvty  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addComp_nature_listbox']/li")
	List <WebElement>  ListBussinessActvty;


	/* Locator Next_Adress on add address Company page  */
	@FindBy(how = How.XPATH, using="//a[@trigger-id='addcomP4' and contains(text(),'Next')]")
	WebElement  Next_Address;


	/* Locator GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addComp_username']")
	WebElement  GSTN_username;


	/* Locator GSTN_username_1 dropdown on Add Company page  */
	@FindBy(how = How.XPATH, using=".//div[@id='addcompP4']/div[2]/div/span[@class='k-widget k-dropdown k-header form-control']/span/span[@class='k-select']")
	WebElement  GSTN_username1;

	/* Locator GSTN_username_2 dropdown on Add Company page  */
	@FindBy(how = How.XPATH, using=".//div[@id='addcompP4']/div[3]/div/span[@class='k-widget k-dropdown k-header form-control']/span/span[@class='k-select']")
	WebElement  GSTN_username2;


	/* Locator GSTN_users on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addComp_auth1_email_listbox']/li")
	List <WebElement>  GSTN_users;


	/* Locator Verify Name on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//span[@id='addComp_auth1_name']")
	WebElement  Name_GSTN;

	/* Locator Verify Number on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//span[@id='addComp_auth1_mobile']")
	WebElement  Number_GSTN;


	/* Locator Verify PAN on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//span[@id='addComp_auth1_pan']")
	WebElement  PAN_GSTN;




	/* Locator NEXT on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//a[@trigger-id='addcomP5' and contains(text(),'Next')]")
	WebElement  Next_GSTN;


	/* Locator GSTR3B_march2018 of GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addcompP5']/div/div[2]/span/span/span[@class='k-select']")
	List <WebElement>  GSTR3B_List;

	/* Locator List Compile   GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//li[contains(text(),'Compile')]")
	List <WebElement>  GSTR3B_Compile;

	//div[@class='k-animation-container']/div[contains(@id,'editComp_3b_input_type')]
	/* Locator Next on GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using="//a[@trigger-id='addcomP6' and contains(text(),'Next')]")
	WebElement Next_GSTR3B;

	/* Locator HSN Summary checkbox on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addcompP6']/div/div/input[@id='addComp_out_HSN_config']")
	WebElement HSN_checkbox;

	/* Locator ITR checkbox on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addcompP6']/div/div/input[@id='addComp_in_ITC_config']")
	WebElement ITC_checkbox;

	/* Locator Save of Add Company page  */
	@FindBy(how = How.XPATH, using="//button[@class='next-step addCompany k-button k-success float-right' and contains(text(),'Save')]")
	WebElement Save_AddCompany;

	/* Locator Company successfully created on Add Company page  */
	@FindBy(how = How.XPATH, using="//button[@class='k-button k-primary' and contains(text(),'OK')]")
	WebElement Company_Created;

	/* Locator Company successfully creation List on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='company']/div/span")
	List <WebElement> CompCreatnList;

	/* Locator  for VIEW Company List on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='company']/div/div/div/div[1]/a")
	List <WebElement> CompCreatnListView;

	/* Locator  for EDIT Company List on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='company']/div/div/div/div[2]/a")
	List <WebElement> CompCreatnListEdit;




	//Xpaths for View Company

	/* Locator to view AccountName/Group on View Company page */
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_account_name']")
	WebElement View_Acc_Grp;

	/* Locator to view CompanyName on View Company page */
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_name']")
	WebElement View_CompanyName;


	/*  Locator to view Turnover on View Company page */
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_avg_turnover']")
	WebElement View_turnover;


	/* Locator to view Email-ID on View Company page */
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_email']")
	WebElement View_emailid;


	/*Locator to view State on View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_main_state']")
	WebElement View_State ;


	/* Locator to view GSTIN on View Company page  */
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_gstin']")
	WebElement View_GSTIN ;

	/* Locator to view Pan on View Company page */
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_pan']")
	WebElement View_Pan ;

	/*Locator to view TurnoverApr-June on View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_curr_turnover']")
	WebElement View_turnoverApr_June;

	/*Locator to view Mobile on View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_mobile']")
	WebElement View_mobile;


	/* Locator to view Constitution on View Company page */
	@FindBy(how = How.XPATH, using=".//*[@id='viewComp_constitution']")
	WebElement View_Constitution;


	/* Locator to view City on View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_main_city']")
	WebElement View_City;

	/* Locator to Address_Tab on View Company page*/
	@FindBy(how = How.XPATH, using=".//a[@id='comViewP3' and contains(text(),'Address')]")
	WebElement Address_Tab;



	/* Locator to view Address1 on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_add_line_1']")
	WebElement View_Add1;

	/* Locator to view Address2 on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_add_line_2']")
	WebElement View_Add2;


	/*Locator to view Pincode on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_pincode']")
	WebElement View_Pincode;

	/*Locator to view Addresstype on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_address_type']")
	WebElement View_Addresstype;

	/*Locator to view Premises on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_premises']")
	WebElement View_Premises;

	/*Locator to view State on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_state']")
	WebElement View_StateAddress;

	/*Locator to view City on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_city']")
	WebElement View_CityAddress;

	/*Locator to view Nature of Business Activity on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_nature']")
	WebElement View_BA;

	/*Locator to click on GSTIN_User Tab on GST User Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//a[@id='comViewP4' and contains(text(),'GST User')]")
	WebElement View_GSTIN_User;


	/*Locator to view GSTIN_Username on GST User Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_username']")
	WebElement View_GSTIN_Username;


	/*Locator to view GSTIN_Username on GST User Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_auth1_email']")
	WebElement View_GSTIN_Email_ID;


	/* Locator Verify Name on  GSTN_username on View Company page  */
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_auth1_name']")

	WebElement  View_Name_GSTN;

	/* Locator Verify Number on  GSTN_username on View Company page  */
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_auth1_mobile']")
	WebElement  View_Number_GSTN;


	/* Locator Verify PAN on  GSTN_username on View Company page  */
	@FindBy(how = How.XPATH, using=".//span[@id='viewComp_auth1_pan']")
	WebElement  View_PAN_GSTN;


	/* Locator GSTR3B on View Company page  */
	@FindBy(how = How.XPATH, using=".//a[@id='comViewP5' and contains(text(),'GSTR3B')]")
	WebElement  View_GSTRR3B;

	
	/* Locator List of GSTR3B Compile on View Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='viewComp_gstr3B_config']/div/div/span")
	List <WebElement>  List_GSTRR3BCompile;
	
	
	/* Locator List of GSTR3B Compile on View Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='viewComp_gstr3B_config']/div/div/span")
	List <WebElement>  List_GSTRR3BDirectInput;
	
	/* Locator Other Configuration on View Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='comViewP6' and contains(text(),'Other Configuration')]")
	WebElement  View_OtherConfig;

	/* Locator Type in Other Configuration on HSN Summary VALUE on View Company page  */
	@FindBy(how = How.XPATH, using="//label[contains(text(),'Upload HSN Summary Separately:')]/parent::div/span[@id='viewComp_out_HSN']")
	WebElement  View_HSNSummary_Value;


	/* Locator Type in Other Configuration on ITC VALUE on View Company page  */
	@FindBy(how = How.XPATH, using="//label[contains(text(),'Allow ITC Available Upload:')]/parent::div/span[@id='viewComp_in_itcUpload']")
	WebElement  View_ITC_Value;

	/* Locator CLOSE on View Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='compViewModal']/div/div/div/button[contains(text(),'Close')]")
	WebElement  View_Close;




	//Xpaths for EDIT Company

	/* Locator  for EDIT Company List on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='company']/div/div/div/div[2]/a")
	List <WebElement> CompListEdit;

	/*
	 * Sample
	 */
	/* Locator EDIT  Check non-editable Field - AccountGrp_Name Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editComp_account_name' and @readonly='']")
	WebElement  Edit_AccGrp;


	/* Locator for EDIT CompanyName on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editComp_name' and @readonly='']")
	WebElement Edit_CompanyName;


	/* Locator for EDIT TurnOver16_17  on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editComp_avg_turnover']")
	WebElement Edit_TurnOver16_17;


	/* Locator for EDIT Registered Email-ID  on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editComp_email' and @type='email']")
	WebElement Edit_Registered_Email_id;

	/* Locator for EDIT  TradeName on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editComp_tradename']")
	WebElement Edit_TradeName;

	/* Locator for EDIT Select State Dropwdown on Company page  */
	@FindBy(how = How.XPATH, using="//div[@id='editcompP1']/div/div/div/div/span/span")
	WebElement  Edit_State_Present;

	/* Locator for EDIT GSTIN on Company page  */
	@FindBy(how = How.XPATH, using="//input[@id='editComp_gstin' and @class='k-textbox' and @type='text' and @readonly='']")
	WebElement Edit_GSTIN;


	/* Locator for EDIT PAN on  Company page  */
	@FindBy(how = How.XPATH, using="//input[@id='editComp_pan' and @class='k-textbox' and  @type='text' and @readonly='']")
	WebElement Edit_PAN;

	/* Locator for EDIT  TurnOver_April - June 2017  on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editComp_curr_turnover']")
	WebElement Edit_TurnOver_AprilJune2017;

	/* Locator for EDIT Mobile  on Company page  */
	@FindBy(how = How.XPATH, using="//input[@id='editComp_mobile' and @class='k-textbox' and @type='text']")
	WebElement Edit_Mobile;


	/* Locator for EDIT Select Constitution of Business  Dropwdown on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editComp_constitution' and @readonly='']")
	WebElement  Edit_ConstOfBussiness;


	/* Locator for EDIT Select City Dropwdown on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editcompP1']/div[6]/div[2]/div/div/span/span/span[2]")
	WebElement  Edit_City_District_Dropdown;


	/* Locator for EDIT Select List of City from the Dropdowm State  on Company page  */
	@FindBy(how = How.XPATH, using="//div[@id='editComp_main_city-list']/div[@class='k-list-scroller']/ul/li")
	List <WebElement>  Edit_ListofCity_District;


	/* Locator for Address on Edit Company Details on Company page  */
	@FindBy(how = How.XPATH, using=".//a[@id='editcomP3']")
	WebElement  Edit_Address_Tab;


	/* Locator EDIT Add address line 1 on add address Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editComp_add_line_1']")
	WebElement  Edit_address1;

	/* Locator EDIT Add address line 2 on add address Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editComp_add_line_2' and @class='clearEdit k-textbox']")
	WebElement  Edit_address2;

	/* Locator EDIT Add pincode on add address Company page  */
	@FindBy(how = How.XPATH, using="//input[@id='editComp_pincode']")
	WebElement   Edit_pincode_address;


	/* Locator EDIT Address type dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editcompP3']/div/div/div[1]/div[1]/div/div/span/span/span[@class='k-select']")
	WebElement   Edit_dropdown_address;


	/* Locator EDIT Address type List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editComp_address_type_listbox']/li")
	List <WebElement>   Edit_ListddlAddress;

	/* Locator EDIT Premises  dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editcompP3']/div/div/div[1]/div[2]/div/div/span/span/span[@class='k-select']")
	WebElement   Edit_dropdown_Premises;


	/* Locator EDIT Premises  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editComp_premises_listbox']/li")
	List <WebElement>   Edit_ListddlPremises;


	/* Locator EDIT States  dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editcompP3']/div/div/div[4]/div[1]/div/div/span/span/span[@class='k-select']")
	WebElement   Edit_dropdown_States;


	/* Locator EDIT States  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editComp_state_listbox']/li")
	List <WebElement>   Edit_ListddlStates;

	/* Locator EDIT City  dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editcompP3']/div/div/div[4]/div[2]/div/div/span/span/span[@class='k-select']")
	WebElement  Edit_dropdown_City;


	/* Locator EDIT City  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editComp_city_listbox']/li")
	List <WebElement>  Edit_ListddlCity;

	/* Locator EDIT BussinessActvty  dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editcompP3']/div/div[2]/span/span/span[@class='k-select']")
	WebElement  Edit_dropdown_BussinessActvty;


	/* Locator EDIT  BussinessActvty  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editComp_nature_listbox']/li")
	List <WebElement>  Edit_ListBussinessActvty;


	/* Locator EDIT  NEXT on  add address Company page  */
	@FindBy(how = How.XPATH, using="//a[@trigger-id='editcomP4' and contains(text(),'Next')]")
	WebElement  Edit_Address_next;



	/* Locator EDIT GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editComp_username']")
	WebElement  Edit_GSTN_username;


	/* Locator EDIT GSTN_username_1 dropdown on Add Company page  */
	@FindBy(how = How.XPATH, using=".//div[@id='editcompP4']/div[2]/div/span[@class='k-widget k-dropdown k-header form-control']/span/span[@class='k-select']")
	WebElement  Edit_GSTN_username1;

	/* Locator EDIT GSTN_username_2 dropdown on Add Company page  */
	@FindBy(how = How.XPATH, using=".//div[@id='editcompP4']/div[3]/div/span[@class='k-widget k-dropdown k-header form-control']/span/span[@class='k-select']")
	WebElement  Edit_GSTN_username2;


	/* Locator EDIT GSTN_users on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editComp_auth1_email_listbox']/li")
	List <WebElement>  Edit_GSTN_users;


	/* Locator EDIT Verify Name on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//span[@id='editComp_auth1_name']")
	WebElement  Edit_Name_GSTN;

	/* Locator EDIT Verify Number on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//span[@id='editComp_auth1_mobile']")
	WebElement  Edit_Number_GSTN;


	/* Locator EDIT Verify PAN on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//span[@id='editComp_auth1_pan']")
	WebElement  Edit_PAN_GSTN;


	/* Locator EDIT NEXT on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//a[@trigger-id='editcomP5' and contains(text(),'Next')]")
	WebElement  Edit_GSTUser_Next_GSTN;

	/* Locator Edit of List of GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editComp_gstr3B_config']/div/div[2]/span/span/span[2]")
	List <WebElement>  Edit_GSTR3B_List;

	/* Locator Edit of List Direct Input  of GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using="//ul[contains(@id,'editComp_3b_input_type')]/li[contains(text(),'Direct Input')]")
	List <WebElement>  EditDirectInput_GSTR3B_List;

	/* Locator EDIT Next on GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using="//a[@trigger-id='editcomP6' and contains(text(),'Next')]")
	WebElement  Edit_Next_GSTR3B;


	/* Locator EDIT HSN Summary checkbox on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editcompP6']/div/div/input[@id='editComp_out_HSN_config']")
	WebElement Edit_HSN_checkbox;

	/* Locator EDIT ITC checkbox on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editcompP6']/div/div/input[@id='editComp_in_ITC_config']")
	WebElement Edit_ITC_checkbox;

	/* Locator EDIT Save of Add Company page  */
	@FindBy(how = How.XPATH, using="//button[@class='next-step updateComp k-button k-success float-right' and contains(text(),'Save')]")
	WebElement Edit_Save_AddCompany;

	/* Locator Company Updated Successfully on Add Company page  */
	@FindBy(how = How.XPATH, using="//div[@data-role='confirm' and contains(text(),'Company Updated successfully..')]")
	WebElement CompanyUpdated_Success;

	/* Locator for OK Company Updated Successfully on Add Company page  */
	@FindBy(how = How.XPATH, using="//button[@class='k-button k-primary' and contains(text(),'OK')]")
	WebElement Edit_OK;

	//Xpaths for Location

	/* Locator for Location_1 page on Company page  */
	@FindBy(how = How.XPATH, using="//span[contains(text(),'Location')]")
	WebElement VerifyLocation;

	/* Locator for Add Location_1 on Company page  */
	@FindBy(how = How.XPATH, using="//button[@trigger-id='addsubP1' and @type='button']")
	WebElement AddLocation;


	//Xpaths for Adding a New Location_1 data from excel

	/* Locator for Location_1  List of Companies  for Location on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addLoc_company_name_listbox']/li")
	List <WebElement> ListOfCompanies;


	/* Locator for Location_1  Select  Company DropDown on Location page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addsublocP1']/div[1]/div/div/div/span/span/span[@class='k-select']")
	WebElement SelectCompanyDDLforLoc1;

	/* Locator for Location_1  Legal Name of the Business on Location page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addLoc_name']")
	WebElement VerifyLegalBussinesName_Loc1;

	/* Locator for Location_1  TurnOver16_17  on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addLoc_avg_turnover']")
	WebElement Loc1_TurnOver16_17;


	/* Locator for Location_1  Registered Email-ID  on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addLoc_email']")
	WebElement Loc1_Registered_Email_id;

	/* Locator for Location_1  TradeName on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addLoc_tradename']")
	WebElement Loc1_TradeName;

	/* Locator for Location_1  Select State Dropwdown on Company page  */
	@FindBy(how = How.XPATH, using="//div[@id='addsublocP1']/div[6]/div[1]/div/div/span/span/span[@aria-label='select']")
	WebElement  Loc1_State_Dropdown;


	/* Locator for Location_1   Select List of States from the Dropdowm State  on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addLoc_main_state_listbox']/li")
	List <WebElement>  Loc1_ListofStates;


	/* Locator for Location_1  GSTIN on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addLoc_gstin']")
	WebElement Loc1_GSTIN;


	/* Locator for Location_1  PAN on  Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addLoc_pan' and @readonly='']")
	WebElement Loc1_PAN;

	/* Locator for Location_1   TurnOver_April - June 2017  on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addLoc_curr_turnover']")
	WebElement Loc1_TurnOver_AprilJune2017;

	/* Locator for Location_1   Mobile  on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addLoc_mobile']")
	WebElement Loc1_Mobile;


	/* Locator for Location_1  of  Constitution of Business  on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addLoc_constitution' and @readonly='']")
	WebElement  Loc1_ConstOfBussiness;


	/* Locator for Location_1  of Select City Dropwdown on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addsublocP1']/div[6]/div[2]/div/div/span/span/span[@class='k-select']")
	WebElement  Loc1_City_District_Dropdown;


	/* Locator for Location_1  ocation of Select List of City from the Dropdowm State  on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addLoc_main_city_listbox']/li")
	List <WebElement>  Loc1_ListofCity_District;

	/* Locator for Location_1  ocation of Select List of City from the Dropdowm State  on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addLoc_main_city_listbox']/li")
	List <WebElement>  Loc2_ListofCity_District;


	/* Locator for Location_1  Address Tab on add a new location page  */
	@FindBy(how = How.XPATH, using=".//a[@id='addsubP3' and contains(text(),'Address')]")
	WebElement  Loc1_AddressTab;

	//Xpaths for Location_1 Address Tab for add new Location on Company Page

	/* Locator Add address line 1 on add address Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addLoc_add_line_1' and @class='clearEdit k-textbox']")
	WebElement  Loc1_address1;

	/* Locator Location Add address line 2 on add address Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addLoc_add_line_2' and @class='clearEdit k-textbox']")
	WebElement  Loc1_address2;

	/* Locator Location Add pincode on add address Company page  */
	@FindBy(how = How.XPATH, using="//input[@id='addLoc_pincode']")
	WebElement  Loc1_pincode_address;

	/* Locator Location Address type dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='branchaddress']/div[1]/div[1]/div/div/span/span/span[2]")
	WebElement  Loc1_dropdown_address;


	/* Locator Location Address type List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addLoc_address_type_listbox']/li")
	List <WebElement>  Loc1_ListddlAddress;


	/* Locator Location Premises  dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='branchaddress']/div[1]/div[2]/div/div/span/span/span[2]")
	WebElement  Loc1_dropdown_Premises;


	/* Locator Location  Premises  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addLoc_premises_listbox']/li")
	List <WebElement>  Loc1_ListddlPremises;


	/* Locator Location States dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='branchaddress']/div[4]/div[1]/div/div/span/span/span[2]")
	WebElement  Loc1_dropdown_States;


	/* Locator Location States  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addLoc_state_listbox']/li")
	List <WebElement>  Loc1_ListddlStates;

	/* Locator Location City dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='branchaddress']/div[4]/div[2]/div/div/span/span/span[2]")
	WebElement  Loc1_dropdown_City;


	/* Locator Location  City List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addLoc_city_listbox']/li")
	List <WebElement>  Loc1_ListddlCity;

	/* Locator Location BussinessActvty  dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addsublocP3']/div[2]/div[2]/span/span/span[2]")
	WebElement  Loc1_dropdown_BussinessActvty;


	/* Locator  Location BussinessActvty  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addLoc_nature_listbox']/li")
	List <WebElement>  Loc1_ListBussinessActvty;


	/* Locator Location Next_Adress on add address Company page  */
	@FindBy(how = How.XPATH, using=".//a[@id='prev2' and contains(text(),'Next')]")
	WebElement  Loc1_Next_to_GST_User;


	/* Locator Location GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='addLoc_username']")
	WebElement  Loc1_GSTN_username;


	/* Locator Location GSTN_username_1 dropdown on Add Company page  */
	@FindBy(how = How.XPATH, using=".//div[@id='addsublocP4']/div[2]/div/span[@class='k-widget k-dropdown k-header form-control']/span/span[@class='k-select']")
	WebElement  Loc1_GSTN_username1;

	/* Locator Location GSTN_username_2 dropdown on Add Company page  */
	@FindBy(how = How.XPATH, using=".//div[@id='addsublocP4']/div[3]/div/span[@class='k-widget k-dropdown k-header form-control']/span/span[@class='k-select']")
	WebElement  Loc1_GSTN_username2;


	/* Locator Location GSTN_users on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addLoc_auth1_email_listbox']/li")
	List <WebElement>  Loc1_GSTN_users;


	/* Locator Location Verify Name on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//span[@id='addLoc_auth1_name']")
	WebElement  Loc1_Name_GSTN;

	/* Locator Location Verify Number on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using=".//span[@id='addLoc_auth1_mobile']")
	WebElement  Loc1_Number_GSTN;


	/* Locator Location Verify PAN on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//span[@id='addLoc_auth1_pan']")
	WebElement  Loc1_PAN_GSTN;


	/* Locator Location 1 on NEXT  GST User on Add Company page  */
	@FindBy(how = How.XPATH, using="//a[@trigger-id='addsubP5' and contains(text(),'Next')]")
	WebElement  Loc1_Next_GSTR3B;
	

	/* Locator GSTR3B _Loc01 of GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addsublocP5']/div/div[2]/span/span/span[2]")
	List <WebElement>  GSTR3B_Loc01_List;
	

	/* Locator List Compile Loc01  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//li[contains(text(),'Compile')]")
	List <WebElement>  GSTR3B_Loc01_Compile;
	

	/* Locator Location 1 on NEXT on GSTR3B Add Company page  */
	@FindBy(how = How.XPATH, using="//a[@trigger-id='addsubP6' and contains(text(),'Next')]")
	WebElement  Loc1_Next_HSN;


	/* Locator  Location 1  dropdown on GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addsublocP5']/div/div[2]/span/span/span[@class='k-select']")
	WebElement Loc1_GSTR3B;
	

	/* Locator  Location 1  GSTR3B_march2018 of GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addLoc_3b_input_type0_listbox']/li")
	List <WebElement>  Loc1_GSTR3B_march2018;


	/* Locator  Location 1  Next on GSTR3B tab on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addsublocP5']/div/div[2]/span/span/span[@class='k-select']")
	WebElement Loc1_HSN_Next;


	/* Locator of Location 1  HSN Summary checkbox on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addLoc_out_HSN_config']")
	WebElement Loc1_HSN_checkbox;

	/* Locator of  Location 1 ITR checkbox on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addLoc_in_ITC_config']")
	WebElement Loc1_ITC_checkbox;

	/* Locator of  Location 1 Save of Add Company page  */
	@FindBy(how = How.XPATH, using="//button[@class='k-button addLoc k-success float-right' and contains(text(),'Save')]")
	WebElement Save_AddLoc_1;


	/* Locator of  Location 1 successfully created on Add Company page  */
	@FindBy(how = How.XPATH, using="//button[@class='k-button k-primary' and contains(text(),'OK')]")
	WebElement Location_1_Created;

	/* Locator Location_1 successfully creation List on Company page  */
	@FindBy(how = How.XPATH, using="//div[@class='card-block ov-auto sublocappend']/div/span")
	List <WebElement> Loc_1_CreatnList;


	//Xapths for View Location

	/* Locator Location 1 for VIEW Loc_1 List on Company page  */
	@FindBy(how = How.XPATH, using="//div[@class='card-block ov-auto sublocappend']/div/div/div/div[1]")
	List <WebElement> Loc_1CreatnListView;

	/* Locator Location 1 for VIEW Loc_1 List on Company page  */
	@FindBy(how = How.XPATH, using="//div[@class='card-block ov-auto sublocappend']/div/div/div/div[2]")
	List <WebElement> Loc_1CreatnListEdit;

	/* Locator Location 1  to view Company Loc_1 on View Location page */
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_company_name']")
	WebElement View_Loc1_Company;

	/* Locator Location 1  to view Legal Name of Bussiness Loc_1 on View Location_1 page */
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_name']")
	WebElement View_Loc1_bussiness;


	/*  Locator Location 1  to view Turnover Loc_1 on View Location_1 page */
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_avg_turnover']")
	WebElement View_Loc1_turnover;


	/* Locator  Location 1  to view Email-ID Loc_1 on View Location_1 page */
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_email']")
	WebElement View_Loc1_emailid;


	/* Locator Location 1  to view TradeName Loc_1 on View Location_1 page */
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_tradename']")
	WebElement View_Loc1_Tradename;


	/*Locator Location 1  to view State Loc_1 on View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_main_state']")
	WebElement View_Loc1_State ;


	/* Locator Location 1  to view GSTIN Loc_1 on View Company page  */
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_gstin']")
	WebElement View_GSTIN_Loc1_State ;

	/* Locator Location 1  to view Pan Loc_1 on View Company page */
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_pan']")
	WebElement View_Pan_Loc1_State ;

	/*Locator Location 1 to view TurnoverApr-June Loc_1 on View Company page*/
	@FindBy(how = How.XPATH, using=".//*[@id='viewLoc_curr_turnover']")
	WebElement View_turnoverApr_Loc1_June;

	/*Locator Location 1 to view Mobile Loc_1 on View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_mobile']")
	WebElement View_Loc1_mobile;


	/* Locator Location 1 to view Constitution Loc_1 on View Company page */
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_constitution']")
	WebElement View_Loc1_Constitution;


	/* Locator Location 1  to view City Loc_1 on View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_main_city']")
	WebElement View_Loc1_City;

	/* Locator Location 1  to Address_Tab Loc_1 on View Company page*/
	@FindBy(how = How.XPATH, using=".//a[@id='locViewP3']")
	WebElement View_Address_Loc1_Tab;



	/* Locator Location 1  to view Address1 Loc_1 on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_add_line_1']")
	WebElement View_Add1_Loc_1;

	/* Locator Location 1  to view Address2 Loc_1 on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_add_line_2']")
	WebElement View_Add2_Loc_1;


	/*Locator Location 1   to view Pincode Loc_1 on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_pincode']")
	WebElement View_Pincode_Loc_1;

	/*Locator Location 1  to view Addresstype Loc_1 on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_address_type']")
	WebElement View_Addresstype_Loc_1;

	/*Locator Location 1  to view Premises Loc_1 on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_premises']")
	WebElement View_Premises_Loc_1;

	/*Locator Location 1  to view State Loc_1 on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_state']")
	WebElement View_StateAddress_Loc_1;

	/*Locator Location 1  to view City Loc_1 on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_city']")
	WebElement View_CityAddress_Loc_1;

	/*Locator Location 1  to view Nature of Business Activity Loc_1 on Address Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_nature']")
	WebElement View_BA_Loc_1;

	/*Locator Location 1  to click on GSTIN_User Tab Loc_1 on GST User Tab View Company page*/
	@FindBy(how = How.XPATH, using="//a[@id='locViewP4' and contains(text(),'GST User')]")
	WebElement View_GSTIN_User_Loc_1;


	/*Locator Location 1  to view GSTIN_Username Loc_1 on GST User Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_username']")
	WebElement View_GSTIN_Username_Loc_1;


	/*Locator Location 1  to view GSTIN_Username Loc_1 on GST User Tab View Company page*/
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_auth1_email']")
	WebElement View_GSTIN_Email_ID_Loc_1;


	/* Locator Location 1  Verify Name Loc_1 on  GSTN_username on View Company page  */
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_auth1_name']")
	WebElement  View_Name_GSTN_Loc_1;

	/* Locator Location 1  Verify Number Loc_1 on  GSTN_username on View Company page  */
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_auth1_mobile']")
	WebElement  View_Number_GSTN_Loc_1;


	/* Locator Location 1  Verify PAN Loc_1 son  GSTN_username on View Company page  */
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_auth1_pan']")
	WebElement  View_PAN_GSTN_Loc_1;


	/* Locator Location 1  GSTR3B on View Company page  */
	@FindBy(how = How.XPATH, using=".//a[@id='locViewP5' and contains(text(),'GSTR3B')]")
	WebElement  View_GSTRR3B_Loc_1;
	
	/* Locator List of Location 1 GSTR3B Compile on View Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='viewLoc_gstr3B_config']/div/div[2]/span")
	List <WebElement>  List_Loc_GSTRR3BCompile;


	/* Locator Location 1  Type in GSTR3B on View Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='viewLoc_gstr3B_config']/div/div[2]/span")
	WebElement  View_Type_Loc_1;


	//View X-paths for HSN & ITC


	/* Locator Location 1  Other Configuration on View Company page  */
	@FindBy(how = How.XPATH, using=".//a[@id='locViewP6' and contains(text(),'Other Configuration')]")
	WebElement  Loc_1_View_OtherConfig;


	/* Locator Location 1  Type in Other Configuration on HSN Summary VALUE on View Company page  */
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_out_HSN_config']")
	WebElement   Loc_1_View_HSNSummary_Value;


	/* Locator Location 1  Type in Other Configuration on ITC VALUE on View Company page  */
	@FindBy(how = How.XPATH, using=".//span[@id='viewLoc_in_ITC_config']")
	WebElement  Loc_1_View_ITC_Value;

	/* Locator Location 1 CLOSE on View Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='sublocViewModal']/div/div/div/button[contains(text(),'Close')]")
	WebElement   Loc_1_View_Close;


	//Xpaths for EDIT
	/*
	 * Sample
	 *
	/* Locator Location EDIT  Check non-editable Field - Loc_1_Edit_CompanyName Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editLoc_company_name' and @readonly='']")
	WebElement  Loc_1_Edit_CompanyName;


	/* Locator for EDIT Legal NameofBussiness on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editLoc_name' and @readonly='']")
	WebElement Loc_1_Edit_NameofBussiness;


	/* Locator for EDIT TurnOver16_17  on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editLoc_avg_turnover']")
	WebElement Loc_1_Edit_TurnOver16_17;


	/* Locator for EDIT Registered Email-ID  on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editLoc_email']")
	WebElement Loc_1_Edit_Registered_Email_id;
	

	/* Locator for EDIT  TradeName on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editLoc_tradename']")
	WebElement Loc_1_Edit_TradeName;


	/* Locator for EDIT Select State Dropwdown on Company page  */
	@FindBy(how = How.XPATH, using="//div[@id='editsublocP1']/div[6]/div[1]/div/div/span/span[1]")
	WebElement  Loc_1_Edit_State_Present;


	/* Locator for EDIT GSTIN on Company page  */
	@FindBy(how = How.XPATH, using="//input[@id='editLoc_gstin' and @class='k-textbox' and @type='text' and @readonly='']")
	WebElement Loc_1_Edit_GSTIN;


	/* Locator for EDIT PAN on  Company page  */
	@FindBy(how = How.XPATH, using="//input[@id='editLoc_pan' and @class='k-textbox' and  @type='text' and @readonly='']")
	WebElement Loc_1_Edit_PAN;

	/* Locator for EDIT  TurnOver_April - June 2017  on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editLoc_curr_turnover']")
	WebElement Loc_1_Edit_TurnOver_AprilJune2017;

	/* Locator for EDIT Mobile  on Company page  */
	@FindBy(how = How.XPATH, using="//input[@id='editLoc_mobile' and @class='k-textbox' and @type='text']")
	WebElement Loc_1_Edit_Mobile;


	/* Locator for EDIT Select Constitution of Business  Dropwdown on Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editLoc_constitution' and @readonly='']")
	WebElement  Loc_1_Edit_ConstOfBussiness;


	/* Locator for EDIT Select City Dropwdown on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editsublocP1']/div[6]/div[2]/div/div/span/span/span[1]")
	WebElement  Loc_1_Edit_City_District_Dropdown;


	/* Locator for EDIT Select List of City from the Dropdowm State  on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editLoc_main_city_listbox']/li")
	List <WebElement>   Loc_1_Edit_ListofCity_District;


	/* Locator for Address on Edit Company Details on Company page  */
	@FindBy(how = How.XPATH, using=".//a[@id='editsubP3']")
	WebElement  Loc_1_Edit_Address_Tab;


	/* Locator EDIT Add address line 1 on add address Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editLoc_add_line_1']")
	WebElement  Loc_1_Edit_address1;

	/* Locator EDIT Add address line 2 on add address Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editLoc_add_line_2']")
	WebElement  Loc_1_Edit_address2;

	/* Locator EDIT Add pincode on add address Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editLoc_pincode']")
	WebElement   Loc_1_Edit_pincode_address;


	/* Locator EDIT Address type dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editsublocP3']/div[1]/div/div[1]/div[1]/div/div/span/span/span[2]")
	WebElement   Loc_1_Edit_dropdown_address;


	/* Locator EDIT Address type List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editLoc_address_type_listbox']/li")
	List <WebElement>  Loc_1_Edit_ListddlAddress;

	/* Locator EDIT Premises  dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editsublocP3']/div[1]/div/div[1]/div[2]/div/div/span/span/span[2]")
	WebElement   Loc_1_Edit_dropdown_Premises;


	/* Locator EDIT Premises  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editLoc_premises_listbox']/li")
	List <WebElement>   Loc_1_Edit_ListddlPremises;


	/* Locator EDIT States  dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editsublocP3']/div[1]/div/div[4]/div[1]/div/div/span/span/span[@class='k-select']")
	WebElement    Loc_1_Edit_dropdown_States;


	/* Locator EDIT States  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editLoc_state_listbox']/li	")
	List <WebElement>    Loc_1_Edit_ListddlStates;

	/* Locator EDIT City  dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editsublocP3']/div[1]/div/div[4]/div[2]/div/div/span/span/span[@class='k-select']")
	WebElement   Loc_1_Edit_dropdown_City;


	/* Locator EDIT City  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editLoc_city_listbox']/li")
	List <WebElement>   Loc_1_Edit_ListddlCity;

	/* Locator EDIT BussinessActvty  dropdown on add address Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editsublocP3']/div[2]/div[2]/span/span/span[@class='k-select']")
	WebElement  Loc_1_Edit_dropdown_BussinessActvty;


	/* Locator EDIT  BussinessActvty  List  on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editLoc_nature_listbox']/li")
	List <WebElement>  Loc_1_Edit_ListBussinessActvty;


	/* Locator EDIT  NEXT on  add address Company page  */
	@FindBy(how = How.XPATH, using="//a[@trigger-id='editsubP4' and contains(text(),'Next')]")
	WebElement  Loc_1_Edit_Address_next;



	/* Locator EDIT GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editLoc_username']")
	WebElement  Loc_1_Edit_GSTN_username;


	/* Locator EDIT GSTN_username_1 dropdown on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editsublocP4']/div[2]/div[1]/span/span/span[2]")
	WebElement  Loc_1_Edit_GSTN_username1;

	/* Locator EDIT GSTN_username_2 dropdown on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editsublocP4']/div[3]/div[1]/span/span/span[2]")
	WebElement  Loc_1_Edit_GSTN_username2;


	/* Locator EDIT GSTN_users on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editLoc_auth1_email_listbox']/li")
	List <WebElement>  Loc_1_Edit_GSTN_users;


	/* Locator EDIT Verify Name on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using=".//span[@id='editLoc_auth1_name']")
	WebElement  Loc_1_Edit_Name_GSTN;

	/* Locator EDIT Verify Number on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//span[@id='editLoc_auth1_mobile']")
	WebElement  Loc_1_Edit_Number_GSTN;


	/* Locator EDIT Verify PAN on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//span[@id='editLoc_auth1_pan']")
	WebElement  Loc_1_Edit_PAN_GSTN;


	/* Locator EDIT NEXT on  GSTN_username on Add Company page  */
	@FindBy(how = How.XPATH, using="//a[@trigger-id='editsubP5' and contains(text(),'Next')]")
	WebElement   Loc_1_Edit_GSTUser_Next_GSTN;

	
	

	/* Locator EDIT GSTR3B DropDown of GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='editLoc_gstr3B_config']/div/div[2]/span/span/span[2]")
	List <WebElement>  Loc_1_Edit_GSTR3B_drpdown;
	


	/* Locator EDIT GSTR3B Direct Input of GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using="//li[contains(text(),'Direct Input')]")
	List <WebElement>  Loc_1_Edit_GSTR3B_DirectInput;
	
	

	/* Locator EDIT Next on GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using="//a[@trigger-id='editsubP6' and contains(text(),'Next')]")
	WebElement  Loc_1_Edit_Next_GSTR3B;


	/* Locator EDIT HSN Summary checkbox on Add Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editLoc_out_HSN_config']")
	WebElement Loc_1_Edit_HSN_checkbox;

	/* Locator EDIT ITC checkbox on Add Company page  */
	@FindBy(how = How.XPATH, using=".//input[@id='editLoc_in_ITC_config']")
	WebElement Loc_1_Edit_ITC_checkbox;

	/* Locator EDIT Save of Add Company page  */
	@FindBy(how = How.XPATH, using="//button[@class='k-button updateLoc k-success float-right' and contains(text(),'Save')]")
	WebElement Loc_1_Edit_Save_AddCompany;

	/* Locator Company Updated Successfully on Add Company page  */
	@FindBy(how = How.XPATH, using="//div[@data-role='confirm' and contains(text(),'Location Updated successfully.')]")
	WebElement Loc_1_LocationUpdated_Success;

	/* Locator for OK Company Updated Successfully on Add Company page  */
	@FindBy(how = How.XPATH, using="//button[@class='k-button k-primary' and contains(text(),'OK')]")
	WebElement Loc_1_Edit_OK;




	//Negative scenaios for Company 1
	/* Locator for Please Select State on Add Company page  */
	@FindBy(how = How.XPATH, using="//div[@id='addComp_main_state-list']/div[contains(text(),'Please Select')]")
	WebElement Comp01_pleaseSelect_State;


	/* Locator for Please Select State on constitution of bussiness on Add Company page  */
	@FindBy(how = How.XPATH, using="//div[@id='addComp_constitution-list']/div[contains(text(),'Please Select')]")
	WebElement Comp01_pleaseSelect_ConstBussiness;


	/* Locator for Please Select City on Add Company page  */
	@FindBy(how = How.XPATH, using="//div[@id='addComp_constitution-list']/div[contains(text(),'Please Select')]")
	WebElement Comp01_pleaseSelect_City;


	//Xpaths for GSTR3B tab 5 months present

	/* Locator for first GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using="//div[@id='addcompP5']/div/div/label[contains(text(),'January 2018')]")
	WebElement GSTR3B_1;


	/* Locator for second GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using="//div[@id='addcompP5']/div/div/label[contains(text(),'January 2018')]")
	WebElement GSTR3B_2;


	/* Locator for third GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using="//div[@id='addcompP5']/div/div/label[contains(text(),'January 2018')]")
	WebElement GSTR3B_3;


	/* Locator for fourth GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using="//div[@id='addcompP5']/div/div/label[contains(text(),'January 2018')]")
	WebElement GSTR3B_4;


	/* Locator for fifth GSTR3B on Add Company page  */
	@FindBy(how = How.XPATH, using="//div[@id='addcompP5']/div/div/label[contains(text(),'January 2018')]")
	WebElement GSTR3B_5;

	//xpath for _indicates error on save

	/* Locator for Save_indicates Error Company on   Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addcompP6']/span[@class='errindi']")
	WebElement Save_indicatesError_comp;

	
	/* Locator for Save_indicates Error Location on Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addsublocP6']/span[@class='errindi']")
	WebElement Save_indicatesError_Loc01;

	
	//xpaths for errors on subtabs on Add Company

	/* Locator for error1  on Basic Information Sub-tab  Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='error1']")
	WebElement error_1;

	/* Locator for error3  on Address Sub-tab  Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='error3']")
	WebElement error_3;


	/* Locator for error4  on GST User Sub-tab  Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='error4']")
	WebElement error_4;


	/* Locator for error5  on GSTR3B Sub-tab  Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='error5']")
	WebElement error_5;


	/* Locator for error msg for company name on Basic Information Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Company Name')]/span[@class='k-widget k-tooltip k-tooltip-validation k-invalid-msg']")
	WebElement CompName_ErrorMsg;


	/* Locator for error msg for Turnover name on Basic Information Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Turnover 2016-2017')]/span[@class='k-widget k-tooltip k-tooltip-validation k-invalid-msg']")
	WebElement Turnover_ErrorMsg;


	/* Locator for error msg for EMail name on Basic Information Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Registered Email-id')]/span[@class='k-widget k-tooltip k-tooltip-validation k-invalid-msg']")
	WebElement Email_ErrorMsg;


	/* Locator for error msg for Trade name on Basic Information Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Trade Name/Alias')]/span[@class='k-widget k-tooltip k-tooltip-validation k-invalid-msg']")
	WebElement TradeName_ErrorMsg;


	/* Locator for error msg for State name on Basic Information Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'State')]/span[@class='k-widget k-tooltip k-tooltip-validation k-invalid-msg']")
	WebElement State_ErrorMsg;


	/* Locator for error msg for GSTIN_ErrorMsg on Basic Information Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//label[contains(text(),'GSTIN ')]/span[@class='k-widget k-tooltip k-tooltip-validation k-invalid-msg']")
	WebElement GSTIN_ErrorMsg;


	/* Locator for error msg for PAN_ErrorMsg on Basic Information Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'PAN')]/span[@class='k-widget k-tooltip k-tooltip-validation k-invalid-msg']")
	WebElement PAN_ErrorMsg;


	/* Locator for error msg for Turnover_2017__ErrorMsg on Basic Information Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Turnover April-June 2017')]/span[@class='k-widget k-tooltip k-tooltip-validation k-invalid-msg']")
	WebElement Turnover_2017__ErrorMsg;

	/* Locator for error msg for Registered Mobile on Basic Information Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Registered Mobile')]/span[@class='k-widget k-tooltip k-tooltip-validation k-invalid-msg']")
	WebElement Registered_Mobile_ErrorMsg;


	/* Locator for error msg for ConstBussiness on Basic Information Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Constitution of Business')]/span[@class='k-widget k-tooltip k-tooltip-validation k-invalid-msg']")
	WebElement ConstBussiness_ErrorMsg;	

	//xpaths for address sub tab
	/* Locator for AddLine1 on Address Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//label[contains(text(),'Address Line 1')]/span[@data-for='addComp_add_line_1']")
	WebElement AddLine1_ErrorMsg;	


	/* Locator for AddLine2 on Address Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//label[contains(text(),'Address Line 2')]/span[@data-for='addComp_add_line_2']")
	WebElement AddLine2_ErrorMsg;


	/* Locator for  AddressType on Address Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//label[contains(text(),'Address Type')]/span[@data-for='addComp_address_type']")
	WebElement AddressType_ErrorMsg;


	/* Locator for PinCode on Address Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//label[contains(text(),'PIN Code')]/span[@data-for='addComp_pincode']")
	WebElement PinCode_ErrorMsg;


	/* Locator for Add State on Address Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//label[contains(text(),'State')]/span[@data-for='addComp_state']")
	WebElement AddState_ErrorMsg;


	/* Locator for Nature of BA on Address Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//label[contains(text(),'Nature of Business Activity')]/span[@data-for='addComp_nature']")
	WebElement NatureofBA_ErrorMsg;


	/* Locator for GSTIN Username  on GST User Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'GSTIN Username')]/span[@data-for='addComp_username']")
	WebElement GSTIN_Username_ErrorMsg;


	/* Locator for List  of months in GSTR3B  on GSTR3B Sub-tab Add Company page  */
	@FindBy(how = How.XPATH, using="//span[@data-for='addComp_3b_input_type[]']")
	List <WebElement> List_months_GSTR3B_ErrorMsg;



	//xpaths for errors on SubTabs of Add Location


	/* Locator for error_01_Loc1  on Basic Information Sub-tab  Add Location page  */

	@FindBy(how = How.XPATH, using=".//*[@id='addsubtab1']")
	WebElement error_1_LOC01;

	
	/* Locator for error_03_Loc1   on Address Sub-tab  Add Location page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addsubtab3']")
	WebElement error_3_LOC01;


	/* Locator for error_04_Loc1   on GST User Sub-tab  Add Location page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addsubtab4']")
	WebElement error_4_LOC01;


	/* Locator for error_05_Loc1   on GSTR3B Sub-tab  Add Location page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addsubtab5']")
	WebElement error_5_LOC01;


	/* Locator for loc1 please select option in state dropdown   on GSTR3B Sub-tab  Add Location page  */
	@FindBy(how = How.XPATH, using="//div[@id='addLoc_main_state-list']/div[contains(text(),'Please Select')]")
	WebElement Loc01_State_PleaseSelect;


	/* Locator for Select City Dropdown on Location 01 page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addsublocP1']/div[6]/div[2]/div/div/span/span[@class='k-dropdown-wrap k-state-disabled']")
	WebElement  Loc01_CityDropdown_stateinactive;

	
	
	/* Locator for error msg for company name on Basic Information Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Select Company')]/span[@data-for='addLoc_company_name']")
	WebElement Loc01_Company_MandatoryMsg;
	
	
	/* Locator for error msg for company name on Basic Information Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Turnover 2016-2017')]/span[@data-for='addLoc_avg_turnover']")
	WebElement Loc01_Turnover_ErrorMsg;


	/* Locator for error msg for Turnover name on Basic Information Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Registered Email-id')]/span[@data-for='addLoc_email']")
	WebElement Loc01_Email_ErrorMsg;


	/* Locator for error msg for EMail name on Basic Information Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Trade Name/Alias')]/span[@data-for='addLoc_tradename']")
	WebElement Loc01_TradeName_ErrorMsg;


	/* Locator for error msg for Trade name on Basic Information Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'State')]/span[@data-for='addLoc_main_state']")
	WebElement Loc01_State_ErrorMsg;


	/* Locator for error msg for State name on Basic Information Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'GSTIN')]/span[@data-for='addLoc_gstin']")
	WebElement Loc01_GSTIN_ErrorMsg;

	
	/* Locator for error msg for GSTIN_ErrorMsg on Basic Information Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Turnover April-June 2017')]/span[@data-for='addLoc_curr_turnover']")
	WebElement Loc01_TurnoverApril2017_ErrorMsg;

	
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Registered Mobile')]/span[@data-for='addLoc_mobile']")
	WebElement Loc01_RegisteredMobile_ErrorMsg;
	
	
	

	//xpaths for address sub tab
	
	/* Locator for AddLine1 on Address Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using="//label[contains(text(),'Address Line 1')]/span[@data-for='addLoc_add_line_1']")
	WebElement AddLine1_Loc01_ErrorMsg;	


	/* Locator for AddLine2 on Address Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using="//label[contains(text(),'Address Line 2')]/span[@data-for='addLoc_add_line_2']")
	WebElement AddLine2_Loc01_ErrorMsg;


	/* Locator for  AddressType on Address Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using="//label[contains(text(),'Address Type')]/span[@data-for='addLoc_address_type']")
	WebElement AddressType_Loc01_ErrorMsg;


	/* Locator for PinCode on Address Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using="//label[contains(text(),'PIN Code')]/span[@data-for='addLoc_pincode']")
	WebElement PinCode_Loc01_ErrorMsg;


	/* Locator for Add State on Address Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using="//label[contains(text(),'State')]/span[@data-for='addLoc_state']")
	WebElement AddState_Loc01_ErrorMsg;


	/* Locator for Nature of BA on Address Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Nature of Business Activity')]/span[@data-for='addLoc_nature']")
	WebElement NatureofBA_Loc01_ErrorMsg;


	/* Locator for GSTIN Username  on GST User Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using=".//p[@id='chCompName']/span[@data-for='addLoc_username']")
	WebElement GSTIN_Username_Loc01_ErrorMsg;


	/* Locator for List  of months in GSTR3B  on GSTR3B Sub-tab Add Location page  */
	@FindBy(how = How.XPATH, using="//span[@data-for='addLoc_3b_input_type[]']")
	List <WebElement> List_months_Loc01_GSTR3B_ErrorMsg;

	/* Locator for Other Configuration on Sub-tab of Add Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addcomP6']")
	WebElement OtherConfig_Comp_Neg_02;

	
	/* Locator for Other Configuration on Sub-tab of Add Location page  */
	@FindBy(how = How.XPATH, using=".//*[@id='addsubP6']")
	WebElement OtherConfig_Loc_Neg_02;


	/* Locator for PAN exists error message on Company page  */
	@FindBy(how = How.XPATH, using=".//*[@id='globalErrorModal']/div[@class='error_messages']")
	WebElement Comp01_Pan_exist_error;
	

	



}
