package Company;

import java.io.File;
import java.io.FileInputStream;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.testng.asserts.SoftAssert;

import Group.TestBase;

public class CompanyMethods extends TestBase{

	int companynames=0;
	SoftAssert softassert = new SoftAssert();
	String GSTIN_Mail = null;
	CompanyPage comp = new CompanyPage(driver);
	File src ; 
	FileInputStream fis;
	Workbook wb ; 
	XSSFSheet sheet2 ; 
	XSSFSheet sheet3 ;
	XSSFSheet sheet4 ; 
	XSSFSheet sheet5 ;
	XSSFSheet sheet6 ;
	XSSFSheet sheet7 ;

	public void Login() throws Exception
	{

		Thread.sleep(3000);
		comp.UserNameBDO.sendKeys("test3119@mailinator.com");
		//wait(2, incrementor );
		comp.PasswordBDO.sendKeys("Bdo@123#");	
		//wait(2, incrementor );
		comp.LoginBDO.click();
		wait(3, incrementor );
		if(comp.NewCloseFunctionality.isDisplayed())
		{
			comp.NewCloseFunctionality.click();
			Thread.sleep(3000);
		}
		else
		{
			System.out.println("No new Release notes added");
		}


	}

	public void SelectCompany() throws Exception 

	{
		Thread.sleep(5000);
		comp.sidebaricon.click();
		Thread.sleep(6000);
		comp.Company.click();
		Thread.sleep(5000);
		softassert.assertEquals(comp.Verifycompany.getText().trim() , "Company" , " Company page does not exist ");
		softassert.assertAll();
	}

	public void AddCompany() throws Exception 

	{
		int companyone=1;
		comp.Addcompany.click();
		Thread.sleep(3000);
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet3 = (XSSFSheet) wb.getSheetAt(2);
		sheet2 = (XSSFSheet) wb.getSheetAt(1);
		System.out.println("The total row number is : "+sheet3.getLastRowNum());
		String GSTIN = null;
		for(int i=0 ; i <sheet3.getLastRowNum() ; i++)

		{

			String value  = sheet3.getRow(i).getCell(companynames).toString();
			System.out.println("The value is : "+ value);

			if(value.equalsIgnoreCase("Account/Group Name"))
			{
				String accnameGrp  = sheet2.getRow(i).getCell(1).toString();
				Thread.sleep(2000);				
			}

			else if(value.equalsIgnoreCase("Company Name"))
			{
				String companyName  = sheet3.getRow(i).getCell(1).toString();
				comp.AddCompanyName.sendKeys(companyName);
				Thread.sleep(2000);
			}


			else if(value.equalsIgnoreCase("Turnover_2016_2017"))
			{
				String Turnover1  = sheet3.getRow(i).getCell(1).toString();
				Thread.sleep(2000);
				comp.TurnOver16_17.sendKeys(Turnover1);
				Thread.sleep(2000);
			}


			else if(value.equalsIgnoreCase("Registered Email-id"))
			{
				String Emailid  = sheet3.getRow(i).getCell(1).toString();
				Thread.sleep(2000);
				comp.Registered_Email_id.sendKeys(Emailid);
				Thread.sleep(3000);
			}

			else if(value.equalsIgnoreCase("Trade Name/Alias"))
			{
				String Tradename  = sheet3.getRow(i).getCell(1).toString();
				comp.TradeName.sendKeys(Tradename);
				Thread.sleep(2000);
			}

			else if(value.equals("State_Type"))
			{
				System.out.println("Entered State Loop");
				Thread.sleep(4000);
				comp.State_Dropdown.click();
				Thread.sleep(5000);

				for(int c=0 ; c<comp.ListofStates.size() ; c++)
				{
					if(comp.ListofStates.get(c).getText().equalsIgnoreCase("Maharashtra"))
					{
						Thread.sleep(5000);
						comp.ListofStates.get(c).click();
						Thread.sleep(8000);
						break;
					}

				}
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("GSTIN_Type"))
			{
				GSTIN  = sheet3.getRow(i).getCell(1).toString();
				Thread.sleep(3000);
				System.out.println("The GSTIN is  : "+GSTIN);
				Thread.sleep(4000);
				comp.GSTIN.sendKeys(GSTIN);
				Thread.sleep(3000);
			}
			else if(value.equalsIgnoreCase("Pan_Type"))
			{
				String Pan  = GSTIN.substring(2,12);
				Thread.sleep(4000);
				System.out.println("The PAN from GSTIN "+GSTIN+" is "+Pan);
				comp.PAN.sendKeys(Pan);
				Thread.sleep(3000);
			}
			else if(value.equalsIgnoreCase("TurnoverType"))
			{
				Thread.sleep(2000);
				String TurnOver_AprilJune2017  = sheet3.getRow(i).getCell(1).toString();
				Thread.sleep(5000);
				comp.TurnOver_AprilJune2017.sendKeys(TurnOver_AprilJune2017);
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Registered_Mobile"))
			{
				String Mobile  = sheet3.getRow(i).getCell(1).toString();
				Thread.sleep(3000);
				comp.Mobile.sendKeys(Mobile);
				Thread.sleep(3000);
			}

			else if(value.equals("Constitution_of_Business"))
			{
				System.out.println("Entered Constitution of Business");
				Thread.sleep(3000);
				comp.ConstOfBussiness.click();
				Thread.sleep(3000);

				for(int a=0 ; a<comp.ListConstOfBussiness.size() ; a++)
				{
					Thread.sleep(3000);
					if(comp.ListConstOfBussiness.get(a).getText().equalsIgnoreCase(sheet3.getRow(i).getCell(1).toString()))
					{
						Thread.sleep(5000);
						comp.ListConstOfBussiness.get(a).click();
						Thread.sleep(3000);
						break;
					}

				}
				Thread.sleep(2000);
			}
			else if(value.equals("City_District"))
			{
				System.out.println("Entered City/District loop");
				Thread.sleep(3000);
				comp.City_District_Dropdown.click();
				Thread.sleep(3000);

				for(int j=0 ; j<comp.ListofCity_District.size() ; j++)
				{
					if(comp.ListofCity_District.get(j).getText().equalsIgnoreCase(sheet3.getRow(i).getCell(1).toString()))
					{
						Thread.sleep(2000);
						comp.ListofCity_District.get(j).click();
						Thread.sleep(3000);
						break;
					}

				}
				Thread.sleep(2000);
				break;
			}
			else
			{
				System.out.println("Fail to take data from excel");
			}

		}

		softassert.assertAll();
	}

	public void Users() throws Exception
	{
		driver.get("http://test.enablegst.info");
		Thread.sleep(4000);
		comp.UserNameBDO.sendKeys("newgroup1@mailinator.com");
		Thread.sleep(3000);
		comp.PasswordBDO.sendKeys("Bdo@123#");
		Thread.sleep(3000);
		//logger.info("Password entered successfully");
		comp.LoginBDO.click();
		Thread.sleep(6000);
		comp.sidebaricon.click();
		Thread.sleep(3000);
		driver.findElement(By.xpath(".//*[@id='menu']/li/a[contains(text(),'User')]")).click();
		Thread.sleep(6000);
		System.out.println("The no of rows for email id are : "+comp.Listofmailsid.size());

		for(int i =0 ; i <comp.Listofmailsid.size() ; i++ )
		{

			if(comp.Listofmailsid.get(i).getText().trim().equalsIgnoreCase("Clientusers123@mailinator.com"))
			{
				System.out.println("The mail id : "+comp.Listofmailsid.get(i).getText().trim());
				Thread.sleep(6000);
				System.out.println("Reached here");
				comp.ListofUsers.get(i-2).click();
				Thread.sleep(6000);
				break;
			}

		}
		softassert.assertAll();
	}

	public void AddAddress() throws Exception 
	{
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		comp.Next.click();
		Thread.sleep(3000);
		comp.NextRegistration.click();
		Thread.sleep(3000);
		sheet4 = (XSSFSheet) wb.getSheetAt(3);
		for (int i =0 ; i< sheet4.getLastRowNum() ; i++)
		{
			if(i==0)
			{
				comp.address1.sendKeys(sheet4.getRow(i).getCell(1).toString());
				Thread.sleep(3000);
			}

			if(i==1)
			{
				comp.address2.sendKeys(sheet4.getRow(i).getCell(1).toString());
				Thread.sleep(3000);
			}

			if(i==2)
			{
				comp.pincode_address.sendKeys(sheet4.getRow(i).getCell(1).toString());
				Thread.sleep(3000);
				break;
			}

		}

		Thread.sleep(2000);
		comp.dropdown_address.click();
		Thread.sleep(2000);
		comp.ListddlAddress.get(0).click();


		Thread.sleep(2000);
		comp.dropdown_Premises.click();
		Thread.sleep(2000);
		comp.ListddlPremises.get(0).click();


		Thread.sleep(2000);
		comp.dropdown_States.click();
		Thread.sleep(2000);
		comp.ListddlStates.get(20).click();


		Thread.sleep(2000);
		comp.dropdown_City.click();
		Thread.sleep(2000);
		comp.ListddlCity.get(233).click();


		Thread.sleep(2000);
		comp.dropdown_BussinessActvty.click();
		Thread.sleep(2000);
		comp.ListBussinessActvty.get(0).click();

		Thread.sleep(4000);
		comp.Next_Address.click();
		Thread.sleep(3000);
		softassert.assertAll();

	}


	public void addGSTN() throws Exception

	{
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet4 = (XSSFSheet) wb.getSheetAt(3);
		comp.GSTN_username.sendKeys(sheet4.getRow(3).getCell(1).toString());
		Thread.sleep(3000);
		comp.GSTN_username1.click();
		Thread.sleep(3000);
		comp.GSTN_users.get(0).click();
		Thread.sleep(4000);
		softassert.assertTrue(comp.Name_GSTN.isDisplayed(), "The Name is not Displayed");
		softassert.assertTrue(comp.Number_GSTN.isDisplayed(), "The Number is not Displayed");
		softassert.assertTrue(comp.PAN_GSTN.isDisplayed(), "The PAN is not Displayed");
		Thread.sleep(2000);
		comp.Next_GSTN.click();
		Thread.sleep(3000);
		/*
		 * GSTR3B
		 */

		for(int i =0 ; i< comp.GSTR3B_List.size() ; i++)
		{

			Thread.sleep(3000);
			comp.GSTR3B_List.get(i).click();
			Thread.sleep(3000);
			comp.GSTR3B_Compile.get(i).click();
			Thread.sleep(3000);
		}

		Thread.sleep(3000);		
		comp.Next_GSTR3B.click();
		Thread.sleep(5000);
		softassert.assertAll();

	}

	public void HSN_ITC() throws Exception
	{

		if(comp.HSN_checkbox.isSelected())
		{

			softassert.assertTrue(comp.HSN_checkbox.isSelected(), "The CheckBox is not selected");

		}

		else
		{
			softassert.assertFalse(comp.HSN_checkbox.isSelected(), "The CheckBox is not selected");

		}

		if(comp.ITC_checkbox.isSelected())
		{
			softassert.assertTrue(comp.ITC_checkbox.isSelected(), "The CheckBox is not selected");

		}

		else
		{
			softassert.assertFalse(comp.ITC_checkbox.isSelected(), "The CheckBox is not selected");

		}

		softassert.assertAll();
	}

	public void VerifyCompany() throws Exception
	{
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		Thread.sleep(5000);
		comp.Save_AddCompany.click();
		Thread.sleep(15000);
		comp.Company_Created.click();
		Thread.sleep(8000);

		String Tradename = null;
		for(int i=0 ; i <= sheet3.getLastRowNum() ; i++)
		{
			String value = sheet3.getRow(i).getCell(0).toString();
			if(value.equalsIgnoreCase("Trade Name/Alias"))
			{
				Tradename  = sheet3.getRow(i).getCell(1).toString();
				System.out.println("The company name from excel is : "+Tradename);
				Thread.sleep(2000);
				break;
			}
		}
		int Value = comp.CompCreatnList.size();
		String Company =  comp.CompCreatnList.get(Value-1).getText().trim();
		softassert.assertEquals(Company, Tradename , "The company is not created Successfully & not present in list");
		softassert.assertAll();
	}


	public void ViewCompany() throws Exception
	{
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet2 = (XSSFSheet) wb.getSheetAt(1);
		sheet3 = (XSSFSheet) wb.getSheetAt(2);
		sheet4 = (XSSFSheet) wb.getSheetAt(3);
		int Value = comp.CompCreatnList.size();
		String Company =  comp.CompCreatnList.get(Value-1).getText().trim();

		String Tradename = null;
		for(int i=0 ; i <= sheet3.getLastRowNum() ; i++)
		{
			String value = sheet3.getRow(i).getCell(0).toString();
			if(value.equalsIgnoreCase("Trade Name/Alias"))
			{
				Tradename  = sheet3.getRow(i).getCell(1).toString();
				System.out.println("The TRADENAME from excel is : "+Tradename);
				Thread.sleep(3000);
				break;
			}
		}


		for(int a=0 ; a< comp.CompCreatnList.size() ; a++)
		{
			if(comp.CompCreatnList.get(a).getText().equalsIgnoreCase(Tradename))
			{
				Thread.sleep(5000);
				comp.CompCreatnListView.get(a).click();
				System.out.println("The company from company page is : "+comp.CompCreatnList.get(a).getText());
				softassert.assertEquals(Company, Tradename , "The company is not Viewed Successfully & not present in company list");
				softassert.assertAll();
				Thread.sleep(5000);
				break;
			}
		}

		String GSTIN = null;
		for(int i=0 ; i <sheet3.getLastRowNum() ; i++)

		{

			String value  = sheet3.getRow(i).getCell(0).toString();
			System.out.println("The value is : "+ value);

			if(value.equalsIgnoreCase("Account/Group Name"))
			{
				Thread.sleep(4000);
				String accnameGrp  = sheet3.getRow(i).getCell(1).toString();
				System.out.println("The Acc Name is : "+accnameGrp);
				Thread.sleep(2000);
				System.out.println("The acc name of group from screen is : "+comp.View_Acc_Grp.getText());
				softassert.assertEquals(accnameGrp, comp.View_Acc_Grp.getText() , " The Account name does not match ");

			}

			else if(value.equalsIgnoreCase("Company Name"))
			{
				String companyName  = sheet3.getRow(i).getCell(1).toString();
				softassert.assertEquals(companyName, comp.View_CompanyName.getText() , " The Company name does not match ");
				Thread.sleep(2000);
			}


			else if(value.equalsIgnoreCase("Turnover_2016_2017"))
			{
				String Turnover1  = sheet3.getRow(i).getCell(1).toString();
				Thread.sleep(2000);
				softassert.assertEquals(Turnover1, comp.View_turnover.getText() , " The Turnover does not match ");
				Thread.sleep(2000);
			}


			else if(value.equalsIgnoreCase("Registered Email-id"))
			{
				String Emailid  = sheet3.getRow(i).getCell(1).toString();
				Thread.sleep(2000);
				softassert.assertEquals(Emailid, comp.View_emailid.getText() , " The Email-ID does not match ");
				Thread.sleep(2000);
			}

			else if(value.equals("State_Type"))
			{
				System.out.println("Entered State Loop");
				String State  = sheet3.getRow(i).getCell(1).toString();
				softassert.assertEquals(State, comp.View_State.getText() , " The State does not match ");
				Thread.sleep(2000);

			}

			else if(value.equalsIgnoreCase("GSTIN_Type"))
			{
				GSTIN  = sheet3.getRow(i).getCell(1).toString();
				Thread.sleep(3000);
				System.out.println("The GSTIN is  : "+GSTIN);
				softassert.assertEquals(GSTIN, comp.View_GSTIN.getText() , " The GSTIN does not match ");
				Thread.sleep(2000);
			}
			else if(value.equalsIgnoreCase("Pan_Type"))
			{
				String Pan  = GSTIN.substring(2,12);
				Thread.sleep(2000);
				System.out.println("The PAN from GSTIN "+GSTIN+" is "+Pan);
				softassert.assertEquals(Pan, comp.View_Pan.getText() , " The PAN does not match ");
				Thread.sleep(2000);
			}
			else if(value.equalsIgnoreCase("TurnoverType"))
			{
				Thread.sleep(2000);
				String TurnOver_AprilJune2017  = sheet3.getRow(i).getCell(1).toString();
				softassert.assertEquals(TurnOver_AprilJune2017, comp.View_turnoverApr_June.getText() , " The TurnoverAprJune does not match ");
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Registered_Mobile"))
			{
				String Mobile  = sheet3.getRow(i).getCell(1).toString();
				Thread.sleep(2000);
				softassert.assertEquals(Mobile, comp.View_mobile.getText() , " The Mobile does not match ");
				Thread.sleep(3000);
			}

			else if(value.equals("Constitution_of_Business"))
			{
				System.out.println("Entered Constitution of Business");
				Thread.sleep(3000);
				String ConsBussiness  = sheet3.getRow(i).getCell(1).toString();
				softassert.assertEquals(ConsBussiness, comp.View_Constitution.getText() , " The Constitution of Bussiness does not match ");
				Thread.sleep(3000);



			}
			else if(value.equals("City_District"))
			{
				System.out.println("Entered City/District loop");
				Thread.sleep(5000);
				String City  = sheet3.getRow(i).getCell(1).toString();
				softassert.assertEquals(City, comp.View_City.getText() , "The City does not match");
				Thread.sleep(2000);
				break;

			}

		}


		comp.Address_Tab.click();
		Thread.sleep(5000);
		for (int i =0 ; i< sheet4.getLastRowNum() ; i++)
		{
			if(i==0)
			{
				String Add1 = sheet4.getRow(i).getCell(1).toString();
				softassert.assertEquals(Add1, comp.View_Add1.getText() , " The Address on 1st line did not match ");
				Thread.sleep(3000);
			}

			if(i==1)
			{
				String Add2 = sheet4.getRow(i).getCell(1).toString();
				softassert.assertEquals(Add2, comp.View_Add2.getText() , " The Address on 2nd line did not match ");
				Thread.sleep(3000);
			}

			if(i==2)
			{
				String Pincode = sheet4.getRow(i).getCell(1).toString();
				softassert.assertEquals(Pincode, comp.View_Pincode.getText() , " The Pincode does not match ");
				Thread.sleep(3000);
				break;
			}


		}

		softassert.assertEquals("Registered Office", comp.View_Addresstype.getText() , " The Address Type does not match ");
		softassert.assertEquals("Owned", comp.View_Premises.getText() , " The Premises does not match ");
		softassert.assertEquals("Maharashtra", comp.View_StateAddress.getText() , " The State does not match ");
		softassert.assertEquals("Mumbai", comp.View_CityAddress.getText() , " The City does not match ");
		softassert.assertEquals("Factory / Manufacturing", comp.View_BA.getText() , " The Nature of Business Activity  does not match ");



		comp.View_GSTIN_User.click();
		Thread.sleep(3000);


		String GSTIN_username = sheet4.getRow(3).getCell(1).toString();
		softassert.assertEquals(GSTIN_username , comp.View_GSTIN_Username.getText() , "The GSTIN username does not match");
		Thread.sleep(3000);

		GSTIN_Mail = sheet3.getRow(3).getCell(1).toString();
		softassert.assertEquals(GSTIN_Mail , comp.View_GSTIN_Email_ID.getText() , "The GSTIN EmailID does not match");
		Thread.sleep(3000);


		String GSTIN_FirstName = sheet2.getRow(3).getCell(1).toString();
		String GSTIN_LastName = sheet2.getRow(4).getCell(1).toString();
		System.out.println("The name from Excel is : "+GSTIN_FirstName);
		System.out.println("The name from Excel is : "+GSTIN_LastName);
		String FullName = GSTIN_FirstName+" "+GSTIN_LastName;
		System.out.println("The name from Excel is : "+FullName);
		System.out.println("The name from screen is  is : "+ comp.View_Name_GSTN.getText());
		softassert.assertEquals(FullName , comp.View_Name_GSTN.getText() , "The GSTIN Name does not match");
		Thread.sleep(3000);


		String GSTIN_Number = sheet2.getRow(1).getCell(1).toString();
		softassert.assertEquals(GSTIN_Number , comp.View_Number_GSTN.getText() , "The GSTIN Number does not match");
		Thread.sleep(3000);



		String GSTIN_Pan = sheet2.getRow(2).getCell(1).toString();
		softassert.assertEquals(GSTIN_Pan , comp.View_PAN_GSTN.getText() , "The GSTIN PAN does not match");
		Thread.sleep(3000);


		comp.View_GSTRR3B.click();
		for(int i=0 ; i < comp.List_GSTRR3BCompile.size() ; i++)
		{
			softassert.assertEquals(comp.List_GSTRR3BCompile.get(i).getText().trim() , "Compile" ,  "The GSTR3B Compile does not match");
			Thread.sleep(2000);
		}
		comp.View_OtherConfig.click();
		softassert.assertAll();
	}

	public void ViewOtherConfigDeselected() throws Exception
	{
		Thread.sleep(5000);
		softassert.assertEquals("No" , comp.View_HSNSummary_Value.getText() , "The HSNSummary 'No' does not match");
		softassert.assertEquals("No" , comp.View_ITC_Value.getText() , "The ITC 'No' does not match");
		Thread.sleep(4000);
		comp.View_Close.click();
		softassert.assertAll();
	}


	public void ViewOtherConfigSelected() throws Exception
	{
		Thread.sleep(5000);
		softassert.assertEquals("Yes" , comp.View_HSNSummary_Value.getText() , "The HSNSummary 'Yes' does not match");
		softassert.assertEquals("Yes" , comp.View_ITC_Value.getText() , "The ITC 'Yes' does not match");
		Thread.sleep(4000);
		comp.View_Close.click();
		softassert.assertAll();
	}




	public void EditCompany() throws Exception
	{

		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet2 = (XSSFSheet) wb.getSheetAt(1);
		sheet3 = (XSSFSheet) wb.getSheetAt(2);
		sheet4 = (XSSFSheet) wb.getSheetAt(3);
		sheet5 = (XSSFSheet) wb.getSheetAt(4);
		int Value = comp.CompCreatnList.size();
		String Company =  comp.CompCreatnList.get(Value-1).getText().trim();
		String Tradename = null;

		for(int a=0 ; a< comp.CompCreatnList.size() ; a++)
		{
			Tradename  = sheet3.getRow(4).getCell(1).toString();
			System.out.println(Tradename);
			if(comp.CompCreatnList.get(a).getText().equalsIgnoreCase(Tradename))
			{

				Thread.sleep(5000);
				comp.CompListEdit.get(a).click();
				System.out.println("The company from company page is : "+comp.CompCreatnList.get(a).getText());
				softassert.assertEquals(Company, Tradename , "The company is not Viewed Successfully & not present in company list");
				Thread.sleep(5000);
				break;
			}
		}



		System.out.println("The attribute is  : "+comp.Edit_AccGrp.getAttribute("readonly"));

		String GSTIN = null;
		for(int i=0 ; i <sheet5.getLastRowNum() ; i++)

		{

			String value  = sheet5.getRow(i).getCell(0).toString();
			System.out.println("The value is : "+ value);

			if(value.equalsIgnoreCase("Account/Group Name"))
			{
				if(comp.Edit_AccGrp.getAttribute("readonly").equals("true"))
				{

					softassert.assertTrue(comp.Edit_AccGrp.getAttribute("readonly").contains("true"), "The test case did not pass");

				}

				else
				{
					comp.Edit_AccGrp.sendKeys("TestValidation");
					System.out.println(" Testcase Failed");
					softassert.fail("Field validation has failed");
				}


			}

			else if(value.equalsIgnoreCase("Company Name"))
			{

				if(comp.Edit_CompanyName.getAttribute("readonly").equals("true"))
				{
					System.out.println(comp.Edit_CompanyName.getAttribute("readonly"));
					softassert.assertTrue(comp.Edit_CompanyName.getAttribute("readonly").contains("true"), "The test case did not pass");

				}

				else
				{
					comp.Edit_CompanyName.sendKeys("TestValidation");
					System.out.println(" Testcase Failed");
					softassert.fail("Field validation has failed");
				}
				Thread.sleep(2000);
			}


			else if(value.equalsIgnoreCase("Turnover_2016_2017"))
			{
				String Turnover1  = sheet5.getRow(i).getCell(1).toString();
				System.out.println("TURNOVER IS : "+Turnover1);
				Thread.sleep(5000);
				comp.Edit_TurnOver16_17.clear();
				Thread.sleep(4000);
				comp.Edit_TurnOver16_17.sendKeys(Turnover1);
				Thread.sleep(4000);
			}


			else if(value.equalsIgnoreCase("Registered Email-id"))
			{
				String Emailid  = sheet5.getRow(i).getCell(1).toString();
				comp.Edit_Registered_Email_id.clear();
				Thread.sleep(2000);
				comp.Edit_Registered_Email_id.sendKeys(Emailid);
				Thread.sleep(3000);
			}

			else if(value.equalsIgnoreCase("Trade Name/Alias"))
			{
				Tradename  = sheet5.getRow(i).getCell(1).toString();
				comp.Edit_TradeName.clear();
				Thread.sleep(2000);
				comp.Edit_TradeName.sendKeys(Tradename);
				Thread.sleep(2000);
			}

			else if(value.equals("State_Type"))
			{
				System.out.println("Entered State Loop");
				Thread.sleep(4000);
				String EditState = comp.Edit_State_Present.getText().trim();
				System.out.println("The state is : "+EditState);
				Thread.sleep(5000);
				String StateExcel  = sheet3.getRow(i).getCell(1).toString();
				softassert.assertEquals(EditState, StateExcel , "State is non- editable");		
				softassert.assertAll();
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("GSTIN_Type"))
			{
				Thread.sleep(3000);
				System.out.println("The GSTIN is  : "+GSTIN);

				if(comp.Edit_GSTIN.getAttribute("readonly").equals("true"))
				{

					softassert.assertTrue(comp.Edit_GSTIN.getAttribute("readonly").contains("true"), "The test case did not pass");
					softassert.assertAll();
				}

				else
				{
					comp.Edit_GSTIN.sendKeys("GSTINFAILED");
					System.out.println(" Testcase Failed");
					softassert.fail("Field validation has failed");
					softassert.assertAll();
				}
				Thread.sleep(3000);
			}
			else if(value.equalsIgnoreCase("Pan_Type"))
			{
				if(comp.Edit_PAN.getAttribute("readonly").equals("true"))
				{

					softassert.assertTrue(comp.Edit_PAN.getAttribute("readonly").contains("true"), "The test case did not pass");
					softassert.assertAll();

				}

				else
				{
					comp.Edit_PAN.sendKeys("PANFAILED");
					System.out.println(" Testcase Failed");
					softassert.fail("Field validation has failed");
				}

				Thread.sleep(3000);
			}
			else if(value.equalsIgnoreCase("TurnoverType"))
			{
				Thread.sleep(2000);
				String TurnOver_AprilJune2017  = sheet5.getRow(i).getCell(1).toString();
				Thread.sleep(2000);
				comp.Edit_TurnOver_AprilJune2017.clear();
				Thread.sleep(2000);
				comp.Edit_TurnOver_AprilJune2017.sendKeys(TurnOver_AprilJune2017);
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Registered_Mobile"))
			{
				String Mobile  = sheet5.getRow(i).getCell(1).toString();
				Thread.sleep(3000);
				comp.Edit_Mobile.clear();
				Thread.sleep(3000);
				comp.Edit_Mobile.sendKeys(Mobile);
				Thread.sleep(3000);
			}

			else if(value.equals("Constitution_of_Business"))
			{
				System.out.println("Entered Constitution of Business");
				Thread.sleep(3000);

				if(comp.Edit_ConstOfBussiness.getAttribute("readonly").equals("true"))
				{

					softassert.assertTrue(comp.Edit_ConstOfBussiness.getAttribute("readonly").contains("true"), "The test case did not pass");
					softassert.assertAll();

				}

				else
				{
					comp.Edit_ConstOfBussiness.sendKeys("FAILED");
					System.out.println(" Testcase Failed");
					softassert.fail("Field validation has failed");
				}
				Thread.sleep(2000);
			}
			else if(value.equals("City_District"))
			{
				System.out.println("Entered City/District loop");
				Thread.sleep(2000);
				comp.Edit_City_District_Dropdown.click();
				Thread.sleep(3000);

				for(int j=0 ; j<comp.Edit_ListofCity_District.size() ; j++)
				{

					if(comp.Edit_ListofCity_District.get(j).getText().equalsIgnoreCase(sheet5.getRow(i).getCell(1).toString()))
					{
						Thread.sleep(2000);
						comp.Edit_ListofCity_District.get(j).click();
						Thread.sleep(4000);
						break;
					}

				}
				
				break;
			}

		}	
		Thread.sleep(2000);
		comp.Edit_Address_Tab.click();
		Thread.sleep(3000);
		sheet5 = (XSSFSheet) wb.getSheetAt(4);
		for (int i =0 ; i< sheet5.getLastRowNum() ; i++)
		{
			if(i==12)
			{
				comp.Edit_address1.clear();
				Thread.sleep(3000);
				comp.Edit_address1.sendKeys(sheet5.getRow(i).getCell(1).toString());
				Thread.sleep(3000);
			}

			if(i==13)
			{
				comp.Edit_address2.clear();
				Thread.sleep(3000);
				comp.Edit_address2.sendKeys(sheet5.getRow(i).getCell(1).toString());
				Thread.sleep(3000);
			}

			if(i==14)
			{	
				comp.Edit_pincode_address.clear();
				Thread.sleep(3000);
				comp.Edit_pincode_address.sendKeys(sheet5.getRow(i).getCell(1).toString());
				Thread.sleep(3000);
				break;
			}

		}

		Thread.sleep(5000);
		comp.Edit_dropdown_address.click();
		Thread.sleep(5000);
		comp.Edit_ListddlAddress.get(3).click();


		Thread.sleep(4000);
		comp.Edit_dropdown_Premises.click();
		Thread.sleep(2000);
		comp.Edit_ListddlPremises.get(1).click();


		Thread.sleep(2000);
		comp.Edit_dropdown_States.click();
		Thread.sleep(2000);
		comp.Edit_ListddlStates.get(9).click();


		Thread.sleep(2000);
		comp.Edit_dropdown_City.click();
		Thread.sleep(2000);
		comp.Edit_ListddlCity.get(0).click();


		Thread.sleep(2000);
		comp.Edit_dropdown_BussinessActvty.click();
		Thread.sleep(2000);
		comp.Edit_ListBussinessActvty.get(1).click();

		Thread.sleep(4000);
		comp.Edit_Address_next.click();
		Thread.sleep(3000);

		comp.Edit_GSTN_username.clear();
		Thread.sleep(2000);
		comp.Edit_GSTN_username.sendKeys(sheet5.getRow(15).getCell(1).toString());
		Thread.sleep(3000);
		comp.Edit_GSTN_username1.click();
		Thread.sleep(3000);
		comp.Edit_GSTN_users.get(0).click();
		Thread.sleep(4000);
		softassert.assertTrue(comp.Edit_Name_GSTN.isDisplayed(), "The Name is not Displayed");
		softassert.assertTrue(comp.Edit_Number_GSTN.isDisplayed(), "The Number is not Displayed");
		softassert.assertTrue(comp.Edit_PAN_GSTN.isDisplayed(), "The PAN is not Displayed");
		Thread.sleep(2000);
		comp.Edit_GSTUser_Next_GSTN.click();
		Thread.sleep(3000);
		
		/*
		 * GSTR3B
		 */
		
		for(int i =0 ; i< comp.Edit_GSTR3B_List.size() ; i++)
		{

			Thread.sleep(5000);
			comp.Edit_GSTR3B_List.get(i).click();
			Thread.sleep(6000);
			comp.EditDirectInput_GSTR3B_List.get(i).click();
			Thread.sleep(5000);

		}

		comp.Edit_Next_GSTR3B.click();
		Thread.sleep(5000);
		softassert.assertAll();

		if((comp.Edit_HSN_checkbox.isSelected() && comp.Edit_ITC_checkbox.isSelected()))
		{
			comp.Edit_HSN_checkbox.click();	
			Thread.sleep(2000);
			comp.Edit_ITC_checkbox.click();
			Thread.sleep(2000);
			softassert.assertFalse(comp.Edit_HSN_checkbox.isSelected(), "The CheckBox is not selected");
			softassert.assertFalse(comp.Edit_ITC_checkbox.isSelected(), "The CheckBox is not selected");
		}

		else if (!(comp.Edit_HSN_checkbox.isSelected() && comp.Edit_ITC_checkbox.isSelected()))
		{
			comp.Edit_HSN_checkbox.click();	
			Thread.sleep(2000);
			comp.Edit_ITC_checkbox.click();
			Thread.sleep(2000);
			softassert.assertTrue(comp.Edit_HSN_checkbox.isSelected(), "The CheckBox is not selected");
			softassert.assertTrue(comp.Edit_ITC_checkbox.isSelected(), "The CheckBox is not selected");

		}

		comp.Edit_Save_AddCompany.click();
		Thread.sleep(8000);

		System.out.println("the msg : "+comp.CompanyUpdated_Success.getText());
		softassert.assertEquals(comp.CompanyUpdated_Success.getText(), "Company Updated successfully.." , "The company successfully created is not visible");
		Thread.sleep(4000);
		comp.Edit_OK.click();
		Thread.sleep(6000);

		softassert.assertAll();

	}

	public void VerifyCompanyEdited() throws Exception
	{
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet5 = (XSSFSheet) wb.getSheetAt(4);

		String Tradename = null;
		for(int i=0 ; i <= sheet5.getLastRowNum() ; i++)
		{
			String value = sheet5.getRow(i).getCell(0).toString();
			if(value.equalsIgnoreCase("Trade Name/Alias"))
			{
				Tradename  = sheet5.getRow(i).getCell(1).toString();
				System.out.println("The company name from excel is : "+Tradename);
				Thread.sleep(2000);
				break;
			}
		}
		int Value = comp.CompCreatnList.size();
		String Company =  comp.CompCreatnList.get(Value-1).getText().trim();
		softassert.assertEquals(Company, Tradename , "The company is not created Successfully & not present in list");
		softassert.assertAll();
	}



	public void AddLocation() throws Exception
	{
		softassert.assertEquals(comp.VerifyLocation.getText(), "Location(s)" , "The Location is not verified");
		Thread.sleep(3000);
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet6 = (XSSFSheet) wb.getSheetAt(5);
		sheet2 = (XSSFSheet) wb.getSheetAt(1);
		sheet3 = (XSSFSheet) wb.getSheetAt(2);
		sheet6 = (XSSFSheet) wb.getSheetAt(5);
		int Value = comp.CompCreatnList.size();
		Thread.sleep(3000);
		String Company =  comp.CompCreatnList.get(Value-1).getText().trim();
		System.out.println("The selected company is : "+Company);
		comp.CompCreatnList.get(Value-1).click();
		Thread.sleep(3000);
		comp.AddLocation.click();
		Thread.sleep(3000);
		comp.SelectCompanyDDLforLoc1.click();
		Thread.sleep(3000);
		for(int a=0 ; a< comp.ListOfCompanies.size() ; a++)
		{
			if(comp.ListOfCompanies.get(a).getText().equalsIgnoreCase(sheet6.getRow(0).getCell(1).toString()))
			{
				Thread.sleep(3000);
				System.out.println("The Location from is : "+comp.ListOfCompanies.get(a).getText());
				comp.ListOfCompanies.get(a).click();
				break;
			}
		}

		String GSTIN = null;
		for(int i=0 ; i<sheet6.getLastRowNum() ; i++)

		{
			String value  = sheet6.getRow(i).getCell(0).toString();
			System.out.println("The value is : "+ value);

			Thread.sleep(2000);
			String LegalBussiness=null;
			if(value.equalsIgnoreCase("Company_Name"))
			{
				comp.SelectCompanyDDLforLoc1.click();
				for(int b =0 ; b<comp.ListOfCompanies.size() ; b++)
				{

					if(comp.ListOfCompanies.get(b).getText().equalsIgnoreCase(sheet6.getRow(i).getCell(1).toString()))
					{
						LegalBussiness = comp.ListOfCompanies.get(b).getText();
						comp.ListOfCompanies.get(b).click();
						Thread.sleep(4000);
						break;
					}

				}

			}

			else if(value.equalsIgnoreCase("LegalNameOfBussiness"))
			{

				if(comp.VerifyLegalBussinesName_Loc1.getAttribute("readonly").contains("true"))
				{
					softassert.assertTrue(comp.VerifyLegalBussinesName_Loc1.getAttribute("readonly").contains("true"), "This field is non editable");

				}
				else
				{
					softassert.fail("LegalNameOfBussiness is editable");
				}

				Thread.sleep(2000);
			}
			else if(value.equalsIgnoreCase("Turnover_2016_2017"))
			{
				String Loc_Turnover1  = sheet6.getRow(i).getCell(1).toString();
				comp.Loc1_TurnOver16_17.sendKeys(Loc_Turnover1);
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Registered_Email_id"))
			{
				String Loc_Emailid  = sheet6.getRow(i).getCell(1).toString();
				comp.Loc1_Registered_Email_id.sendKeys(Loc_Emailid);
				Thread.sleep(3000);
			}

			else if(value.equalsIgnoreCase("Trade Name/Alias"))
			{
				String Loc_Tradename  = sheet6.getRow(i).getCell(1).toString();
				comp.Loc1_TradeName.sendKeys(Loc_Tradename);
				Thread.sleep(2000);
			}

			else if(value.equals("State_Type"))
			{
				System.out.println("Entered State Loop");
				comp.Loc1_State_Dropdown.click();
				Thread.sleep(3000);
				for(int c=0 ; c<comp.Loc1_ListofStates.size() ; c++)
				{
					if(comp.Loc1_ListofStates.get(c).getText().equalsIgnoreCase(sheet6.getRow(i).getCell(1).toString()))
					{
						Thread.sleep(2000);
						comp.Loc1_ListofStates.get(c).click();
						Thread.sleep(2000);
						break;
					}

				}
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("GSTIN_Type"))
			{
				GSTIN  = sheet6.getRow(i).getCell(1).toString();
				System.out.println("The GSTIN is  : "+GSTIN);
				Thread.sleep(2000);
				comp.Loc1_GSTIN.sendKeys(GSTIN);
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Pan_Type"))
			{
				Thread.sleep(2000);
				softassert.assertTrue(comp.Loc1_PAN.isDisplayed(), "The Legal name is not displayed");

			}
			else if(value.equalsIgnoreCase("TurnoverType"))
			{
				Thread.sleep(2000);
				String Loc_TurnOver_AprilJune2017  = sheet6.getRow(i).getCell(1).toString();
				Thread.sleep(5000);
				comp.Loc1_TurnOver_AprilJune2017.sendKeys(Loc_TurnOver_AprilJune2017);
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Registered_Mobile"))
			{
				String Loc_Mobile  = sheet6.getRow(i).getCell(1).toString();
				Thread.sleep(3000);
				comp.Loc1_Mobile.sendKeys(Loc_Mobile);
				Thread.sleep(3000);
			}

			else if(value.equals("Constitution_of_Business"))
			{
				System.out.println("Entered Constitution of Business");
				softassert.assertTrue(comp.Loc1_ConstOfBussiness.isDisplayed(), "Constitution of Bussiness is not displayed");
				Thread.sleep(2000);
			}
			else if(value.equals("City_District"))
			{
				System.out.println("Entered City/District loop");
				comp.Loc1_City_District_Dropdown.click();
				Thread.sleep(2000);

				for(int j=0 ; j<comp.Loc1_ListofCity_District.size() ; j++)
				{

					if(comp.Loc1_ListofCity_District.get(j).getText().equalsIgnoreCase(sheet6.getRow(i).getCell(1).toString()))
					{
						Thread.sleep(2000);
						comp.Loc1_ListofCity_District.get(j).click();
						Thread.sleep(2000);
						break;
					}

				}

			}

			else
			{
				System.out.println("Fail to take data from excel");
			}

		}

		comp.Loc1_AddressTab.click();
		softassert.assertAll();

	}


	public void AddAddressLoc() throws Exception 
	{
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet6 = (XSSFSheet) wb.getSheetAt(5);
		for (int i =0 ; i< sheet6.getLastRowNum() ; i++)
		{
			if(i==12)
			{
				comp.Loc1_address1.sendKeys(sheet6.getRow(i).getCell(1).toString());
				Thread.sleep(2000);
			}

			if(i==13)
			{
				comp.Loc1_address2.sendKeys(sheet6.getRow(i).getCell(1).toString());
				Thread.sleep(2000);
			}

			if(i==14)
			{
				comp.Loc1_pincode_address.sendKeys(sheet6.getRow(i).getCell(1).toString());
				Thread.sleep(2000);
				break;
			}

		}

		comp.Loc1_dropdown_address.click();
		Thread.sleep(2000);
		comp.Loc1_ListddlAddress.get(0).click();


		Thread.sleep(2000);
		comp.Loc1_dropdown_Premises.click();
		Thread.sleep(2000);
		comp.Loc1_ListddlPremises.get(0).click();


		Thread.sleep(2000);
		comp.Loc1_dropdown_States.click();
		Thread.sleep(2000);
		comp.Loc1_ListddlStates.get(20).click();


		Thread.sleep(2000);
		comp.Loc1_dropdown_City.click();
		Thread.sleep(2000);
		comp.Loc1_ListddlCity.get(233).click();


		Thread.sleep(2000);
		comp.Loc1_dropdown_BussinessActvty.click();
		Thread.sleep(5000);
		comp.Loc1_ListBussinessActvty.get(0).click();

		Thread.sleep(3000);
		comp.Loc1_Next_to_GST_User.click();
		softassert.assertAll();

	}


	public void addGSTNLoc() throws Exception

	{
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet6 = (XSSFSheet) wb.getSheetAt(5);
		Thread.sleep(3000);
		comp.Loc1_GSTN_username.sendKeys(sheet6.getRow(15).getCell(1).toString());
		Thread.sleep(2000);
		comp.Loc1_GSTN_username1.click();
		Thread.sleep(2000);
		comp.Loc1_GSTN_users.get(0).click();
		Thread.sleep(2000);
		softassert.assertTrue(comp.Loc1_Name_GSTN.isDisplayed(), "The Name of location 1 is not Displayed");
		softassert.assertTrue(comp.Loc1_Number_GSTN.isDisplayed(), "The Number of location 1 is not Displayed");
		softassert.assertTrue(comp.Loc1_PAN_GSTN.isDisplayed(), "The PAN of location 1 is not Displayed");
		comp.Loc1_Next_GSTR3B.click();
		Thread.sleep(2000);
		/*
		 * GSTR3B
		 */
		for(int i =0 ; i< comp.GSTR3B_Loc01_List.size() ; i++)
		{

			Thread.sleep(3000);
			comp.GSTR3B_Loc01_List.get(i).click();
			Thread.sleep(3000);
			comp.GSTR3B_Loc01_Compile.get(i).click();
			Thread.sleep(3000);
		}

		comp.Loc1_Next_HSN.click();
		Thread.sleep(3000);

		softassert.assertAll();
	}


	public void HSN_ITC_Loc() throws Exception
	{

		if(comp.Loc1_HSN_checkbox.isSelected())
		{

			softassert.assertTrue(comp.Loc1_HSN_checkbox.isSelected(), "1.The CheckBox is not selected");

		}

		else
		{
			softassert.assertFalse(comp.Loc1_HSN_checkbox.isSelected(), "2.The CheckBox is not selected");

		}



		if(comp.Loc1_ITC_checkbox.isSelected())
		{
			softassert.assertTrue(comp.Loc1_ITC_checkbox.isSelected(), "3.The CheckBox is not selected");

		}

		else
		{
			softassert.assertFalse(comp.Loc1_ITC_checkbox.isSelected(), "4.The CheckBox is not selected");

		}

		Thread.sleep(7000);
		comp.Save_AddLoc_1.click();
		Thread.sleep(5000);
		comp.Location_1_Created.click();
		Thread.sleep(7000);
		softassert.assertAll();

	}
	public void VerifyLoc01() throws Exception
	{
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet6 = (XSSFSheet) wb.getSheetAt(5);
		String Tradename = null;
		for(int i=0 ; i <sheet6.getLastRowNum() ; i++)
		{
			String value = sheet6.getRow(i).getCell(0).toString();
			if(value.equalsIgnoreCase("Trade Name/Alias"))
			{
				Tradename  = sheet6.getRow(i).getCell(1).toString();
				System.out.println("The Location name from excel is : "+Tradename);
				Thread.sleep(2000);
				break;
			}
		}
		int Value = comp.CompCreatnList.size();
		String Company =  comp.CompCreatnList.get(Value-1).getText().trim();
		comp.CompCreatnList.get(Value-1).click();
		Thread.sleep(3000);
		int Value1 = comp.Loc_1_CreatnList.size();
		String Location =  comp.Loc_1_CreatnList.get(Value1-1).getText().trim();
		System.out.println(Location);
		softassert.assertEquals(Location, Tradename , "The Location is not created Successfully & not present in list");
		softassert.assertAll();
	}

	public void ViewLocation() throws Exception
	{
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet6 = (XSSFSheet) wb.getSheetAt(5);
		sheet2 = (XSSFSheet) wb.getSheetAt(1);
		sheet3 = (XSSFSheet) wb.getSheetAt(2);

		String Tradename = null;
		for(int i=0 ; i <= sheet6.getLastRowNum() ; i++)
		{
			String value = sheet6.getRow(i).getCell(0).toString();
			if(value.equalsIgnoreCase("Trade Name/Alias"))
			{
				Tradename  = sheet6.getRow(i).getCell(1).toString();
				System.out.println("The TradeName from excel is : "+Tradename);
				Thread.sleep(3000);
				break;
			}
		}

		int Value = comp.CompCreatnList.size();
		String Company =  comp.CompCreatnList.get(Value-1).getText().trim();
		System.out.println("The selected company is : "+Company);
		comp.CompCreatnList.get(Value-1).click();
		Thread.sleep(3000);
		int Value1 = comp.Loc_1_CreatnList.size();
		String Location =  comp.Loc_1_CreatnList.get(Value1-1).getText().trim();
		System.out.println("The selected Location is : "+Location);

		for(int a=0 ; a<comp.Loc_1_CreatnList.size() ; a++)
		{
			if(comp.Loc_1_CreatnList.get(a).getText().equalsIgnoreCase(Tradename))
			{

				Thread.sleep(5000);
				comp.Loc_1CreatnListView.get(a).click();
				System.out.println("The company from company page is : "+comp.Loc_1_CreatnList.get(a).getText());
				softassert.assertEquals(Location, Tradename , "The Location is not Viewed Successfully & not present in company list");
				softassert.assertAll();
				Thread.sleep(5000);
				break;
			}
		}
		String CompName = null;
		String GSTIN = null;
		for(int i=0 ; i <sheet6.getLastRowNum() ; i++)

		{

			String value  = sheet6.getRow(i).getCell(0).toString();
			System.out.println("The value is : "+ value);

			if(value.equalsIgnoreCase("Company_Name"))
			{
				Thread.sleep(4000);
				CompName  = sheet6.getRow(i).getCell(1).toString();
				System.out.println("The Company from excel is : "+CompName);
				Thread.sleep(2000);
				System.out.println("The Company of group from screen is : "+comp.View_Loc1_Company.getText());
				softassert.assertEquals(CompName, comp.View_Loc1_Company.getText() , " The Company name does not match ");

			}

			else if(value.equalsIgnoreCase("LegalNameOfBussiness"))
			{

				softassert.assertEquals(CompName, comp.View_Loc1_bussiness.getText() , " The NameofBussiness  does not match ");
				Thread.sleep(2000);
			}


			else if(value.equalsIgnoreCase("Turnover_2016_2017"))
			{
				String Turnover1  = sheet6.getRow(i).getCell(1).toString();
				Thread.sleep(2000);
				softassert.assertEquals(Turnover1, comp.View_Loc1_turnover.getText() , " The Turnover does not match ");
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Registered_Email_id"))
			{
				String Emailid  = sheet6.getRow(i).getCell(1).toString();
				Thread.sleep(2000);
				softassert.assertEquals(Emailid, comp.View_Loc1_emailid.getText() , " The Email-ID does not match ");
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Trade Name/Alias"))
			{
				Tradename  = sheet6.getRow(i).getCell(1).toString();
				Thread.sleep(2000);
				softassert.assertEquals(Tradename, comp.View_Loc1_Tradename.getText() , " The TradeName  does not match ");
				Thread.sleep(2000);

			}

			else if(value.equals("State_Type"))
			{
				System.out.println("Entered State Loop");
				String State  = sheet6.getRow(i).getCell(1).toString();
				softassert.assertEquals(State, comp.View_Loc1_State.getText() , " The State does not match ");
				Thread.sleep(2000);

			}

			else if(value.equalsIgnoreCase("GSTIN_Type"))
			{
				GSTIN  = sheet6.getRow(i).getCell(1).toString();
				Thread.sleep(3000);
				System.out.println("The GSTIN is  : "+GSTIN);
				softassert.assertEquals(GSTIN, comp.View_GSTIN_Loc1_State.getText() , " The GSTIN does not match ");
				Thread.sleep(2000);
			}
			else if(value.equalsIgnoreCase("Pan_Type"))
			{
				String Pan  = GSTIN.substring(2,12);
				Thread.sleep(2000);
				System.out.println("The PAN from GSTIN "+GSTIN+" is "+Pan);
				softassert.assertEquals(Pan, comp.View_Pan_Loc1_State.getText() , " The PAN does not match ");
				Thread.sleep(2000);
			}
			else if(value.equalsIgnoreCase("TurnoverType"))
			{
				Thread.sleep(2000);
				String TurnOver_AprilJune2017  = sheet6.getRow(i).getCell(1).toString();
				softassert.assertEquals(TurnOver_AprilJune2017, comp.View_turnoverApr_Loc1_June.getText() , " The TurnoverAprJune does not match ");
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Registered_Mobile"))
			{
				String Mobile  = sheet6.getRow(i).getCell(1).toString();
				Thread.sleep(2000);
				softassert.assertEquals(Mobile, comp.View_Loc1_mobile.getText() , " The Mobile does not match ");
				Thread.sleep(3000);
			}

			else if(value.equals("Constitution_of_Business"))
			{
				Thread.sleep(3000);
				String ConsBussiness  = sheet6.getRow(i).getCell(1).toString();
				softassert.assertEquals(ConsBussiness, comp.View_Loc1_Constitution.getText() , " The Constitution of Bussiness does not match ");
				Thread.sleep(3000);

			}
			else if(value.equals("City_District"))
			{
				Thread.sleep(3000);
				String City  = sheet6.getRow(i).getCell(1).toString();
				softassert.assertEquals(City, comp.View_Loc1_City.getText() , "The City does not match");
				Thread.sleep(2000);

			}

		}

		Thread.sleep(4000);
		comp.View_Address_Loc1_Tab.click();
		Thread.sleep(5000);
		for (int i =0 ; i< sheet6.getLastRowNum(); i++)
		{

			if(sheet6.getRow(i).getCell(0).toString().equalsIgnoreCase("Address_Line_1"))
			{
				String Add1 = sheet6.getRow(i).getCell(1).toString();
				System.out.println(Add1);
				softassert.assertEquals(Add1, comp.View_Add1_Loc_1.getText() , " The Address on 1st line did not match ");
				Thread.sleep(3000);
			}

			if(sheet6.getRow(i).getCell(0).toString().equalsIgnoreCase("Address_Line_2"))
			{
				String Add2 = sheet6.getRow(i).getCell(1).toString();
				System.out.println(Add2);
				softassert.assertEquals(Add2, comp.View_Add2_Loc_1.getText() , " The Address on 2nd line did not match ");
				Thread.sleep(3000);
			}

			if(sheet6.getRow(i).getCell(0).toString().equalsIgnoreCase("Pincode"))
			{
				String Pincode = sheet6.getRow(i).getCell(1).toString();
				System.out.println(Pincode);
				softassert.assertEquals(Pincode, comp.View_Pincode_Loc_1.getText() , " The Pincode does not match ");
				Thread.sleep(3000);
				break;
			}


		}

		softassert.assertEquals("Registered Office",    comp.View_Addresstype_Loc_1.getText() , " The Address Type does not match ");
		softassert.assertEquals("Owned",	 comp.View_Premises_Loc_1.getText() , " The Premises does not match ");
		softassert.assertEquals("Maharashtra",	 comp.View_StateAddress_Loc_1.getText() , " The State does not match ");
		softassert.assertEquals("Mumbai",	 comp.View_CityAddress_Loc_1.getText() , " The City does not match ");
		softassert.assertEquals("Factory / Manufacturing", 	comp.View_BA_Loc_1.getText() , " The Nature of Business Activity  does not match ");



		comp.View_GSTIN_User_Loc_1.click();
		Thread.sleep(3000);


		String GSTIN_username = sheet6.getRow(15).getCell(1).toString();
		softassert.assertEquals(GSTIN_username , comp.View_GSTIN_Username_Loc_1.getText() , "The GSTIN username does not match");
		Thread.sleep(3000);

		String GSTIN_Mail_1 = sheet3.getRow(3).getCell(1).toString();
		System.out.println(GSTIN_Mail_1);
		softassert.assertEquals(GSTIN_Mail_1 , comp.View_GSTIN_Email_ID_Loc_1.getText() , "The GSTIN EmailID does not match");
		Thread.sleep(3000);


		String GSTIN_FirstName = sheet2.getRow(3).getCell(1).toString();
		String GSTIN_LastName = sheet2.getRow(4).getCell(1).toString();
		System.out.println("The name from Excel is : "+GSTIN_FirstName);
		System.out.println("The name from Excel is : "+GSTIN_LastName);
		String FullName = GSTIN_FirstName+" "+GSTIN_LastName;
		System.out.println("The name from Excel is : "+FullName);
		System.out.println("The name from screen is  is : "+ comp.View_Name_GSTN_Loc_1.getText());
		softassert.assertEquals(FullName , comp.View_Name_GSTN_Loc_1.getText() , "The GSTIN Name does not match");
		Thread.sleep(3000);


		String GSTIN_Number = sheet2.getRow(1).getCell(1).toString();
		softassert.assertEquals(GSTIN_Number , comp.View_Number_GSTN_Loc_1.getText() , "The GSTIN Number does not match");
		Thread.sleep(3000);



		String GSTIN_Pan = sheet2.getRow(2).getCell(1).toString();
		softassert.assertEquals(GSTIN_Pan , comp.View_PAN_GSTN_Loc_1.getText() , "The GSTIN PAN does not match");
		Thread.sleep(3000);


		comp.View_GSTRR3B_Loc_1.click();

		for(int i=0 ; i < comp.List_Loc_GSTRR3BCompile.size() ; i++)
		{
			softassert.assertEquals(comp.List_Loc_GSTRR3BCompile.get(i).getText().trim() , "Compile" ,  "The Location GSTR3B Compile does not match");
			Thread.sleep(2000);
		}
		comp.Loc_1_View_OtherConfig.click();
		softassert.assertAll();
	}


	public void Location_1_ViewOtherConfigDeselected() throws Exception
	{			
		Thread.sleep(4000);
		System.out.println("HSN :"+comp.Loc_1_View_HSNSummary_Value.getText());
		System.out.println("ITC :"+comp.Loc_1_View_ITC_Value.getText());
		softassert.assertEquals("No" , comp.Loc_1_View_HSNSummary_Value.getText() , "The HSNSummary 'No' does not match");
		softassert.assertEquals("No" , comp.Loc_1_View_ITC_Value.getText() , "The ITC 'No' does not match");
		Thread.sleep(3000);
		comp.Loc_1_View_Close.click();
		Thread.sleep(3000);
		softassert.assertAll();

	}	


	public void EditCompany_Location_01() throws Exception
	{

		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet2 = (XSSFSheet) wb.getSheetAt(1);
		sheet3 = (XSSFSheet) wb.getSheetAt(2);
		sheet4 = (XSSFSheet) wb.getSheetAt(3);
		sheet5 = (XSSFSheet) wb.getSheetAt(4);
		sheet6 = (XSSFSheet) wb.getSheetAt(5);
		sheet7 = (XSSFSheet) wb.getSheetAt(6);
		int Value = comp.CompCreatnList.size();
		String Company =  comp.CompCreatnList.get(Value-1).getText().trim();
		System.out.println("The selected company is : "+Company);
		comp.CompCreatnList.get(Value-1).click();
		Thread.sleep(3000);
		int Value1 = comp.Loc_1_CreatnList.size();
		String Location =  comp.Loc_1_CreatnList.get(Value1-1).getText().trim();
		System.out.println("The selected Location is : "+Location);

		String Tradename = null;


		for(int a=0 ; a< comp.Loc_1_CreatnList.size() ; a++)
		{
			if(comp.Loc_1_CreatnList.get(a).getText().equalsIgnoreCase(sheet6.getRow(4).getCell(1).toString()))
			{
				Thread.sleep(5000);
				comp.Loc_1CreatnListEdit.get(a).click();
				System.out.println("The location from company page is : "+comp.Loc_1_CreatnList.get(a).getText());
				softassert.assertEquals(Location, sheet6.getRow(4).getCell(1).toString() , "The location is not Viewed Successfully & not present in company list");
				Thread.sleep(5000);
				break;
			}
		}

		String GSTIN = null;
		for(int i=0 ; i <= sheet7.getLastRowNum() ; i++)

		{

			String value  = sheet7.getRow(i).getCell(0).toString();
			System.out.println("The value is : "+ value);

			if(value.equalsIgnoreCase("Company_Name"))
			{
				if(comp.Loc_1_Edit_CompanyName.getAttribute("readonly").equals("true"))
				{

					softassert.assertTrue(comp.Loc_1_Edit_CompanyName.getAttribute("readonly").contains("true"), "The Company Name did not pass");;

				}

				else
				{
					comp.Loc_1_Edit_CompanyName.sendKeys("TestValidation");
					System.out.println(" Testcase Failed");
					softassert.fail("Company Name Field validation has failed");
				}

			}

			else if(value.equalsIgnoreCase("LegalNameOfBussiness"))
			{
				String NameofBussiness  = sheet7.getRow(i).getCell(1).toString();
				if(comp.Loc_1_Edit_NameofBussiness.getAttribute("readonly").equals("true"))
				{

					softassert.assertTrue(comp.Loc_1_Edit_NameofBussiness.getAttribute("readonly").contains("true"), " NameofBussiness The test case did not pass");

				}

				else
				{
					comp.Loc_1_Edit_NameofBussiness.sendKeys("TestValidation");
					System.out.println(" Testcase Failed");
					softassert.fail("NameofBussiness Field validation has failed");
				}
				Thread.sleep(2000);

			}


			else if(value.equalsIgnoreCase("Turnover_2016_2017"))
			{
				String Turnover1  = sheet7.getRow(i).getCell(1).toString();
				System.out.println("TURNOVER IS 2016_2017 : "+Turnover1);
				Thread.sleep(4000);
				comp.Loc_1_Edit_TurnOver16_17.clear();
				Thread.sleep(4000);
				comp.Loc_1_Edit_TurnOver16_17.sendKeys(Turnover1);
				Thread.sleep(4000);
			}


			else if(value.equalsIgnoreCase("RegisteredEmail_id"))
			{
				System.out.println("Into Email ID");
				String Emailid  = sheet7.getRow(i).getCell(1).toString();
				comp.Loc_1_Edit_Registered_Email_id.clear();
				Thread.sleep(4000);
				comp.Loc_1_Edit_Registered_Email_id.sendKeys(Emailid);
				Thread.sleep(3000);
			}

			else if(value.equalsIgnoreCase("Trade Name/Alias"))
			{
				Tradename  = sheet7.getRow(i).getCell(1).toString();
				comp.Loc_1_Edit_TradeName.clear();
				Thread.sleep(2000);
				comp.Loc_1_Edit_TradeName.sendKeys(Tradename);
				Thread.sleep(2000);
			}

			else if(value.equals("State_Type"))
			{
				System.out.println("Entered State Loop");
				Thread.sleep(4000);
				String EditState = comp.Loc_1_Edit_State_Present.getText().trim();
				System.out.println("The state is : "+EditState);
				Thread.sleep(5000);
				String StateExcel  = sheet7.getRow(i).getCell(1).toString();
				softassert.assertEquals(EditState, StateExcel , "State is non- editable");		
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("GSTIN_Type"))
			{
				Thread.sleep(3000);
				System.out.println("The GSTIN is  : "+GSTIN);
				if(comp.Loc_1_Edit_GSTIN.getAttribute("readonly").equals("true"))
				{

					softassert.assertTrue(comp.Loc_1_Edit_GSTIN.getAttribute("readonly").contains("true"), "The GSTIN test case did not pass");

				}

				else
				{
					comp.Edit_GSTIN.sendKeys("GSTINFAILED");
					System.out.println(" Testcase Failed");
					softassert.fail("GSTIN Field validation has failed");
				}
				Thread.sleep(3000);
			}
			else if(value.equalsIgnoreCase("Pan_Type"))
			{
				if(comp.Loc_1_Edit_PAN.getAttribute("readonly").equals("true"))
				{
					softassert.assertTrue(comp.Loc_1_Edit_PAN.getAttribute("readonly").contains("true"), "The PAN test case did not pass");

				}

				else
				{
					comp.Edit_PAN.sendKeys("PAN-FAILED");
					System.out.println(" Testcase Failed");
					softassert.fail("Field validation has failed");
				}

				Thread.sleep(3000);
			}
			else if(value.equalsIgnoreCase("TurnoverType"))
			{
				Thread.sleep(2000);
				String TurnOver_AprilJune2017  = sheet7.getRow(i).getCell(1).toString();
				Thread.sleep(2000);
				comp.Loc_1_Edit_TurnOver_AprilJune2017.clear();
				Thread.sleep(2000);
				comp.Loc_1_Edit_TurnOver_AprilJune2017.sendKeys(TurnOver_AprilJune2017);
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Registered_Mobile"))
			{
				String Mobile  = sheet7.getRow(i).getCell(1).toString();
				Thread.sleep(3000);
				comp.Loc_1_Edit_Mobile.clear();
				Thread.sleep(3000);
				comp.Loc_1_Edit_Mobile.sendKeys(Mobile);
				Thread.sleep(3000);
			}

			else if(value.equals("Constitution_of_Business"))
			{
				System.out.println("Entered Constitution of Business");
				Thread.sleep(3000);

				if(comp.Loc_1_Edit_ConstOfBussiness.getAttribute("readonly").equals("true"))
				{

					softassert.assertTrue(comp.Loc_1_Edit_ConstOfBussiness.getAttribute("readonly").contains("true"), " Const of Bussiness The test case did not pass");
					softassert.assertAll();

				}

				else
				{
					comp.Edit_ConstOfBussiness.sendKeys("FAILED");
					System.out.println(" Testcase Failed");
					softassert.fail("Field validation has failed");
				}
				Thread.sleep(2000);
			}
			else if(value.equals("City_District"))
			{
				System.out.println("Entered City/District loop");
				Thread.sleep(2000);
				comp.Loc_1_Edit_City_District_Dropdown.click();
				Thread.sleep(3000);

				for(int j=0 ; j<comp.Loc_1_Edit_ListofCity_District.size() ; j++)
				{

					if(comp.Loc_1_Edit_ListofCity_District.get(j).getText().equalsIgnoreCase(sheet7.getRow(i).getCell(1).toString()))
					{
						Thread.sleep(3000);
						comp.Loc_1_Edit_ListofCity_District.get(j).click();
						Thread.sleep(3000);
						break;
					}

				}
				Thread.sleep(2000);
			}
			else
			{
				System.out.println("Failed to take data from excel");
			}

		}	
		Thread.sleep(3000);
		comp.Loc_1_Edit_Address_Tab.click();
		Thread.sleep(3000);
		for (int i =0 ; i< sheet7.getLastRowNum() ; i++)
		{
			if(i==12)
			{
				comp.Loc_1_Edit_address1.clear();
				Thread.sleep(3000);
				comp.Loc_1_Edit_address1.sendKeys(sheet7.getRow(i).getCell(1).toString());
				Thread.sleep(3000);
			}

			if(i==13)
			{
				comp.Loc_1_Edit_address2.clear();
				Thread.sleep(3000);
				comp.Loc_1_Edit_address2.sendKeys(sheet7.getRow(i).getCell(1).toString());
				Thread.sleep(3000);
			}

			if(i==14)
			{	
				comp.Loc_1_Edit_pincode_address.clear();
				Thread.sleep(3000);
				comp.Loc_1_Edit_pincode_address.sendKeys(sheet7.getRow(i).getCell(1).toString());
				Thread.sleep(3000);
				break;
			}

		}

		Thread.sleep(5000);
		comp.Loc_1_Edit_dropdown_address.click();
		Thread.sleep(5000);
		comp.Loc_1_Edit_ListddlAddress.get(3).click();


		Thread.sleep(4000);
		comp.Loc_1_Edit_dropdown_Premises.click();
		Thread.sleep(2000);
		comp.Loc_1_Edit_ListddlPremises.get(1).click();


		Thread.sleep(2000);
		comp.Loc_1_Edit_dropdown_States.click();
		Thread.sleep(2000);
		comp.Loc_1_Edit_ListddlStates.get(9).click();


		Thread.sleep(4000);
		comp.Loc_1_Edit_dropdown_City.click();
		Thread.sleep(4000);
		comp.Loc_1_Edit_ListddlCity.get(0).click();


		Thread.sleep(2000);
		comp.Loc_1_Edit_dropdown_BussinessActvty.click();
		Thread.sleep(2000);
		comp.Loc_1_Edit_ListBussinessActvty.get(1).click();

		Thread.sleep(4000);
		comp.Loc_1_Edit_Address_next.click();
		Thread.sleep(3000);

		comp.Loc_1_Edit_GSTN_username.clear();
		Thread.sleep(2000);
		comp.Loc_1_Edit_GSTN_username.sendKeys(sheet7.getRow(15).getCell(1).toString());
		Thread.sleep(3000);
		comp.Loc_1_Edit_GSTN_username1.click();
		Thread.sleep(3000);
		comp.Loc_1_Edit_GSTN_users.get(0).click();
		Thread.sleep(4000);
		softassert.assertTrue(comp.Loc_1_Edit_Name_GSTN.isDisplayed(), "The Name is not Displayed");
		softassert.assertTrue(comp.Loc_1_Edit_Number_GSTN.isDisplayed(), "The Number is not Displayed");
		softassert.assertTrue(comp.Loc_1_Edit_PAN_GSTN.isDisplayed(), "The PAN is not Displayed");
		Thread.sleep(2000);
		comp.Loc_1_Edit_GSTUser_Next_GSTN.click();
		Thread.sleep(3000);
		/*
		 * GSTR3B
		 */	
		for(int i=0 ; i <comp.Loc_1_Edit_GSTR3B_drpdown.size() ; i++)
		{

			comp.Loc_1_Edit_GSTR3B_drpdown.get(i).click();
			Thread.sleep(2000);
			comp.Loc_1_Edit_GSTR3B_DirectInput.get(i).click();
			Thread.sleep(2000);
		}

		comp.Loc_1_Edit_Next_GSTR3B.click();
		Thread.sleep(5000);


		if((comp.Loc_1_Edit_HSN_checkbox.isSelected() && comp.Loc_1_Edit_ITC_checkbox.isSelected()))
		{
			Thread.sleep(2000);
			comp.Loc_1_Edit_HSN_checkbox.click();	
			Thread.sleep(2000);
			comp.Loc_1_Edit_ITC_checkbox.click();
			Thread.sleep(2000);
			softassert.assertFalse(comp.Loc_1_Edit_HSN_checkbox.isSelected(), "The HSN CheckBox is not selected");
			softassert.assertFalse(comp.Loc_1_Edit_ITC_checkbox.isSelected(), "The ITC CheckBox is not selected");
		}

		else if (!(comp.Loc_1_Edit_HSN_checkbox.isSelected() && comp.Loc_1_Edit_ITC_checkbox.isSelected()))
		{
			Thread.sleep(3000);
			comp.Loc_1_Edit_HSN_checkbox.click();	
			Thread.sleep(5000);
			comp.Loc_1_Edit_ITC_checkbox.click();
			Thread.sleep(3000);
			softassert.assertTrue(comp.Loc_1_Edit_HSN_checkbox.isSelected(), "The HSN CheckBox is not selected");
			softassert.assertTrue(comp.Loc_1_Edit_ITC_checkbox.isSelected(), "The ITC CheckBox is not selected");
		}

		comp.Loc_1_Edit_Save_AddCompany.click();
		Thread.sleep(8000);
		System.out.println("the msg : "+comp.Loc_1_LocationUpdated_Success.getText());
		softassert.assertEquals(comp.Loc_1_LocationUpdated_Success.getText(), "Location Updated successfully." , "The Location successfully created is not visible");
		Thread.sleep(4000);
		comp.Loc_1_Edit_OK.click();
		Thread.sleep(6000);
		softassert.assertAll();
	}


	public void Edit_ViewLocation() throws Exception
	{
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet2 = (XSSFSheet) wb.getSheetAt(1);
		sheet3 = (XSSFSheet) wb.getSheetAt(2);
		sheet4 = (XSSFSheet) wb.getSheetAt(3);
		sheet5 = (XSSFSheet) wb.getSheetAt(4);
		sheet6 = (XSSFSheet) wb.getSheetAt(5);
		sheet7 = (XSSFSheet) wb.getSheetAt(6);

		int Value = comp.CompCreatnList.size();
		String Company =  comp.CompCreatnList.get(Value-1).getText().trim();
		System.out.println("The selected company is : "+Company);
		comp.CompCreatnList.get(Value-1).click();
		Thread.sleep(3000);
		int Value1 = comp.Loc_1_CreatnList.size();
		String Location =  comp.Loc_1_CreatnList.get(Value1-1).getText().trim();
		System.out.println("The selected Location is : "+Location);

		String Tradename = null;

		for(int i=0 ; i <= sheet7.getLastRowNum() ; i++)
		{
			String value = sheet7.getRow(i).getCell(0).toString();
			if(value.equalsIgnoreCase("Trade Name/Alias"))
			{
				Tradename  = sheet7.getRow(i).getCell(1).toString();
				System.out.println("The TRADENAME from excel is : "+Tradename);
				Thread.sleep(3000);
				break;
			}
		}


		for(int a=0 ; a< comp.Loc_1_CreatnList.size() ; a++)
		{
			if(comp.Loc_1_CreatnList.get(a).getText().equalsIgnoreCase(Tradename))
			{

				Thread.sleep(5000);
				comp.Loc_1CreatnListView.get(a).click();
				System.out.println("The company from company page is : "+comp.Loc_1_CreatnList.get(a).getText());
				softassert.assertEquals(Location, Tradename , "The Location is not Viewed Successfully & not present in company list");
				softassert.assertAll();
				Thread.sleep(5000);
				break;
			}
		}
		String CompName = null;
		String GSTIN = null;
		for(int i=0 ; i <= sheet7.getLastRowNum() ; i++)

		{

			String value  = sheet7.getRow(i).getCell(0).toString();
			System.out.println("The value is : "+ value);

			if(value.equalsIgnoreCase("Company_Name"))
			{
				Thread.sleep(4000);
				CompName  = sheet7.getRow(i).getCell(1).toString();
				System.out.println("The Company from excel is : "+CompName);
				Thread.sleep(2000);
				System.out.println("The Company of group from screen is : "+comp.View_Loc1_Company.getText());
				softassert.assertEquals(CompName, comp.View_Loc1_Company.getText() , " The Company name does not match ");

			}

			else if(value.equalsIgnoreCase("LegalNameOfBussiness"))
			{

				softassert.assertEquals(CompName, comp.View_Loc1_bussiness.getText() , " The NameofBussiness  does not match ");
				Thread.sleep(2000);
			}


			else if(value.equalsIgnoreCase("Turnover_2016_2017"))
			{
				String Turnover1  = sheet7.getRow(i).getCell(1).toString();
				Thread.sleep(2000);
				softassert.assertEquals(Turnover1, comp.View_Loc1_turnover.getText() , " The Turnover does not match ");
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Registered_Email_id"))
			{
				String Emailid  = sheet7.getRow(i).getCell(1).toString();
				System.out.println("Emailid from screen is : "+Emailid);
				Thread.sleep(2000);
				softassert.assertEquals(Emailid, comp.View_Loc1_emailid.getText() , " The Email-ID does not match ");
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Trade Name/Alias"))
			{
				Tradename  = sheet7.getRow(i).getCell(1).toString();
				Thread.sleep(2000);
				softassert.assertEquals(Tradename, comp.View_Loc1_Tradename.getText() , " The TradeName  does not match ");
				Thread.sleep(2000);

			}

			else if(value.equals("State_Type"))
			{
				System.out.println("Entered State Loop");
				String State  = sheet7.getRow(i).getCell(1).toString();
				softassert.assertEquals(State, comp.View_Loc1_State.getText() , " The State does not match ");
				Thread.sleep(2000);

			}

			else if(value.equalsIgnoreCase("GSTIN_Type"))
			{
				GSTIN  = sheet7.getRow(i).getCell(1).toString();
				Thread.sleep(3000);
				System.out.println("The GSTIN is  : "+GSTIN);
				softassert.assertEquals(GSTIN, comp.View_GSTIN_Loc1_State.getText() , " The GSTIN does not match ");
				Thread.sleep(2000);
			}
			else if(value.equalsIgnoreCase("Pan_Type"))
			{
				String Pan  = GSTIN.substring(2,12);
				Thread.sleep(2000);
				System.out.println("The PAN from GSTIN "+GSTIN+" is "+Pan);
				softassert.assertEquals(Pan, comp.View_Pan_Loc1_State.getText() , " The PAN does not match ");
				Thread.sleep(2000);
			}
			else if(value.equalsIgnoreCase("TurnoverType"))
			{
				Thread.sleep(2000);
				String TurnOver_AprilJune2017  = sheet7.getRow(i).getCell(1).toString();
				softassert.assertEquals(TurnOver_AprilJune2017, comp.View_turnoverApr_Loc1_June.getText() , " The TurnoverAprJune does not match ");
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Registered_Mobile"))
			{
				String Mobile  = sheet7.getRow(i).getCell(1).toString();
				Thread.sleep(2000);
				softassert.assertEquals(Mobile, comp.View_Loc1_mobile.getText() , " The Mobile does not match ");
				Thread.sleep(3000);
			}

			else if(value.equals("Constitution_of_Business"))
			{
				Thread.sleep(3000);
				String ConsBussiness  = sheet7.getRow(i).getCell(1).toString();
				softassert.assertEquals(ConsBussiness, comp.View_Loc1_Constitution.getText() , " The Constitution of Bussiness does not match ");
				Thread.sleep(3000);

			}
			else if(value.equals("City_District"))
			{
				Thread.sleep(5000);
				String City  = sheet7.getRow(i).getCell(1).toString();
				softassert.assertEquals(City, comp.View_Loc1_City.getText() , "The City does not match");
				Thread.sleep(2000);

			}

		}

		Thread.sleep(4000);
		comp.View_Address_Loc1_Tab.click();
		Thread.sleep(5000);
		for (int i =0 ; i< sheet7.getLastRowNum(); i++)
		{

			if(sheet6.getRow(i).getCell(0).toString().equalsIgnoreCase("Address_Line_1"))
			{
				String Add1 = sheet7.getRow(i).getCell(1).toString();
				System.out.println(Add1);
				softassert.assertEquals(Add1, comp.View_Add1_Loc_1.getText() , " The Address on 1st line did not match ");
				Thread.sleep(3000);
			}

			if(sheet6.getRow(i).getCell(0).toString().equalsIgnoreCase("Address_Line_2"))
			{
				String Add2 = sheet7.getRow(i).getCell(1).toString();
				System.out.println(Add2);
				softassert.assertEquals(Add2, comp.View_Add2_Loc_1.getText() , " The Address on 2nd line did not match ");
				Thread.sleep(3000);
			}

			if(sheet6.getRow(i).getCell(0).toString().equalsIgnoreCase("Pincode"))
			{
				String Pincode = sheet7.getRow(i).getCell(1).toString();
				System.out.println(Pincode);
				softassert.assertEquals(Pincode, comp.View_Pincode_Loc_1.getText() , " The Pincode does not match ");
				Thread.sleep(3000);
				break;
			}


		}

		softassert.assertEquals("Corporate Office",    comp.View_Addresstype_Loc_1.getText() , " The Address Type does not match ");
		softassert.assertEquals("Rented",	 comp.View_Premises_Loc_1.getText() , " The Premises does not match ");
		softassert.assertEquals("Delhi",	 comp.View_StateAddress_Loc_1.getText() , " The State does not match ");
		softassert.assertEquals("Delhi",	 comp.View_CityAddress_Loc_1.getText() , " The City does not match ");
		softassert.assertEquals("EOU / STP / EHTP", 	comp.View_BA_Loc_1.getText() , " The Nature of Business Activity  does not match ");



		comp.View_GSTIN_User_Loc_1.click();
		Thread.sleep(3000);


		String GSTIN_username = sheet7.getRow(15).getCell(1).toString();
		softassert.assertEquals(GSTIN_username , comp.View_GSTIN_Username_Loc_1.getText() , "The GSTIN username does not match");
		Thread.sleep(3000);

		String GSTIN_Mail_1 = sheet3.getRow(3).getCell(1).toString();
		System.out.println(GSTIN_Mail_1);
		softassert.assertEquals(GSTIN_Mail_1 , comp.View_GSTIN_Email_ID_Loc_1.getText() , "The GSTIN EmailID does not match");
		Thread.sleep(3000);


		String GSTIN_FirstName = sheet2.getRow(3).getCell(1).toString();
		String GSTIN_LastName = sheet2.getRow(4).getCell(1).toString();
		System.out.println("The name from Excel is : "+GSTIN_FirstName);
		System.out.println("The name from Excel is : "+GSTIN_LastName);
		String FullName = GSTIN_FirstName+" "+GSTIN_LastName;
		System.out.println("The name from Excel is : "+FullName);
		System.out.println("The name from screen is  is : "+ comp.View_Name_GSTN_Loc_1.getText());
		softassert.assertEquals(FullName , comp.View_Name_GSTN_Loc_1.getText() , "The GSTIN Name does not match");
		Thread.sleep(3000);


		String GSTIN_Number = sheet2.getRow(1).getCell(1).toString();
		softassert.assertEquals(GSTIN_Number , comp.View_Number_GSTN_Loc_1.getText() , "The GSTIN Number does not match");
		Thread.sleep(3000);



		String GSTIN_Pan = sheet2.getRow(2).getCell(1).toString();
		softassert.assertEquals(GSTIN_Pan , comp.View_PAN_GSTN_Loc_1.getText() , "The GSTIN PAN does not match");
		Thread.sleep(3000);

		/*	
		comp.View_GSTRR3B_Loc_1.click();
		Thread.sleep(3000);
		softassert.assertEquals("March 2018" , comp.View_Month_Year_Loc_1.getText() , "The GSTR3B month & year does not match");
		softassert.assertEquals("Compile" , comp.View_Type_Loc_1.getText() , "The GSTR3B Type does not match");*/
		comp.Loc_1_View_OtherConfig.click();
		softassert.assertAll();
	}


	public void Location_1_Edit_ViewOtherConfigDeselected() throws Exception
	{			
		Thread.sleep(4000);

		System.out.println("HSN :"+comp.Loc_1_View_HSNSummary_Value.getText());
		System.out.println("ITC :"+comp.Loc_1_View_ITC_Value.getText());


		softassert.assertEquals("Yes" , comp.Loc_1_View_HSNSummary_Value.getText() , "The HSNSummary 'Yes' does not match");
		softassert.assertEquals("Yes" , comp.Loc_1_View_ITC_Value.getText() , "The ITC 'Yes' does not match");

		Thread.sleep(3000);
		comp.Loc_1_View_Close.click();
		softassert.assertAll();

	}	

	public void ViewEditedCompany() throws Exception
	{
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet2 = (XSSFSheet) wb.getSheetAt(1);
		sheet3 = (XSSFSheet) wb.getSheetAt(2);
		sheet4 = (XSSFSheet) wb.getSheetAt(3);
		sheet5 = (XSSFSheet) wb.getSheetAt(4);
		int Value = comp.CompCreatnList.size();
		String Company =  comp.CompCreatnList.get(Value-1).getText().trim();

		String Tradename = null;
		for(int i=0 ; i <= sheet5.getLastRowNum() ; i++)
		{
			String value = sheet5.getRow(i).getCell(0).toString();
			if(value.equalsIgnoreCase("Trade Name/Alias"))
			{
				Tradename  = sheet5.getRow(i).getCell(1).toString();
				System.out.println("The TRADENAME from excel is : "+Tradename);
				Thread.sleep(3000);
				break;
			}
		}


		for(int a=0 ; a< comp.CompCreatnList.size() ; a++)
		{
			if(comp.CompCreatnList.get(a).getText().equalsIgnoreCase(Tradename))
			{
				Thread.sleep(5000);
				comp.CompCreatnListView.get(a).click();
				System.out.println("The company from company page is : "+comp.CompCreatnList.get(a).getText());
				softassert.assertEquals(Company, Tradename , "The company is not Viewed Successfully & not present in company list");
				Thread.sleep(5000);
				break;
			}
		}

		String GSTIN = null;
		for(int i=0 ; i <= sheet5.getLastRowNum() ; i++)

		{
			String value  = sheet5.getRow(i).getCell(0).toString();
			System.out.println("The value is : "+ value);

			if(value.equalsIgnoreCase("Account/Group Name"))
			{
				Thread.sleep(4000);
				String accnameGrp  = sheet5.getRow(i).getCell(1).toString();
				System.out.println("The Acc Name is : "+accnameGrp);
				Thread.sleep(2000);
				System.out.println("The acc name of group from screen is : "+comp.View_Acc_Grp.getText());
				softassert.assertEquals(accnameGrp, comp.View_Acc_Grp.getText() , " The Account name does not match ");

			}

			else if(value.equalsIgnoreCase("Company Name"))
			{
				String companyName  = sheet5.getRow(i).getCell(1).toString();
				softassert.assertEquals(companyName, comp.View_CompanyName.getText() , " The Company name does not match ");
				Thread.sleep(2000);
			}


			else if(value.equalsIgnoreCase("Turnover_2016_2017"))
			{
				String Turnover1  = sheet5.getRow(i).getCell(1).toString();
				Thread.sleep(2000);
				softassert.assertEquals(Turnover1, comp.View_turnover.getText() , " The Turnover does not match ");
				Thread.sleep(2000);
			}


			else if(value.equalsIgnoreCase("Registered Email-id"))
			{
				String Emailid  = sheet5.getRow(i).getCell(1).toString();
				Thread.sleep(2000);
				softassert.assertEquals(Emailid, comp.View_emailid.getText() , " The Email-ID does not match ");
				Thread.sleep(2000);
			}

			else if(value.equals("State_Type"))
			{
				System.out.println("Entered State Loop");
				String State  = sheet5.getRow(i).getCell(1).toString();
				softassert.assertEquals(State, comp.View_State.getText() , " The State does not match ");
				Thread.sleep(2000);

			}

			else if(value.equalsIgnoreCase("GSTIN_Type"))
			{
				GSTIN  = sheet5.getRow(i).getCell(1).toString();
				Thread.sleep(3000);
				System.out.println("The GSTIN is  : "+GSTIN);
				softassert.assertEquals(GSTIN, comp.View_GSTIN.getText() , " The GSTIN does not match ");
				Thread.sleep(2000);
			}
			else if(value.equalsIgnoreCase("Pan_Type"))
			{
				String Pan  = GSTIN.substring(2,12);
				Thread.sleep(2000);
				System.out.println("The PAN from GSTIN "+GSTIN+" is "+Pan);
				softassert.assertEquals(Pan, comp.View_Pan.getText() , " The PAN does not match ");
				Thread.sleep(2000);
			}
			else if(value.equalsIgnoreCase("TurnoverType"))
			{
				String TurnOver_AprilJune2017  = sheet5.getRow(i).getCell(1).toString();
				softassert.assertEquals(TurnOver_AprilJune2017, comp.View_turnoverApr_June.getText() , " The TurnoverAprJune does not match ");
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Registered_Mobile"))
			{
				String Mobile  = sheet5.getRow(i).getCell(1).toString();
				softassert.assertEquals(Mobile, comp.View_mobile.getText() , " The Mobile does not match ");
				Thread.sleep(3000);
			}

			else if(value.equals("Constitution_of_Business"))
			{
				System.out.println("Entered Constitution of Business");
				String ConsBussiness  = sheet5.getRow(i).getCell(1).toString();
				softassert.assertEquals(ConsBussiness, comp.View_Constitution.getText() , " The Constitution of Bussiness does not match ");
				Thread.sleep(3000);



			}
			else if(value.equals("City_District"))
			{
				System.out.println("Entered City/District loop");
				Thread.sleep(5000);
				String City  = sheet5.getRow(i).getCell(1).toString();
				softassert.assertEquals(City, comp.View_City.getText() , "The City does not match");
				Thread.sleep(2000);
				break;

			}

		}


		comp.Address_Tab.click();
		Thread.sleep(5000);
		for (int i =0 ; i< sheet5.getLastRowNum() ; i++)
		{
			if(i==12)
			{
				String Add1 = sheet5.getRow(i).getCell(1).toString();
				softassert.assertEquals(Add1, comp.View_Add1.getText() , " The Address on 1st line did not match ");
				Thread.sleep(3000);
			}

			if(i==13)
			{
				String Add2 = sheet5.getRow(i).getCell(1).toString();
				softassert.assertEquals(Add2, comp.View_Add2.getText() , " The Address on 2nd line did not match ");
				Thread.sleep(3000);
			}

			if(i==14)
			{
				String Pincode = sheet5.getRow(i).getCell(1).toString();
				softassert.assertEquals(Pincode, comp.View_Pincode.getText() , " The Pincode does not match ");
				Thread.sleep(3000);
				break;
			}

		}

		softassert.assertEquals("Corporate Office", comp.View_Addresstype.getText() , " The Address Type does not match ");
		softassert.assertEquals("Rented", comp.View_Premises.getText() , " The Premises does not match ");
		softassert.assertEquals("Delhi", comp.View_StateAddress.getText() , " The State does not match ");
		softassert.assertEquals("Delhi", comp.View_CityAddress.getText() , " The City does not match ");
		softassert.assertEquals("EOU / STP / EHTP", comp.View_BA.getText() , " The Nature of Business Activity  does not match ");
		Thread.sleep(3000);

		comp.View_GSTIN_User.click();
		Thread.sleep(3000);


		String GSTIN_username = sheet5.getRow(15).getCell(1).toString();
		softassert.assertEquals(GSTIN_username , comp.View_GSTIN_Username.getText() , "The GSTIN username does not match");
		Thread.sleep(3000);

		GSTIN_Mail = sheet3.getRow(3).getCell(1).toString();
		softassert.assertEquals(GSTIN_Mail , comp.View_GSTIN_Email_ID.getText() , "The GSTIN EmailID does not match");
		Thread.sleep(3000);


		String GSTIN_FirstName = sheet2.getRow(3).getCell(1).toString();
		String GSTIN_LastName = sheet2.getRow(4).getCell(1).toString();
		System.out.println("The name from Excel is : "+GSTIN_FirstName);
		System.out.println("The name from Excel is : "+GSTIN_LastName);
		String FullName = GSTIN_FirstName+" "+GSTIN_LastName;
		System.out.println("The name from Excel is : "+FullName);
		System.out.println("The name from screen is  is : "+ comp.View_Name_GSTN.getText());
		softassert.assertEquals(FullName , comp.View_Name_GSTN.getText() , "The GSTIN Name does not match");
		Thread.sleep(3000);


		String GSTIN_Number = sheet2.getRow(1).getCell(1).toString();
		softassert.assertEquals(GSTIN_Number , comp.View_Number_GSTN.getText() , "The GSTIN Number does not match");
		Thread.sleep(3000);



		String GSTIN_Pan = sheet2.getRow(2).getCell(1).toString();
		softassert.assertEquals(GSTIN_Pan , comp.View_PAN_GSTN.getText() , "The GSTIN PAN does not match");
		Thread.sleep(3000);


		comp.View_GSTRR3B.click();
		Thread.sleep(3000);
		
		
		
		
		for(int i=0 ; i < comp.List_GSTRR3BDirectInput.size() ; i++)
		{
			softassert.assertEquals(comp.List_GSTRR3BDirectInput.get(i).getText().trim() , "Direct Input" ,  "The GSTR3B Direct Input does not match");
			Thread.sleep(2000);
		}
		
		comp.View_OtherConfig.click();
		
		softassert.assertAll();
	}


	public void ViewEditOtherConfigDeselected() throws Exception
	{
		Thread.sleep(2000);
		softassert.assertEquals("Yes" , comp.View_HSNSummary_Value.getText() , "The HSNSummary 'Yes' does not match");
		softassert.assertEquals("Yes" , comp.View_ITC_Value.getText() , "The ITC 'Yes' does not match");
		Thread.sleep(3000);
		comp.View_Close.click();
		softassert.assertAll();
	}

	public void AddCompany02() throws Exception 

	{
		int companytwo =2;
		comp.Addcompany.click();
		Thread.sleep(3000);
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet3 = (XSSFSheet) wb.getSheetAt(2);
		sheet2 = (XSSFSheet) wb.getSheetAt(1);
		System.out.println("The total row number is : "+sheet3.getLastRowNum());
		String GSTIN = null;
		System.out.println("The boolean value of acc/grp is : "+comp.VerifyAcc_GroupNameComp.getAttribute("readonly"));
		for(int i=0 ; i <= sheet3.getLastRowNum() ; i++)
		{
			String value  = sheet3.getRow(i).getCell(companynames).toString();
			System.out.println("The value is : "+ value);
			if(value.equalsIgnoreCase("Account/Group Name"))
			{
				Thread.sleep(3000);
				if(comp.VerifyAcc_GroupNameComp.getAttribute("readonly").contains("true"))
				{
					System.out.println("TRUE");
					softassert.assertTrue(comp.VerifyAcc_GroupNameComp.getAttribute("readonly").contains("true"), "The test case did not pass");

				}

				else
				{
					System.out.println("FALSE");
					System.out.println(" Testcase Failed");
					softassert.fail("Field validation has failed");
				}

			}

			else if(value.equalsIgnoreCase("Company Name"))
			{
				String companyName  = sheet3.getRow(i).getCell(companytwo).toString();
				comp.AddCompanyName.sendKeys(companyName);
				Thread.sleep(2000);
			}


			else if(value.equalsIgnoreCase("Turnover_2016_2017"))
			{
				String Turnover1  = sheet3.getRow(i).getCell(companytwo).toString();
				Thread.sleep(2000);
				comp.TurnOver16_17.sendKeys(Turnover1);
				Thread.sleep(2000);
			}


			else if(value.equalsIgnoreCase("Registered Email-id"))
			{
				String Emailid  = sheet3.getRow(i).getCell(companytwo).toString();
				Thread.sleep(2000);
				comp.Registered_Email_id.sendKeys(Emailid);
				Thread.sleep(3000);
			}

			else if(value.equalsIgnoreCase("Trade Name/Alias"))
			{
				String Tradename  = sheet3.getRow(i).getCell(companytwo).toString();
				comp.TradeName.sendKeys(Tradename);
				Thread.sleep(2000);
			}

			else if(value.equals("State_Type"))
			{
				System.out.println("Entered State Loop");
				Thread.sleep(4000);
				comp.State_Dropdown.click();
				Thread.sleep(5000);

				for(int c=0 ; c<comp.ListofStates.size() ; c++)
				{
					if(comp.ListofStates.get(c).getText().equalsIgnoreCase(sheet3.getRow(i).getCell(companytwo).toString()))
					{
						Thread.sleep(5000);
						comp.ListofStates.get(c).click();
						Thread.sleep(8000);
						break;
					}

				}
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("GSTIN_Type"))
			{
				GSTIN  = sheet3.getRow(i).getCell(companytwo).toString();
				Thread.sleep(3000);
				System.out.println("The GSTIN is  : "+GSTIN);
				Thread.sleep(4000);
				comp.GSTIN.sendKeys(GSTIN);
				Thread.sleep(3000);
			}
			else if(value.equalsIgnoreCase("Pan_Type"))
			{
				String Pan  = GSTIN.substring(2,12);
				Thread.sleep(4000);
				System.out.println("The PAN from GSTIN "+GSTIN+" is "+Pan);
				comp.PAN.sendKeys(Pan);
				Thread.sleep(3000);
			}
			else if(value.equalsIgnoreCase("TurnoverType"))
			{
				Thread.sleep(2000);
				String TurnOver_AprilJune2017  = sheet3.getRow(i).getCell(companytwo).toString();
				Thread.sleep(5000);
				comp.TurnOver_AprilJune2017.sendKeys(TurnOver_AprilJune2017);
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Registered_Mobile"))
			{
				String Mobile  = sheet3.getRow(i).getCell(companytwo).toString();
				Thread.sleep(3000);
				comp.Mobile.sendKeys(Mobile);
				Thread.sleep(3000);
			}

			else if(value.equals("Constitution_of_Business"))
			{
				System.out.println("Entered Constitution of Business");
				Thread.sleep(3000);
				comp.ConstOfBussiness.click();
				Thread.sleep(3000);

				for(int a=0 ; a<comp.ListConstOfBussiness.size() ; a++)
				{
					Thread.sleep(3000);
					if(comp.ListConstOfBussiness.get(a).getText().equalsIgnoreCase(sheet3.getRow(i).getCell(companytwo).toString()))
					{
						Thread.sleep(5000);
						comp.ListConstOfBussiness.get(a).click();
						Thread.sleep(3000);
						break;
					}

				}
				Thread.sleep(2000);
			}
			else if(value.equals("City_District"))
			{
				System.out.println("Entered City/District loop");
				comp.City_District_Dropdown.click();
				Thread.sleep(2000);
				for(int j=0 ; j<comp.ListofCity_District.size() ; j++)
				{

					if(comp.ListofCity_District.get(j).getText().equalsIgnoreCase(sheet3.getRow(i).getCell(companytwo).toString()))
					{
						Thread.sleep(5000);
						comp.ListofCity_District.get(j).click();
						Thread.sleep(5000);
						break;
					}

				}

			}
			else
			{
				System.out.println("Failed to take data from excel");
			}

		}

		softassert.assertAll();
	}



	public void AddAddress02() throws Exception 
	{
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		int compadd=2;
		comp.Next.click();
		Thread.sleep(3000);
		comp.NextRegistration.click();
		Thread.sleep(3000);
		sheet4 = (XSSFSheet) wb.getSheetAt(3);
		for (int i =0 ; i< sheet4.getLastRowNum() ; i++)
		{
			if(i==0)
			{
				comp.address1.sendKeys(sheet4.getRow(i).getCell(compadd).toString());
				Thread.sleep(3000);
			}

			if(i==1)
			{
				comp.address2.sendKeys(sheet4.getRow(i).getCell(compadd).toString());
				Thread.sleep(3000);
			}

			if(i==2)
			{
				comp.pincode_address.sendKeys(sheet4.getRow(i).getCell(compadd).toString());
				Thread.sleep(3000);
				break;
			}

		}

		Thread.sleep(2000);
		comp.dropdown_address.click();
		Thread.sleep(2000);
		comp.ListddlAddress.get(1).click();


		Thread.sleep(2000);    
		comp.dropdown_Premises.click();
		Thread.sleep(2000);
		comp.ListddlPremises.get(1).click();


		Thread.sleep(2000);
		comp.dropdown_States.click();
		Thread.sleep(2000);
		comp.ListddlStates.get(20).click();


		Thread.sleep(2000);
		comp.dropdown_City.click();
		Thread.sleep(2000);
		comp.ListddlCity.get(233).click();


		Thread.sleep(2000);
		comp.dropdown_BussinessActvty.click();
		Thread.sleep(2000);
		comp.ListBussinessActvty.get(1).click();

		Thread.sleep(4000);
		comp.Next_Address.click();
		Thread.sleep(3000);
		softassert.assertAll();

	}

	public void addGSTN02() throws Exception
	{
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet4 = (XSSFSheet) wb.getSheetAt(3);
		comp.GSTN_username.sendKeys(sheet4.getRow(3).getCell(1).toString());
		Thread.sleep(3000);
		comp.GSTN_username1.click();
		Thread.sleep(3000);
		comp.GSTN_users.get(0).click();
		Thread.sleep(4000);
		softassert.assertTrue(comp.Name_GSTN.isDisplayed(), "The Name is not Displayed");
		softassert.assertTrue(comp.Number_GSTN.isDisplayed(), "The Number is not Displayed");
		softassert.assertTrue(comp.PAN_GSTN.isDisplayed(), "The PAN is not Displayed");
		Thread.sleep(2000);
		comp.Next_GSTN.click();
		Thread.sleep(3000);
		/*
		 * GSTR3B
		 */

		for(int i =0 ; i< comp.GSTR3B_List.size() ; i++)
		{

			comp.GSTR3B_List.get(i).click();
			Thread.sleep(2000);
			comp.GSTR3B_Compile.get(1).click();

		}
		Thread.sleep(5000);		
		comp.Next_GSTR3B.click();
		Thread.sleep(5000);
		softassert.assertAll();
	}


	public void HSN_ITC02() throws Exception
	{

		if(comp.HSN_checkbox.isSelected())
		{

			softassert.assertTrue(comp.HSN_checkbox.isSelected(), "The CheckBox is not selected");

		}

		else
		{
			softassert.assertFalse(comp.HSN_checkbox.isSelected(), "The CheckBox is not selected");

		}

		if(comp.ITC_checkbox.isSelected())
		{
			softassert.assertTrue(comp.ITC_checkbox.isSelected(), "The CheckBox is not selected");

		}

		else
		{
			softassert.assertFalse(comp.ITC_checkbox.isSelected(), "The CheckBox is not selected");

		}

		softassert.assertAll();
	}

	public void VerifyCompany02() throws Exception
	{
		int companytwo =2;
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);

		comp.Save_AddCompany.click();
		Thread.sleep(3000);
		comp.Company_Created.click();
		Thread.sleep(3000);

		String Tradename = null;
		for(int i=0 ; i <sheet3.getLastRowNum() ; i++)
		{
			String value = sheet3.getRow(i).getCell(0).toString();
			if(value.equalsIgnoreCase("Trade Name/Alias"))
			{
				Tradename  = sheet3.getRow(i).getCell(companytwo).toString();
				System.out.println("The company name from excel is : "+Tradename);
				Thread.sleep(2000);
				break;
			}
		}
		int Value = comp.CompCreatnList.size();
		String Company =  comp.CompCreatnList.get(Value-1).getText().trim();
		softassert.assertEquals(Company, Tradename , "The company is not created Successfully & not present in list");
		softassert.assertAll();
	}


	public void ViewCompany02() throws Exception
	{
		int companytwo =2;
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet2 = (XSSFSheet) wb.getSheetAt(1);
		sheet3 = (XSSFSheet) wb.getSheetAt(2);
		sheet4 = (XSSFSheet) wb.getSheetAt(3);
		int Value = comp.CompCreatnList.size();
		String Company =  comp.CompCreatnList.get(Value-1).getText().trim();

		String Tradename = null;
		for(int i=0 ; i <= sheet3.getLastRowNum() ; i++)
		{
			String value = sheet3.getRow(i).getCell(companynames).toString();
			if(value.equalsIgnoreCase("Trade Name/Alias"))
			{
				Tradename  = sheet3.getRow(i).getCell(companytwo).toString();
				System.out.println("The TRADENAME from excel is : "+Tradename);
				Thread.sleep(3000);
				break;
			}
		}


		for(int a=0 ; a< comp.CompCreatnList.size() ; a++)
		{
			if(comp.CompCreatnList.get(a).getText().equalsIgnoreCase(Tradename))
			{
				Thread.sleep(5000);
				comp.CompCreatnListView.get(a).click();
				System.out.println("The company from company page is : "+comp.CompCreatnList.get(a).getText());
				softassert.assertEquals(Company, Tradename , "The company is not Viewed Successfully & not present in company list");
				softassert.assertAll();
				Thread.sleep(5000);
				break;
			}
		}

		String GSTIN = null;
		for(int i=0 ; i <= sheet3.getLastRowNum() ; i++)

		{

			String value  = sheet3.getRow(i).getCell(0).toString();
			System.out.println("The value is : "+ value);

			if(value.equalsIgnoreCase("Account/Group Name"))
			{
				Thread.sleep(4000);
				String accnameGrp  = sheet3.getRow(i).getCell(companytwo).toString();
				System.out.println("The Acc Name is : "+accnameGrp);
				Thread.sleep(2000);
				System.out.println("The acc name of group from screen is : "+comp.View_Acc_Grp.getText());
				softassert.assertEquals(accnameGrp, comp.View_Acc_Grp.getText() , " The Account name does not match ");

			}

			else if(value.equalsIgnoreCase("Company Name"))
			{
				String companyName  = sheet3.getRow(i).getCell(companytwo).toString();
				softassert.assertEquals(companyName, comp.View_CompanyName.getText() , " The Company name does not match ");
				Thread.sleep(2000);
			}


			else if(value.equalsIgnoreCase("Turnover_2016_2017"))
			{
				String Turnover1  = sheet3.getRow(i).getCell(companytwo).toString();
				Thread.sleep(2000);
				softassert.assertEquals(Turnover1, comp.View_turnover.getText() , " The Turnover does not match ");
				Thread.sleep(2000);
			}


			else if(value.equalsIgnoreCase("Registered Email-id"))
			{
				String Emailid  = sheet3.getRow(i).getCell(companytwo).toString();
				Thread.sleep(2000);
				softassert.assertEquals(Emailid, comp.View_emailid.getText() , " The Email-ID does not match ");
				Thread.sleep(2000);
			}

			else if(value.equals("State_Type"))
			{
				System.out.println("Entered State Loop");
				String State  = sheet3.getRow(i).getCell(companytwo).toString();
				softassert.assertEquals(State, comp.View_State.getText() , " The State does not match ");
				Thread.sleep(2000);

			}

			else if(value.equalsIgnoreCase("GSTIN_Type"))
			{
				GSTIN  = sheet3.getRow(i).getCell(companytwo).toString();
				Thread.sleep(3000);
				System.out.println("The GSTIN is  : "+GSTIN);
				softassert.assertEquals(GSTIN, comp.View_GSTIN.getText() , " The GSTIN does not match ");
				Thread.sleep(2000);
			}
			else if(value.equalsIgnoreCase("Pan_Type"))
			{
				String Pan  = GSTIN.substring(2,12);
				Thread.sleep(2000);
				System.out.println("The PAN from GSTIN "+GSTIN+" is "+Pan);
				softassert.assertEquals(Pan, comp.View_Pan.getText() , " The PAN does not match ");
				Thread.sleep(2000);
			}
			else if(value.equalsIgnoreCase("TurnoverType"))
			{
				Thread.sleep(2000);
				String TurnOver_AprilJune2017  = sheet3.getRow(i).getCell(companytwo).toString();
				softassert.assertEquals(TurnOver_AprilJune2017, comp.View_turnoverApr_June.getText() , " The TurnoverAprJune does not match ");
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Registered_Mobile"))
			{
				String Mobile  = sheet3.getRow(i).getCell(companytwo).toString();
				Thread.sleep(2000);
				softassert.assertEquals(Mobile, comp.View_mobile.getText() , " The Mobile does not match ");
				Thread.sleep(3000);
			}

			else if(value.equals("Constitution_of_Business"))
			{
				System.out.println("Entered Constitution of Business");
				Thread.sleep(3000);
				String ConsBussiness  = sheet3.getRow(i).getCell(companytwo).toString();
				softassert.assertEquals(ConsBussiness, comp.View_Constitution.getText() , " The Constitution of Bussiness does not match ");
				Thread.sleep(3000);



			}
			else if(value.equals("City_District"))
			{
				System.out.println("Entered City/District loop");
				Thread.sleep(5000);
				String City  = sheet3.getRow(i).getCell(companytwo).toString();
				softassert.assertEquals(City, comp.View_City.getText() , "The City does not match");
				Thread.sleep(2000);

			}

		}


		comp.Address_Tab.click();
		Thread.sleep(5000);
		for (int i =0 ; i< sheet4.getLastRowNum() ; i++)
		{
			if(i==0)
			{
				String Add1 = sheet4.getRow(i).getCell(companytwo).toString();
				softassert.assertEquals(Add1, comp.View_Add1.getText() , " The Address on 1st line did not match ");
				Thread.sleep(3000);
			}

			if(i==1)
			{
				String Add2 = sheet4.getRow(i).getCell(companytwo).toString();
				softassert.assertEquals(Add2, comp.View_Add2.getText() , " The Address on 2nd line did not match ");
				Thread.sleep(3000);
			}

			if(i==2)
			{
				String Pincode = sheet4.getRow(i).getCell(companytwo).toString();
				softassert.assertEquals(Pincode, comp.View_Pincode.getText() , " The Pincode does not match ");
				Thread.sleep(3000);
				break;
			}


		}

		softassert.assertEquals("Branch Office", comp.View_Addresstype.getText() , " The Address Type does not match ");
		softassert.assertEquals("Rented", comp.View_Premises.getText() , " The Premises does not match ");
		softassert.assertEquals("Maharashtra", comp.View_StateAddress.getText() , " The State does not match ");
		softassert.assertEquals("Mumbai", comp.View_CityAddress.getText() , " The City does not match ");
		softassert.assertEquals("EOU / STP / EHTP", comp.View_BA.getText() , " The Nature of Business Activity  does not match ");



		comp.View_GSTIN_User.click();
		Thread.sleep(3000);


		String GSTIN_username = sheet4.getRow(3).getCell(companytwo).toString();
		softassert.assertEquals(GSTIN_username , comp.View_GSTIN_Username.getText() , "The GSTIN username does not match");
		Thread.sleep(3000);

		//keep this as it is
		GSTIN_Mail = sheet3.getRow(3).getCell(1).toString();
		softassert.assertEquals(GSTIN_Mail , comp.View_GSTIN_Email_ID.getText() , "The GSTIN EmailID does not match");
		Thread.sleep(3000);


		String GSTIN_FirstName = sheet2.getRow(3).getCell(1).toString();
		String GSTIN_LastName = sheet2.getRow(4).getCell(1).toString();
		System.out.println("The name from Excel is : "+GSTIN_FirstName);
		System.out.println("The name from Excel is : "+GSTIN_LastName);
		String FullName = GSTIN_FirstName+" "+GSTIN_LastName;
		System.out.println("The name from Excel is : "+FullName);
		System.out.println("The name from screen is  is : "+ comp.View_Name_GSTN.getText());
		softassert.assertEquals(FullName , comp.View_Name_GSTN.getText() , "The GSTIN Name does not match");
		Thread.sleep(3000);


		String GSTIN_Number = sheet2.getRow(1).getCell(1).toString();
		softassert.assertEquals(GSTIN_Number , comp.View_Number_GSTN.getText() , "The GSTIN Number does not match");
		Thread.sleep(3000);



		String GSTIN_Pan = sheet2.getRow(2).getCell(1).toString();
		softassert.assertEquals(GSTIN_Pan , comp.View_PAN_GSTN.getText() , "The GSTIN PAN does not match");
		Thread.sleep(3000);

		/*
		comp.View_GSTRR3B.click();
		String Month = comp.View_Month_Year.getText();
		System.out.println("Month in GSTR3B is : "+Month);
		if(Month.equalsIgnoreCase("January"))
		{
			softassert.assertEquals("January 2018" , Month , "The GSTR3B month & year does not match");
		}
		else if (Month.contains("February") )
		{
			softassert.assertEquals("February 2018" , Month , "The GSTR3B month & year does not match");
		}
		else if (Month.contains("March") )
		{
			softassert.assertEquals("March 2018" , Month , "The GSTR3B month & year does not match");
		}
		else if (Month.contains("April") )
		{
			softassert.assertEquals("April 2018" , Month , "The GSTR3B month & year does not match");

		}	
		else if (Month.contains("May") )
		{
			softassert.assertEquals("May 2018" , Month , "The GSTR3B month & year does not match");
		}
		else if (Month.contains("June") )
		{
			softassert.assertEquals("June 2018" , Month , "The GSTR3B month & year does not match");
		}
		else if (Month.contains("July") )
		{
			softassert.assertEquals("July 2018" , Month , "The GSTR3B month & year does not match");
		}
		else if (Month.contains("August") )
		{
			softassert.assertEquals("August 2018" , Month , "The GSTR3B month & year does not match");
		}
		else if (Month.contains("September") )
		{
			softassert.assertEquals("September 2018" , Month , "The GSTR3B month & year does not match");
		}
		else if (Month.contains("October") )
		{
			softassert.assertEquals("October 2018" , Month , "The GSTR3B month & year does not match");
		}
		else if (Month.contains("November") )
		{
			softassert.assertEquals("November 2018" , Month , "The GSTR3B month & year does not match");
		}
		else if (Month.contains("December") )
		{
			softassert.assertEquals("December 2018" , Month , "The GSTR3B month & year does not match");
		}
		else
		{
			System.out.println("Month doesnt exist");
			softassert.fail("GSTR3b month does not exist");
		}
		softassert.assertEquals("Compile" , comp.View_Type.getText() , "The GSTR3B Type does not match");*/

		comp.View_OtherConfig.click();
		softassert.assertAll();
	}



	public void ViewOtherConfigDeselected02() throws Exception
	{
		Thread.sleep(4000);
		softassert.assertEquals("No" , comp.View_HSNSummary_Value.getText() , "The HSNSummary 'No' does not match");
		softassert.assertEquals("No" , comp.View_ITC_Value.getText() , "The ITC 'No' does not match");
		Thread.sleep(3000);
		comp.View_Close.click();
		softassert.assertAll();
	}


	public void EditCompany02() throws Exception
	{
		int companytwo =2;
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet2 = (XSSFSheet) wb.getSheetAt(1);
		sheet3 = (XSSFSheet) wb.getSheetAt(2);
		sheet4 = (XSSFSheet) wb.getSheetAt(3);
		sheet5 = (XSSFSheet) wb.getSheetAt(4);
		int Value = comp.CompCreatnList.size();
		String Company =  comp.CompCreatnList.get(Value-1).getText().trim();
		String Tradename = null;


		/*	for(int i=0 ; i <= sheet3.getLastRowNum() ; i++)
		{
			String value = sheet3.getRow(i).getCell(0).toString();
			if(value.equalsIgnoreCase("Trade Name/Alias"))
			{
				Tradename  = sheet3.getRow(i).getCell(1).toString();
				System.out.println("The TRADENAME from excel is : "+Tradename);
				Thread.sleep(3000);
				break;
			}
		}*/

		for(int a=0 ; a< comp.CompCreatnList.size() ; a++)
		{
			if(comp.CompCreatnList.get(a).getText().equalsIgnoreCase("Sony"))
			{
				Thread.sleep(5000);
				comp.CompListEdit.get(a).click();
				System.out.println("The company from company page is : "+comp.CompCreatnList.get(a).getText());
				softassert.assertEquals(Company, "Sony" , "The company is not Viewed Successfully & not present in company list");
				Thread.sleep(5000);
				break;
			}
		}

		System.out.println("The attribute is  : "+comp.Edit_AccGrp.getAttribute("readonly"));

		String GSTIN = null;
		for(int i=0 ; i <= sheet5.getLastRowNum() ; i++)

		{

			String value  = sheet5.getRow(i).getCell(companynames).toString();
			System.out.println("The value is : "+ value);

			if(value.equalsIgnoreCase("Account/Group Name"))
			{
				if(comp.Edit_AccGrp.getAttribute("readonly").equals("true"))
				{

					softassert.assertTrue(comp.Edit_AccGrp.getAttribute("readonly").contains("true"), "The test case did not pass");

				}

				else
				{
					comp.Edit_AccGrp.sendKeys("TestValidation");
					System.out.println(" Testcase Failed");
					softassert.fail("Field validation has failed");
				}

			}

			else if(value.equalsIgnoreCase("Company Name"))
			{

				if(comp.Edit_CompanyName.getAttribute("readonly").equals("true"))
				{

					softassert.assertTrue(comp.Edit_CompanyName.getAttribute("readonly").contains("true"), "The test case did not pass");

				}

				else
				{
					comp.Edit_CompanyName.sendKeys("TestValidation");
					System.out.println(" Testcase Failed");
					softassert.fail("Field validation has failed");
				}
				Thread.sleep(2000);

			}

			else if(value.equalsIgnoreCase("Turnover_2016_2017"))
			{
				String Turnover1  = sheet5.getRow(i).getCell(companytwo).toString();
				System.out.println("TURNOVER IS : "+Turnover1);
				Thread.sleep(4000);
				comp.Edit_TurnOver16_17.clear();
				Thread.sleep(4000);
				comp.Edit_TurnOver16_17.sendKeys(Turnover1);
				Thread.sleep(4000);
			}


			else if(value.equalsIgnoreCase("Registered Email-id"))
			{
				String Emailid  = sheet5.getRow(i).getCell(companytwo).toString();
				comp.Edit_Registered_Email_id.clear();
				Thread.sleep(2000);
				comp.Edit_Registered_Email_id.sendKeys(Emailid);
				Thread.sleep(3000);
			}

			else if(value.equalsIgnoreCase("Trade Name/Alias"))
			{
				Tradename  = sheet5.getRow(i).getCell(companytwo).toString();
				comp.Edit_TradeName.clear();
				Thread.sleep(2000);
				comp.Edit_TradeName.sendKeys(Tradename);
				Thread.sleep(2000);
			}

			else if(value.equals("State_Type"))
			{
				System.out.println("Entered State Loop");
				Thread.sleep(4000);
				String EditState = comp.Edit_State_Present.getText().trim();
				System.out.println("The state is : "+EditState);
				Thread.sleep(5000);
				String StateExcel  = sheet3.getRow(i).getCell(companytwo).toString();
				softassert.assertEquals(EditState, StateExcel , "State is non- editable");
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("GSTIN_Type"))
			{
				Thread.sleep(3000);
				System.out.println("The GSTIN is  : "+GSTIN);

				if(comp.Edit_GSTIN.getAttribute("readonly").equals("true"))
				{

					softassert.assertTrue(comp.Edit_GSTIN.getAttribute("readonly").contains("true"), "The test case did not pass");

				}

				else
				{
					comp.Edit_GSTIN.sendKeys("GSTINFAILED");
					System.out.println(" Testcase Failed");
					softassert.fail("Field validation has failed");
				}
				Thread.sleep(3000);
			}
			else if(value.equalsIgnoreCase("Pan_Type"))
			{
				if(comp.Edit_PAN.getAttribute("readonly").equals("true"))
				{

					softassert.assertTrue(comp.Edit_PAN.getAttribute("readonly").contains("true"), "The test case did not pass");
					softassert.assertAll();

				}

				else
				{
					comp.Edit_PAN.sendKeys("PANFAILED");
					System.out.println(" Testcase Failed");
					softassert.fail("Field validation has failed");
				}

				Thread.sleep(3000);
			}
			else if(value.equalsIgnoreCase("TurnoverType"))
			{
				Thread.sleep(2000);
				String TurnOver_AprilJune2017  = sheet5.getRow(i).getCell(companytwo).toString();
				Thread.sleep(2000);
				comp.Edit_TurnOver_AprilJune2017.clear();
				Thread.sleep(2000);
				comp.Edit_TurnOver_AprilJune2017.sendKeys(TurnOver_AprilJune2017);
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Registered_Mobile"))
			{
				String Mobile  = sheet5.getRow(i).getCell(companytwo).toString();
				Thread.sleep(3000);
				comp.Edit_Mobile.clear();
				Thread.sleep(3000);
				comp.Edit_Mobile.sendKeys(Mobile);
				Thread.sleep(3000);
			}

			else if(value.equals("Constitution_of_Business"))
			{
				System.out.println("Entered Constitution of Business");
				Thread.sleep(3000);

				if(comp.Edit_ConstOfBussiness.getAttribute("readonly").equals("true"))
				{
					softassert.assertTrue(comp.Edit_ConstOfBussiness.getAttribute("readonly").contains("true"), "The test case did not pass");

				}

				else
				{
					comp.Edit_ConstOfBussiness.sendKeys("FAILED");
					System.out.println(" Testcase Failed");
					softassert.fail("Field validation has failed");
				}
				Thread.sleep(2000);
			}
			else if(value.equals("City_District"))
			{
				System.out.println("Entered City/District loop");
				Thread.sleep(2000);
				comp.Edit_City_District_Dropdown.click();
				Thread.sleep(3000);

				for(int j=0 ; j<comp.Edit_ListofCity_District.size() ; j++)
				{

					if(comp.Edit_ListofCity_District.get(j).getText().equalsIgnoreCase("Pune"))
					{
						Thread.sleep(3000);
						comp.Edit_ListofCity_District.get(j).click();
						Thread.sleep(3000);
						break;
					}

				}
				Thread.sleep(2000);
			}
			else
			{
				System.out.println("Fail to take data from excel");
			}

		}	
		Thread.sleep(3000);
		comp.Edit_Address_Tab.click();
		Thread.sleep(4000);
		sheet5 = (XSSFSheet) wb.getSheetAt(4);
		for (int i =0 ; i< sheet5.getLastRowNum() ; i++)
		{
			if(i==12)
			{
				comp.Edit_address1.clear();
				Thread.sleep(3000);
				comp.Edit_address1.sendKeys(sheet5.getRow(i).getCell(companytwo).toString());
				Thread.sleep(3000);
			}

			if(i==13)
			{
				comp.Edit_address2.clear();
				Thread.sleep(3000);
				comp.Edit_address2.sendKeys(sheet5.getRow(i).getCell(companytwo).toString());
				Thread.sleep(3000);
			}

			if(i==14)
			{	
				comp.Edit_pincode_address.clear();
				Thread.sleep(3000);
				comp.Edit_pincode_address.sendKeys(sheet5.getRow(i).getCell(companytwo).toString());
				Thread.sleep(3000);
				break;
			}

		}

		Thread.sleep(5000);
		comp.Edit_dropdown_address.click();
		Thread.sleep(5000);
		comp.Edit_ListddlAddress.get(2).click();


		Thread.sleep(4000);
		comp.Edit_dropdown_Premises.click();
		Thread.sleep(2000);
		comp.Edit_ListddlPremises.get(2).click();


		Thread.sleep(2000);
		comp.Edit_dropdown_States.click();
		Thread.sleep(2000);
		comp.Edit_ListddlStates.get(9).click();


		Thread.sleep(2000);
		comp.Edit_dropdown_City.click();
		Thread.sleep(2000);
		comp.Edit_ListddlCity.get(0).click();


		Thread.sleep(2000);
		comp.Edit_dropdown_BussinessActvty.click();
		Thread.sleep(2000);
		comp.Edit_ListBussinessActvty.get(2).click();

		Thread.sleep(4000);
		comp.Edit_Address_next.click();
		Thread.sleep(3000);


		comp.Edit_GSTN_username.clear();
		Thread.sleep(2000);
		comp.Edit_GSTN_username.sendKeys(sheet5.getRow(15).getCell(companytwo).toString());
		Thread.sleep(3000);
		comp.Edit_GSTN_username1.click();
		Thread.sleep(3000);
		comp.Edit_GSTN_users.get(0).click();
		Thread.sleep(4000);
		softassert.assertTrue(comp.Edit_Name_GSTN.isDisplayed(), "The Name is not Displayed");
		softassert.assertTrue(comp.Edit_Number_GSTN.isDisplayed(), "The Number is not Displayed");
		softassert.assertTrue(comp.Edit_PAN_GSTN.isDisplayed(), "The PAN is not Displayed");
		Thread.sleep(2000);
		comp.Edit_GSTUser_Next_GSTN.click();
		Thread.sleep(3000);
		/*
		 * GSTR3B
		 */
		/*comp.Edit_GSTR3B.click();
		Thread.sleep(5000);
		comp.Edit_GSTR3B_march2018.get(0).click();
		Thread.sleep(5000);		*/
		comp.Edit_Next_GSTR3B.click();
		Thread.sleep(5000);

		if((comp.Edit_HSN_checkbox.isSelected() && comp.Edit_ITC_checkbox.isSelected()))
		{
			comp.Edit_HSN_checkbox.click();	
			Thread.sleep(2000);
			comp.Edit_ITC_checkbox.click();
			Thread.sleep(2000);
			softassert.assertFalse(comp.Edit_HSN_checkbox.isSelected(), "The CheckBox is not selected");
			softassert.assertFalse(comp.Edit_ITC_checkbox.isSelected(), "The CheckBox is not selected");
		}

		else if (!(comp.Edit_HSN_checkbox.isSelected() && comp.Edit_ITC_checkbox.isSelected()))
		{
			comp.Edit_HSN_checkbox.click();	
			Thread.sleep(2000);
			comp.Edit_ITC_checkbox.click();
			Thread.sleep(2000);
			softassert.assertTrue(comp.Edit_HSN_checkbox.isSelected(), "The CheckBox is not selected");
			softassert.assertTrue(comp.Edit_ITC_checkbox.isSelected(), "The CheckBox is not selected");

		}

		comp.Edit_Save_AddCompany.click();
		Thread.sleep(8000);

		System.out.println("the msg : "+comp.CompanyUpdated_Success.getText());
		softassert.assertEquals(comp.CompanyUpdated_Success.getText(), "Company Updated successfully.." , "The company successfully created is not visible");
		Thread.sleep(4000);
		comp.Edit_OK.click();
		Thread.sleep(6000);

		softassert.assertAll();

	}


	public void VerifyCompanyEdited02() throws Exception
	{
		int companytwo =2;
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet5 = (XSSFSheet) wb.getSheetAt(4);

		String Tradename = null;
		for(int i=0 ; i <= sheet5.getLastRowNum() ; i++)
		{
			String value = sheet5.getRow(i).getCell(companynames).toString();
			if(value.equalsIgnoreCase("Trade Name/Alias"))
			{
				Tradename  = sheet5.getRow(i).getCell(companytwo).toString();
				System.out.println("The company name from excel is : "+Tradename);
				Thread.sleep(2000);
				break;
			}
		}
		int Value = comp.CompCreatnList.size();
		String Company =  comp.CompCreatnList.get(Value-1).getText().trim();
		softassert.assertEquals(Company, Tradename , "The company is not created Successfully & not present in list");
		softassert.assertAll();
	}

	public void ViewEditedCompany02() throws Exception
	{
		int companytwo =2;
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet2 = (XSSFSheet) wb.getSheetAt(1);
		sheet3 = (XSSFSheet) wb.getSheetAt(2);
		sheet4 = (XSSFSheet) wb.getSheetAt(3);
		sheet5 = (XSSFSheet) wb.getSheetAt(4);
		int Value = comp.CompCreatnList.size();
		String Company =  comp.CompCreatnList.get(Value-1).getText().trim();
		String Tradename = null;
		for(int i=0 ; i <= sheet5.getLastRowNum() ; i++)
		{
			String value = sheet5.getRow(i).getCell(companynames).toString();
			if(value.equalsIgnoreCase("Trade Name/Alias"))
			{
				Tradename  = sheet5.getRow(i).getCell(companytwo).toString();
				System.out.println("The TRADENAME from excel is : "+Tradename);
				Thread.sleep(3000);
				break;
			}

		}

		for(int a=0 ; a< comp.CompCreatnList.size() ; a++)
		{
			if(comp.CompCreatnList.get(a).getText().equalsIgnoreCase(Tradename))
			{
				Thread.sleep(5000);
				comp.CompCreatnListView.get(a).click();
				System.out.println("The company from company page is : "+comp.CompCreatnList.get(a).getText());
				softassert.assertEquals(Company, Tradename , "The company is not Viewed Successfully & not present in company list");
				Thread.sleep(5000);
				break;
			}
		}

		String GSTIN = null;
		for(int i=0 ; i <= sheet5.getLastRowNum() ; i++)

		{

			String value  = sheet5.getRow(i).getCell(companynames).toString();
			System.out.println("The value is : "+ value);

			if(value.equalsIgnoreCase("Account/Group Name"))
			{
				Thread.sleep(4000);
				String accnameGrp  = sheet5.getRow(i).getCell(companytwo).toString();
				System.out.println("The Acc Name is : "+accnameGrp);
				Thread.sleep(2000);
				System.out.println("The acc name of group from screen is : "+comp.View_Acc_Grp.getText());
				softassert.assertEquals(accnameGrp, comp.View_Acc_Grp.getText() , " The Account name does not match ");

			}

			else if(value.equalsIgnoreCase("Company Name"))
			{
				String companyName  = sheet5.getRow(i).getCell(companytwo).toString();
				softassert.assertEquals(companyName, comp.View_CompanyName.getText() , " The Company name does not match ");
				Thread.sleep(2000);
			}


			else if(value.equalsIgnoreCase("Turnover_2016_2017"))
			{
				String Turnover1  = sheet5.getRow(i).getCell(companytwo).toString();
				Thread.sleep(2000);
				softassert.assertEquals(Turnover1, comp.View_turnover.getText() , " The Turnover does not match ");
				Thread.sleep(2000);
			}


			else if(value.equalsIgnoreCase("Registered Email-id"))
			{
				String Emailid  = sheet5.getRow(i).getCell(companytwo).toString();
				Thread.sleep(2000);
				softassert.assertEquals(Emailid, comp.View_emailid.getText() , " The Email-ID does not match ");
				Thread.sleep(2000);
			}

			else if(value.equals("State_Type"))
			{
				System.out.println("Entered State Loop");
				String State  = sheet5.getRow(i).getCell(companytwo).toString();
				softassert.assertEquals(State, comp.View_State.getText() , " The State does not match ");
				Thread.sleep(2000);

			}

			else if(value.equalsIgnoreCase("GSTIN_Type"))
			{
				GSTIN  = sheet5.getRow(i).getCell(companytwo).toString();
				Thread.sleep(3000);
				System.out.println("The GSTIN is  : "+GSTIN);
				softassert.assertEquals(GSTIN, comp.View_GSTIN.getText() , " The GSTIN does not match ");
				Thread.sleep(2000);
			}
			else if(value.equalsIgnoreCase("Pan_Type"))
			{
				String Pan  = GSTIN.substring(2,12);
				Thread.sleep(2000);
				System.out.println("The PAN from GSTIN "+GSTIN+" is "+Pan);
				softassert.assertEquals(Pan, comp.View_Pan.getText() , " The PAN does not match ");
				Thread.sleep(2000);
			}
			else if(value.equalsIgnoreCase("TurnoverType"))
			{
				String TurnOver_AprilJune2017  = sheet5.getRow(i).getCell(companytwo).toString();
				softassert.assertEquals(TurnOver_AprilJune2017, comp.View_turnoverApr_June.getText() , " The TurnoverAprJune does not match ");
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Registered_Mobile"))
			{
				String Mobile  = sheet5.getRow(i).getCell(companytwo).toString();
				softassert.assertEquals(Mobile, comp.View_mobile.getText() , " The Mobile does not match ");
				Thread.sleep(3000);
			}

			else if(value.equals("Constitution_of_Business"))
			{
				System.out.println("Entered Constitution of Business");
				String ConsBussiness  = sheet5.getRow(i).getCell(companytwo).toString();
				softassert.assertEquals(ConsBussiness, comp.View_Constitution.getText() , " The Constitution of Bussiness does not match ");
				Thread.sleep(3000);



			}
			else if(value.equals("City_District"))
			{
				System.out.println("Entered City/District loop");
				Thread.sleep(5000);
				String City  = sheet5.getRow(i).getCell(companytwo).toString();
				softassert.assertEquals(City, comp.View_City.getText() , "The City does not match");
				Thread.sleep(2000);

			}

		}


		comp.Address_Tab.click();
		Thread.sleep(5000);
		for (int i =0 ; i< sheet5.getLastRowNum() ; i++)
		{
			if(i==12)
			{
				String Add1 = sheet5.getRow(i).getCell(companytwo).toString();
				softassert.assertEquals(Add1, comp.View_Add1.getText() , " The Address on 1st line did not match ");
				Thread.sleep(3000);
			}

			if(i==13)
			{
				String Add2 = sheet5.getRow(i).getCell(companytwo).toString();
				softassert.assertEquals(Add2, comp.View_Add2.getText() , " The Address on 2nd line did not match ");
				Thread.sleep(3000);
			}

			if(i==15)
			{
				String Pincode = sheet5.getRow(i).getCell(companytwo).toString();
				softassert.assertEquals(Pincode, comp.View_Pincode.getText() , " The Pincode does not match ");
				Thread.sleep(3000);
				break;
			}

		}

		softassert.assertEquals("Factory", comp.View_Addresstype.getText() , " The Address Type does not match ");
		softassert.assertEquals("Leased", comp.View_Premises.getText() , " The Premises does not match ");
		softassert.assertEquals("Delhi", comp.View_StateAddress.getText() , " The State does not match ");
		softassert.assertEquals("Delhi", comp.View_CityAddress.getText() , " The City does not match ");
		softassert.assertEquals("SEZ", comp.View_BA.getText() , " The Nature of Business Activity  does not match ");


		comp.View_GSTIN_User.click();
		Thread.sleep(3000);


		String GSTIN_username = sheet5.getRow(15).getCell(companytwo).toString();
		softassert.assertEquals(GSTIN_username , comp.View_GSTIN_Username.getText() , "The GSTIN username does not match");
		Thread.sleep(3000);

		GSTIN_Mail = sheet3.getRow(3).getCell(1).toString();
		softassert.assertEquals(GSTIN_Mail , comp.View_GSTIN_Email_ID.getText() , "The GSTIN EmailID does not match");
		Thread.sleep(3000);


		String GSTIN_FirstName = sheet2.getRow(3).getCell(1).toString();
		String GSTIN_LastName = sheet2.getRow(4).getCell(1).toString();
		System.out.println("The name from Excel is : "+GSTIN_FirstName);
		System.out.println("The name from Excel is : "+GSTIN_LastName);
		String FullName = GSTIN_FirstName+" "+GSTIN_LastName;
		System.out.println("The name from Excel is : "+FullName);
		System.out.println("The name from screen is  is : "+ comp.View_Name_GSTN.getText());
		softassert.assertEquals(FullName , comp.View_Name_GSTN.getText() , "The GSTIN Name does not match");
		Thread.sleep(3000);


		String GSTIN_Number = sheet2.getRow(1).getCell(1).toString();
		softassert.assertEquals(GSTIN_Number , comp.View_Number_GSTN.getText() , "The GSTIN Number does not match");
		Thread.sleep(3000);



		String GSTIN_Pan = sheet2.getRow(2).getCell(1).toString();
		softassert.assertEquals(GSTIN_Pan , comp.View_PAN_GSTN.getText() , "The GSTIN PAN does not match");
		Thread.sleep(3000);


		comp.View_GSTRR3B.click();
		/*	String Month = comp.View_Month_Year.getText();
		System.out.println("Month in GSTR3B is : "+Month);
		if(Month.equalsIgnoreCase("January"))
		{
			softassert.assertEquals("January 2018" , Month , "The GSTR3B month & year does not match");
		}
		else if (Month.contains("February") )
		{
			softassert.assertEquals("February 2018" , Month , "The GSTR3B month & year does not match");
		}
		else if (Month.contains("March") )
		{
			softassert.assertEquals("March 2018" , Month , "The GSTR3B month & year does not match");
		}
		else if (Month.contains("April") )
		{
			softassert.assertEquals("April 2018" , Month , "The GSTR3B month & year does not match");

		}	
		else if (Month.contains("May") )
		{
			softassert.assertEquals("May 2018" , Month , "The GSTR3B month & year does not match");
		}
		else if (Month.contains("June") )
		{
			softassert.assertEquals("June 2018" , Month , "The GSTR3B month & year does not match");
		}
		else if (Month.contains("July") )
		{
			softassert.assertEquals("July 2018" , Month , "The GSTR3B month & year does not match");
		}
		else if (Month.contains("August") )
		{
			softassert.assertEquals("August 2018" , Month , "The GSTR3B month & year does not match");
		}
		else if (Month.contains("September") )
		{
			softassert.assertEquals("September 2018" , Month , "The GSTR3B month & year does not match");
		}
		else if (Month.contains("October") )
		{
			softassert.assertEquals("October 2018" , Month , "The GSTR3B month & year does not match");
		}
		else if (Month.contains("November") )
		{
			softassert.assertEquals("November 2018" , Month , "The GSTR3B month & year does not match");
		}
		else if (Month.contains("December") )
		{
			softassert.assertEquals("December 2018" , Month , "The GSTR3B month & year does not match");
		}
		else
		{
			System.out.println("Month doesnt exist");
			softassert.fail("GSTR3b month does not exist");
		}
		softassert.assertEquals("Compile" , comp.View_Type.getText() , "The GSTR3B Type does not match");*/
		comp.View_OtherConfig.click();
		softassert.assertAll();
	}


	public void ViewEditOtherConfigDeselected02() throws Exception
	{
		Thread.sleep(2000);
		softassert.assertEquals("Yes" , comp.View_HSNSummary_Value.getText() , "The HSNSummary 'Yes' does not match");
		softassert.assertEquals("Yes" , comp.View_ITC_Value.getText() , "The ITC 'Yes' does not match");
		Thread.sleep(3000);
		comp.View_Close.click();
		softassert.assertAll();
	}

	public void AddLocation02() throws Exception
	{
		int locationtwo=2;
		softassert.assertEquals(comp.VerifyLocation.getText(), "Location(s)" , "The Location is not verified");
		Thread.sleep(3000);
		comp.AddLocation.click();
		Thread.sleep(3000);
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		XSSFSheet sheet6 = (XSSFSheet) wb.getSheetAt(5);
		String GSTIN = null;
		for(int i=0 ; i<sheet6.getLastRowNum() ; i++)

		{
			String value  = sheet6.getRow(i).getCell(companynames).toString();
			System.out.println("The value is : "+ value);
			Thread.sleep(2000);
			String LegalBussiness=null;
			if(value.equalsIgnoreCase("Company_Name"))
			{
				comp.SelectCompanyDDLforLoc1.click();
				for(int b =0 ; b<comp.ListOfCompanies.size() ; b++)
				{

					if(comp.ListOfCompanies.get(b).getText().equalsIgnoreCase(sheet6.getRow(i).getCell(locationtwo).toString()))
					{
						LegalBussiness = comp.ListOfCompanies.get(b).getText();
						comp.ListOfCompanies.get(b).click();
						Thread.sleep(4000);
						break;
					}

				}

			}

			else if(value.equalsIgnoreCase("LegalNameOfBussiness"))
			{
				if(comp.VerifyLegalBussinesName_Loc1.getAttribute("readonly").contains("true"))
				{
					softassert.assertTrue(comp.VerifyLegalBussinesName_Loc1.getAttribute("readonly").contains("true"), "This field is non editable");

				}
				else
				{
					softassert.fail("LegalNameOfBussiness is editable");
				}
				Thread.sleep(3000);
			}
			else if(value.equalsIgnoreCase("Turnover_2016_2017"))
			{
				String Loc_Turnover1  = sheet6.getRow(i).getCell(locationtwo).toString();
				comp.Loc1_TurnOver16_17.sendKeys(Loc_Turnover1);
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Registered_Email_id"))
			{
				String Loc_Emailid  = sheet6.getRow(i).getCell(locationtwo).toString();
				comp.Loc1_Registered_Email_id.sendKeys(Loc_Emailid);
				Thread.sleep(3000);
			}

			else if(value.equalsIgnoreCase("Trade Name/Alias"))
			{
				String Loc_Tradename  = sheet6.getRow(i).getCell(locationtwo).toString();
				comp.Loc1_TradeName.sendKeys(Loc_Tradename);
				Thread.sleep(2000);
			}

			else if(value.equals("State_Type"))
			{
				System.out.println("Entered State Loop");
				comp.Loc1_State_Dropdown.click();
				Thread.sleep(3000);
				for(int c=0 ; c<comp.Loc1_ListofStates.size() ; c++)
				{
					if(comp.Loc1_ListofStates.get(c).getText().equalsIgnoreCase(sheet6.getRow(i).getCell(locationtwo).toString()))
					{
						Thread.sleep(2000);
						comp.Loc1_ListofStates.get(c).click();
						Thread.sleep(2000);
						break;
					}

				}
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("GSTIN_Type"))
			{
				GSTIN  = sheet6.getRow(i).getCell(locationtwo).toString();
				System.out.println("The GSTIN is  : "+GSTIN);
				Thread.sleep(2000);
				comp.Loc1_GSTIN.sendKeys(GSTIN);
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Pan_Type"))
			{
				Thread.sleep(2000);
				softassert.assertTrue(comp.Loc1_PAN.isDisplayed(), "The Legal name is not displayed");

			}
			else if(value.equalsIgnoreCase("TurnoverType"))
			{
				Thread.sleep(2000);
				String Loc_TurnOver_AprilJune2017  = sheet6.getRow(i).getCell(locationtwo).toString();
				Thread.sleep(5000);
				comp.Loc1_TurnOver_AprilJune2017.sendKeys(Loc_TurnOver_AprilJune2017);
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Registered_Mobile"))
			{
				String Loc_Mobile  = sheet6.getRow(i).getCell(locationtwo).toString();
				Thread.sleep(3000);
				comp.Loc1_Mobile.sendKeys(Loc_Mobile);
				Thread.sleep(3000);
			}

			else if(value.equals("Constitution_of_Business"))
			{
				System.out.println("Entered Constitution of Business");
				softassert.assertTrue(comp.Loc1_ConstOfBussiness.isDisplayed(), "Constitution of Bussiness is not displayed");
				Thread.sleep(2000);
			}
			else if(value.equals("City_District"))
			{
				System.out.println("Entered City/District loop");
				comp.Loc1_City_District_Dropdown.click();
				Thread.sleep(4000);

				for(int j=0 ; j<comp.Loc2_ListofCity_District.size() ; j++)
				{
					if(comp.Loc2_ListofCity_District.get(j).getText().equalsIgnoreCase(sheet6.getRow(i).getCell(locationtwo).toString()))
					{
						System.out.println("Reached");
						Thread.sleep(8000);
						comp.Loc2_ListofCity_District.get(j).click();
						Thread.sleep(4000);
						break;
					}

				}

			}

			else
			{
				System.out.println("Fail to take data from excel");
			}

		}

		comp.Loc1_AddressTab.click();
		softassert.assertAll();

	}


	public void AddAddressLoc02() throws Exception 
	{
		int locationtwo=2;
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet6 = (XSSFSheet) wb.getSheetAt(5);
		for (int i =0 ; i< sheet6.getLastRowNum() ; i++)
		{
			if(i==12)
			{
				comp.Loc1_address1.sendKeys(sheet6.getRow(i).getCell(locationtwo).toString());
				Thread.sleep(2000);
			}

			if(i==13)
			{
				comp.Loc1_address2.sendKeys(sheet6.getRow(i).getCell(locationtwo).toString());
				Thread.sleep(2000);
			}

			if(i==14)
			{
				comp.Loc1_pincode_address.sendKeys(sheet6.getRow(i).getCell(locationtwo).toString());
				Thread.sleep(2000);
				break;
			}

		}

		comp.Loc1_dropdown_address.click();
		Thread.sleep(2000);
		comp.Loc1_ListddlAddress.get(3).click();


		Thread.sleep(2000);
		comp.Loc1_dropdown_Premises.click();
		Thread.sleep(2000);
		comp.Loc1_ListddlPremises.get(3).click();


		Thread.sleep(2000);
		comp.Loc1_dropdown_States.click();
		Thread.sleep(2000);
		comp.Loc1_ListddlStates.get(20).click();


		Thread.sleep(2000);
		comp.Loc1_dropdown_City.click();
		Thread.sleep(2000);
		comp.Loc1_ListddlCity.get(233).click();


		Thread.sleep(2000);
		comp.Loc1_dropdown_BussinessActvty.click();
		Thread.sleep(2000);
		comp.Loc1_ListBussinessActvty.get(3).click();

		Thread.sleep(3000);
		comp.Loc1_Next_to_GST_User.click();
		softassert.assertAll();

	}


	public void addGSTNLoc02() throws Exception

	{	
		int locationtwo=2;
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet6 = (XSSFSheet) wb.getSheetAt(5);
		Thread.sleep(3000);
		comp.Loc1_GSTN_username.sendKeys(sheet6.getRow(15).getCell(locationtwo).toString());
		Thread.sleep(2000);
		comp.Loc1_GSTN_username1.click();
		Thread.sleep(2000);
		comp.Loc1_GSTN_users.get(0).click();
		Thread.sleep(2000);
		softassert.assertTrue(comp.Loc1_Name_GSTN.isDisplayed(), "The Name of location 1 is not Displayed");
		softassert.assertTrue(comp.Loc1_Number_GSTN.isDisplayed(), "The Number of location 1 is not Displayed");
		softassert.assertTrue(comp.Loc1_PAN_GSTN.isDisplayed(), "The PAN of location 1 is not Displayed");
		comp.Loc1_Next_GSTR3B.click();
		Thread.sleep(2000);
		/*
		 * GSTR3B
		 */
		/*comp.Loc1_GSTR3B.click();
		Thread.sleep(2000);
		comp.Loc1_GSTR3B_march2018.get(1).click();
		Thread.sleep(2000);		*/
		comp.Loc1_Next_HSN.click();
		Thread.sleep(3000);
		softassert.assertAll();
	}

	public void ViewLocation02() throws Exception
	{
		int locationtwo =2;
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet6 = (XSSFSheet) wb.getSheetAt(5);
		sheet2 = (XSSFSheet) wb.getSheetAt(1);
		sheet3 = (XSSFSheet) wb.getSheetAt(2);
		int Value = comp.Loc_1_CreatnList.size();
		String Location =  comp.Loc_1_CreatnList.get(Value-1).getText().trim();

		String Tradename = null;
		for(int i=0 ; i <= sheet6.getLastRowNum() ; i++)
		{
			String value = sheet6.getRow(i).getCell(companynames).toString();
			if(value.equalsIgnoreCase("Trade Name/Alias"))
			{
				Tradename  = sheet6.getRow(i).getCell(locationtwo).toString();
				System.out.println("The TRADENAME from excel is : "+Tradename);
				Thread.sleep(3000);
				break;
			}
		}


		for(int a=0 ; a< comp.Loc_1_CreatnList.size() ; a++)
		{
			if(comp.Loc_1_CreatnList.get(a).getText().equalsIgnoreCase(Tradename))
			{

				Thread.sleep(5000);
				comp.Loc_1CreatnListView.get(a).click();
				System.out.println("The company from company page is : "+comp.Loc_1_CreatnList.get(a).getText());
				softassert.assertEquals(Location, Tradename , "The Location is not Viewed Successfully & not present in company list");
				softassert.assertAll();
				Thread.sleep(5000);
				break;
			}
		}
		String CompName = null;
		String GSTIN = null;
		for(int i=0 ; i <= sheet6.getLastRowNum() ; i++)

		{

			String value  = sheet6.getRow(i).getCell(companynames).toString();
			System.out.println("The value is : "+ value);

			if(value.equalsIgnoreCase("Company_Name"))
			{
				Thread.sleep(4000);
				CompName  = sheet6.getRow(i).getCell(locationtwo).toString();
				System.out.println("The Company from excel is : "+CompName);
				Thread.sleep(2000);
				System.out.println("The Company of group from screen is : "+comp.View_Loc1_Company.getText());
				softassert.assertEquals(CompName, comp.View_Loc1_Company.getText() , " The Company name does not match ");

			}

			else if(value.equalsIgnoreCase("LegalNameOfBussiness"))
			{

				softassert.assertEquals(CompName, comp.View_Loc1_bussiness.getText() , " The NameofBussiness  does not match ");
				Thread.sleep(2000);
			}


			else if(value.equalsIgnoreCase("Turnover_2016_2017"))
			{
				String Turnover1  = sheet6.getRow(i).getCell(locationtwo).toString();
				Thread.sleep(2000);
				softassert.assertEquals(Turnover1, comp.View_Loc1_turnover.getText() , " The Turnover does not match ");
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Registered_Email_id"))
			{
				String Emailid  = sheet6.getRow(i).getCell(locationtwo).toString();
				Thread.sleep(2000);
				softassert.assertEquals(Emailid, comp.View_Loc1_emailid.getText() , " The Email-ID does not match ");
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Trade Name/Alias"))
			{
				Tradename  = sheet6.getRow(i).getCell(1).toString();
				Thread.sleep(2000);
				softassert.assertEquals(Tradename, comp.View_Loc1_Tradename.getText() , " The TradeName  does not match ");
				Thread.sleep(2000);

			}

			else if(value.equals("State_Type"))
			{

				System.out.println("Entered State Loop");
				String State  = sheet6.getRow(i).getCell(1).toString();
				softassert.assertEquals(State, comp.View_Loc1_State.getText() , " The State does not match ");
				Thread.sleep(2000);

			}

			else if(value.equalsIgnoreCase("GSTIN_Type"))
			{
				GSTIN  = sheet6.getRow(i).getCell(1).toString();
				Thread.sleep(3000);
				System.out.println("The GSTIN is  : "+GSTIN);
				softassert.assertEquals(GSTIN, comp.View_GSTIN_Loc1_State.getText() , " The GSTIN does not match ");
				Thread.sleep(2000);
			}
			else if(value.equalsIgnoreCase("Pan_Type"))
			{
				String Pan  = GSTIN.substring(2,12);
				Thread.sleep(2000);
				System.out.println("The PAN from GSTIN "+GSTIN+" is "+Pan);
				softassert.assertEquals(Pan, comp.View_Pan_Loc1_State.getText() , " The PAN does not match ");
				Thread.sleep(2000);
			}
			else if(value.equalsIgnoreCase("TurnoverType"))
			{
				Thread.sleep(2000);
				String TurnOver_AprilJune2017  = sheet6.getRow(i).getCell(1).toString();
				softassert.assertEquals(TurnOver_AprilJune2017, comp.View_turnoverApr_Loc1_June.getText() , " The TurnoverAprJune does not match ");
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Registered_Mobile"))
			{
				String Mobile  = sheet6.getRow(i).getCell(1).toString();
				Thread.sleep(2000);
				softassert.assertEquals(Mobile, comp.View_Loc1_mobile.getText() , " The Mobile does not match ");
				Thread.sleep(3000);
			}

			else if(value.equals("Constitution_of_Business"))
			{
				Thread.sleep(3000);
				String ConsBussiness  = sheet6.getRow(i).getCell(1).toString();
				softassert.assertEquals(ConsBussiness, comp.View_Loc1_Constitution.getText() , " The Constitution of Bussiness does not match ");
				Thread.sleep(3000);

			}
			else if(value.equals("City_District"))
			{
				Thread.sleep(5000);
				String City  = sheet6.getRow(i).getCell(1).toString();
				softassert.assertEquals(City, comp.View_Loc1_City.getText() , "The City does not match");
				Thread.sleep(2000);

			}

		}

		Thread.sleep(4000);
		comp.View_Address_Loc1_Tab.click();
		Thread.sleep(5000);
		for (int i =0 ; i< sheet6.getLastRowNum(); i++)
		{

			if(sheet6.getRow(i).getCell(0).toString().equalsIgnoreCase("Address_Line_1"))
			{
				String Add1 = sheet6.getRow(i).getCell(1).toString();
				System.out.println(Add1);
				softassert.assertEquals(Add1, comp.View_Add1_Loc_1.getText() , " The Address on 1st line did not match ");
				Thread.sleep(3000);
			}

			if(sheet6.getRow(i).getCell(0).toString().equalsIgnoreCase("Address_Line_2"))
			{
				String Add2 = sheet6.getRow(i).getCell(1).toString();
				System.out.println(Add2);
				softassert.assertEquals(Add2, comp.View_Add2_Loc_1.getText() , " The Address on 2nd line did not match ");
				Thread.sleep(3000);
			}

			if(sheet6.getRow(i).getCell(0).toString().equalsIgnoreCase("Pincode"))
			{
				String Pincode = sheet6.getRow(i).getCell(1).toString();
				System.out.println(Pincode);
				softassert.assertEquals(Pincode, comp.View_Pincode_Loc_1.getText() , " The Pincode does not match ");
				Thread.sleep(3000);
				break;
			}


		}

		softassert.assertEquals("Registered Office",    comp.View_Addresstype_Loc_1.getText() , " The Address Type does not match ");
		softassert.assertEquals("Owned",	 comp.View_Premises_Loc_1.getText() , " The Premises does not match ");
		softassert.assertEquals("Maharashtra",	 comp.View_StateAddress_Loc_1.getText() , " The State does not match ");
		softassert.assertEquals("Mumbai",	 comp.View_CityAddress_Loc_1.getText() , " The City does not match ");
		softassert.assertEquals("Factory / Manufacturing", 	comp.View_BA_Loc_1.getText() , " The Nature of Business Activity  does not match ");



		comp.View_GSTIN_User_Loc_1.click();
		Thread.sleep(3000);


		String GSTIN_username = sheet6.getRow(15).getCell(1).toString();
		softassert.assertEquals(GSTIN_username , comp.View_GSTIN_Username_Loc_1.getText() , "The GSTIN username does not match");
		Thread.sleep(3000);

		String GSTIN_Mail_1 = sheet3.getRow(3).getCell(1).toString();
		System.out.println(GSTIN_Mail_1);
		softassert.assertEquals(GSTIN_Mail_1 , comp.View_GSTIN_Email_ID_Loc_1.getText() , "The GSTIN EmailID does not match");
		Thread.sleep(3000);


		String GSTIN_FirstName = sheet2.getRow(3).getCell(1).toString();
		String GSTIN_LastName = sheet2.getRow(4).getCell(1).toString();
		System.out.println("The name from Excel is : "+GSTIN_FirstName);
		System.out.println("The name from Excel is : "+GSTIN_LastName);
		String FullName = GSTIN_FirstName+" "+GSTIN_LastName;
		System.out.println("The name from Excel is : "+FullName);
		System.out.println("The name from screen is  is : "+ comp.View_Name_GSTN_Loc_1.getText());
		softassert.assertEquals(FullName , comp.View_Name_GSTN_Loc_1.getText() , "The GSTIN Name does not match");
		Thread.sleep(3000);


		String GSTIN_Number = sheet2.getRow(1).getCell(1).toString();
		softassert.assertEquals(GSTIN_Number , comp.View_Number_GSTN_Loc_1.getText() , "The GSTIN Number does not match");
		Thread.sleep(3000);



		String GSTIN_Pan = sheet2.getRow(2).getCell(1).toString();
		softassert.assertEquals(GSTIN_Pan , comp.View_PAN_GSTN_Loc_1.getText() , "The GSTIN PAN does not match");
		Thread.sleep(3000);


		comp.View_GSTRR3B_Loc_1.click();
		Thread.sleep(3000);
		comp.Loc_1_View_OtherConfig.click();
		softassert.assertAll();
	}


	public void AddCompany01_Neg() throws Exception 

	{
		int companyone_negative=3;
		comp.Addcompany.click();
		Thread.sleep(3000);
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet3 = (XSSFSheet) wb.getSheetAt(2);
		sheet2 = (XSSFSheet) wb.getSheetAt(1);
		System.out.println("The total row number is : "+sheet3.getLastRowNum());
		String GSTIN = null;
		for(int i=0 ; i <sheet3.getLastRowNum() ; i++)
		{
			String value  = sheet3.getRow(i).getCell(companynames).toString();
			System.out.println("The value is : "+ value);

			if(value.equalsIgnoreCase("Account/Group Name"))
			{
				System.out.println("Success");
			}

			else if(value.equalsIgnoreCase("Company Name"))
			{
				String companyName  = sheet3.getRow(i).getCell(companyone_negative).toString();
				comp.AddCompanyName.sendKeys(companyName);
				Thread.sleep(2000);
			}


			else if(value.equalsIgnoreCase("Turnover_2016_2017"))
			{
				String Turnover1  = sheet3.getRow(i).getCell(companyone_negative).toString();
				Thread.sleep(2000);
				comp.TurnOver16_17.sendKeys(Turnover1);
				Thread.sleep(2000);
			}


			else if(value.equalsIgnoreCase("Registered Email-id"))
			{
				String Emailid  = sheet3.getRow(i).getCell(companyone_negative).toString();
				Thread.sleep(2000);
				comp.Registered_Email_id.sendKeys(Emailid);
				Thread.sleep(3000);
			}


			else if(value.equalsIgnoreCase("Trade Name/Alias"))
			{
				String Tradename  = sheet3.getRow(i).getCell(companyone_negative).toString();
				comp.TradeName.sendKeys(Tradename);
				Thread.sleep(2000);
			}

			else if(value.equals("State_Type"))
			{
				System.out.println("Entered State Loop");
				Thread.sleep(4000);
				comp.State_Dropdown.click();
				Thread.sleep(5000);
				if(comp.Comp01_pleaseSelect_State.getText().equalsIgnoreCase(sheet3.getRow(i).getCell(companyone_negative).toString()))
				{
					Thread.sleep(5000);
					comp.Comp01_pleaseSelect_State.click();
					Thread.sleep(2000);
				}

			}
			else if(value.equalsIgnoreCase("GSTIN_Type"))
			{
				GSTIN  = sheet3.getRow(i).getCell(companyone_negative).toString();
				System.out.println(GSTIN);
				Thread.sleep(3000);
				System.out.println("The GSTIN is  : "+GSTIN);
				Thread.sleep(4000);
				comp.GSTIN.sendKeys(GSTIN);
				Thread.sleep(3000);
			}
			else if(value.equalsIgnoreCase("Pan_Type"))
			{
				comp.PAN.sendKeys(sheet3.getRow(i).getCell(companyone_negative).toString());
				Thread.sleep(3000);
			}
			else if(value.equalsIgnoreCase("TurnoverType"))
			{
				Thread.sleep(2000);
				String TurnOver_AprilJune2017  = sheet3.getRow(i).getCell(companyone_negative).toString();
				Thread.sleep(5000);
				comp.TurnOver_AprilJune2017.sendKeys(TurnOver_AprilJune2017);
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Registered_Mobile"))
			{
				String Mobile  = sheet3.getRow(i).getCell(companyone_negative).toString();
				Thread.sleep(3000);
				comp.Mobile.sendKeys(Mobile);
				Thread.sleep(3000);
			}

			else if(value.equals("Constitution_of_Business"))
			{
				System.out.println("Entered Constitution of Business");
				Thread.sleep(3000);
				comp.ConstOfBussiness.click();
				Thread.sleep(3000);
				if(comp.Comp01_pleaseSelect_ConstBussiness.getText().equalsIgnoreCase(sheet3.getRow(i).getCell(companyone_negative).toString()))
				{
					Thread.sleep(5000);
					comp.Comp01_pleaseSelect_ConstBussiness.click();
					Thread.sleep(3000);
					break;
				}


				Thread.sleep(2000);
			}
			else if(value.equals("City_District"))
			{
				if(comp.City_District_Dropdown_stateinactive.getAttribute("class").contains("disabled"))
				{
					softassert.assertTrue(comp.City_District_Dropdown_stateinactive.getAttribute("class").contains("disabled"), "State dropdwon inactive - success");
				}
				else
				{
					softassert.fail("State dropdown is active , test case fail");
				}
			}
			else
			{
				System.out.println("Fail to take data from excel");
			}

		}
		softassert.assertAll();
	}



	public void AddAddress01_Neg() throws Exception 
	{
		int companyone_negative=3;
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		int compadd=2;
		comp.Next.click();
		Thread.sleep(3000);
		comp.NextRegistration.click();
		Thread.sleep(3000);
		sheet4 = (XSSFSheet) wb.getSheetAt(3);
		for (int i =0 ; i< sheet4.getLastRowNum() ; i++)
		{
			if(i==0)
			{
				comp.address1.sendKeys(sheet4.getRow(i).getCell(companyone_negative).toString());
				Thread.sleep(3000);
			}

			if(i==1)
			{
				comp.address2.sendKeys(sheet4.getRow(i).getCell(companyone_negative).toString());
				Thread.sleep(3000);
			}

			if(i==2)
			{
				comp.pincode_address.sendKeys(sheet4.getRow(i).getCell(companyone_negative).toString());
				Thread.sleep(3000);
				break;
			}

		}

		Thread.sleep(4000);
		comp.Next_Address.click();
		Thread.sleep(3000);
		softassert.assertAll();

	}

	public void addGSTN01_Neg() throws Exception
	{
		int companyone_negative=3; 
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet4 = (XSSFSheet) wb.getSheetAt(3);
		Thread.sleep(3000);
		comp.GSTN_username.sendKeys(sheet4.getRow(3).getCell(companyone_negative).toString());
		Thread.sleep(3000);
		comp.Next_GSTN.click();
		Thread.sleep(3000);
		/*
		 * GSTR3B
		 */
		/*
		 comp.GSTR3B.click();
		Thread.sleep(5000);
		comp.GSTR3B_march2018.get(1).click();
		Thread.sleep(5000);		*/
		comp.Next_GSTR3B.click();
		Thread.sleep(5000);
		softassert.assertAll();
	}


	public void HSN_ITC01_Neg() throws Exception
	{

		if(comp.HSN_checkbox.isSelected())
		{

			softassert.assertTrue(comp.HSN_checkbox.isSelected(), "The CheckBox is not selected");

		}

		else
		{
			softassert.assertFalse(comp.HSN_checkbox.isSelected(), "The CheckBox is not selected");

		}

		if(comp.ITC_checkbox.isSelected())
		{
			softassert.assertTrue(comp.ITC_checkbox.isSelected(), "The CheckBox is not selected");

		}

		else
		{
			softassert.assertFalse(comp.ITC_checkbox.isSelected(), "The CheckBox is not selected");

		}


		softassert.assertAll();
	}


	public void SaveComp_Neg() throws Exception
	{
		Thread.sleep(2000);
		comp.Save_AddCompany.click();
		Thread.sleep(2000);
		softassert.assertEquals(comp.Save_indicatesError_comp.getText().trim(), "- Indicates error" ,  "Verify indicates error is not present on Add Location ");
		softassert.assertAll();

	}


	public void Check_Errors_01() throws Exception
	{
		comp.error_1.click();

		Thread.sleep(4000);

		softassert.assertEquals(comp.CompName_ErrorMsg.getText().trim(), "Please enter a valid Name" , "CompanyName error msg is not present");

		softassert.assertEquals(comp.Turnover_ErrorMsg.getText().trim(), "Please enter an amount of max 15 digits" , "TurnOver error msg is not present");

		softassert.assertEquals(comp.Email_ErrorMsg.getText().trim(), "Please enter a valid email" , "Email error msg is not present");

		softassert.assertEquals(comp.TradeName_ErrorMsg.getText().trim(), "Please enter a valid Trade name" , "Trade name error msg is not present");

		softassert.assertEquals(comp.State_ErrorMsg.getText().trim(), "State is mandatory" , "CompanyName error msg is not present");

		softassert.assertEquals(comp.GSTIN_ErrorMsg.getText().trim(), "Invalid GSTIN" , "GSTIN error msg is not present");

		softassert.assertEquals(comp.PAN_ErrorMsg.getText().trim(), "Please enter a valid PAN" , "PAN error msg is not present");

		softassert.assertEquals(comp.Turnover_2017__ErrorMsg.getText().trim(), "Please enter an amount of max 15 digits" , "TurnOver2017 error msg is not present");

		softassert.assertEquals(comp.Registered_Mobile_ErrorMsg.getText().trim(), "Please enter a valid mobile" , "Mobile error msg is not present");

		softassert.assertEquals(comp.ConstBussiness_ErrorMsg.getText().trim(), "Constitution of Business is mandatory" , "ConstBussiness error msg is not present");

		softassert.assertAll();
		Thread.sleep(3000);

	}


	public void Check_Errors_02() throws Exception
	{

		comp.error_3.click();

		Thread.sleep(5000);

		softassert.assertEquals(comp.AddLine1_ErrorMsg.getText().trim(), "Please enter a valid building/flat no." , "Address 1 error msg is not present");

		softassert.assertEquals(comp.AddLine2_ErrorMsg.getText().trim(), "Please enter a valid floor no." , "Address 2 error msg is not present");

		softassert.assertEquals(comp.AddressType_ErrorMsg.getText().trim(), "Address Type is mandatory" , "Address Type Dropdown error msg is not present");

		softassert.assertEquals(comp.PinCode_ErrorMsg.getText().trim(), "Please enter a valid PIN code" , "PinCode error msg is not present");

		softassert.assertEquals(comp.AddState_ErrorMsg.getText().trim(), "State is mandatory" , "State error msg of Address Sub Tab is not present");

		softassert.assertEquals(comp.NatureofBA_ErrorMsg.getText().trim(), "This field is mandatory" , "Nature of BA error msg is not present");

		softassert.assertAll();
	}

	public void Check_Errors_03() throws Exception
	{

		comp.error_4.click();

		Thread.sleep(4000);

		softassert.assertEquals(comp.GSTIN_Username_ErrorMsg.getText().trim(), "Please enter a valid Name" , "GSTIN_Username  error msg is not present");

		softassert.assertAll();
	}

	public void Check_Errors_04() throws Exception
	{

		comp.error_5.click();

		Thread.sleep(4000);


		for(int i =0 ; i < comp.List_months_GSTR3B_ErrorMsg.size() ; i++)
		{

			softassert.assertEquals(comp.List_months_GSTR3B_ErrorMsg.get(i).getText().trim(), "This field is mandatory" , "GSTR3B error msg is not present");

		}

		softassert.assertAll();	
	}



	public void AddLocation01_Neg() throws Exception
	{
		int locadd=3;
		softassert.assertEquals(comp.VerifyLocation.getText(), "Location(s)" , "The Location is not verified");
		Thread.sleep(3000);
		comp.AddLocation.click();
		Thread.sleep(3000);
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		XSSFSheet sheet6 = (XSSFSheet) wb.getSheetAt(5);
		String GSTIN = null;
		for(int i=0 ; i<sheet6.getLastRowNum() ; i++)

		{
			String value  = sheet6.getRow(i).getCell(companynames).toString();
			System.out.println("The value is : "+ value);

			Thread.sleep(2000);
			String LegalBussiness=null;
			if(value.equalsIgnoreCase("Company_Name"))
			{
				comp.SelectCompanyDDLforLoc1.click();
				for(int b =0 ; b<comp.ListOfCompanies.size() ; b++)
				{
					if(comp.ListOfCompanies.get(b).getText().equalsIgnoreCase(sheet6.getRow(i).getCell(locadd).toString()))
					{
						LegalBussiness = comp.ListOfCompanies.get(b).getText();
						comp.ListOfCompanies.get(b).click();
						Thread.sleep(4000);
						break;
					}
				}

			}

			else if(value.equalsIgnoreCase("LegalNameOfBussiness"))
			{

				if(comp.VerifyLegalBussinesName_Loc1.getAttribute("readonly").contains("true"))
				{
					softassert.assertTrue(comp.VerifyLegalBussinesName_Loc1.getAttribute("readonly").contains("true"), "This field is non editable");

				}
				else
				{
					softassert.fail("LegalNameOfBussiness is editable");
				}

				Thread.sleep(2000);
			}
			else if(value.equalsIgnoreCase("Turnover_2016_2017"))
			{
				String Loc_Turnover1  = sheet6.getRow(i).getCell(locadd).toString();
				comp.Loc1_TurnOver16_17.sendKeys(Loc_Turnover1);
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Registered_Email_id"))
			{
				String Loc_Emailid  = sheet6.getRow(i).getCell(locadd).toString();
				comp.Loc1_Registered_Email_id.sendKeys(Loc_Emailid);
				Thread.sleep(3000);
			}

			else if(value.equalsIgnoreCase("Trade Name/Alias"))
			{
				String Loc_Tradename  = sheet6.getRow(i).getCell(locadd).toString();
				comp.Loc1_TradeName.sendKeys(Loc_Tradename);
				Thread.sleep(2000);
			}

			else if(value.equals("State_Type"))
			{
				System.out.println("Entered State Loop");
				comp.Loc1_State_Dropdown.click();
				Thread.sleep(3000);

				if(comp.Loc01_State_PleaseSelect.getText().equalsIgnoreCase(sheet6.getRow(i).getCell(locadd).toString()))
				{
					Thread.sleep(2000);
					comp.Loc01_State_PleaseSelect.click();
					Thread.sleep(2000);

				}

			}

			else if(value.equalsIgnoreCase("GSTIN_Type"))
			{
				GSTIN  = sheet6.getRow(i).getCell(locadd).toString();
				System.out.println("The GSTIN is  : "+GSTIN);
				Thread.sleep(2000);
				comp.Loc1_GSTIN.sendKeys(GSTIN);
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Pan_Type"))
			{
				Thread.sleep(2000);
				softassert.assertTrue(comp.Loc1_PAN.isDisplayed(), "The Legal name is not displayed");

			}
			else if(value.equalsIgnoreCase("TurnoverType"))
			{
				Thread.sleep(2000);
				String Loc_TurnOver_AprilJune2017  = sheet6.getRow(i).getCell(locadd).toString();
				Thread.sleep(5000);
				comp.Loc1_TurnOver_AprilJune2017.sendKeys(Loc_TurnOver_AprilJune2017);
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Registered_Mobile"))
			{
				String Loc_Mobile  = sheet6.getRow(i).getCell(locadd).toString();
				Thread.sleep(3000);
				comp.Loc1_Mobile.sendKeys(Loc_Mobile);
				Thread.sleep(3000);
			}

			else if(value.equals("Constitution_of_Business"))
			{
				System.out.println("Entered Constitution of Business");
				softassert.assertTrue(comp.Loc1_ConstOfBussiness.isDisplayed(), "Constitution of Bussiness is not displayed");
				Thread.sleep(2000);
			}
			else if(value.equals("City_District"))
			{
				System.out.println("Entered City/District loop");
				{

					if(comp.City_District_Dropdown_stateinactive.getAttribute("class").contains("disabled"))
					{
						softassert.assertTrue(comp.City_District_Dropdown_stateinactive.getAttribute("class").contains("disabled"), "State dropdwon inactive - success");
					}

					else

					{
						softassert.fail("State dropdown is active , test case fail");
					}

				}

			}

			else
			{
				System.out.println("Fail to take data from excel");
			}

		}

		comp.Loc1_AddressTab.click();
		softassert.assertAll();
	}

	public void AddAddressLoc01_Neg() throws Exception 
	{
		int locadd=3;
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet6 = (XSSFSheet) wb.getSheetAt(5);
		for (int i =0 ; i< sheet6.getLastRowNum() ; i++)
		{
			if(i==12)
			{
				comp.Loc1_address1.sendKeys(sheet6.getRow(i).getCell(locadd).toString());
				Thread.sleep(2000);
			}

			if(i==13)
			{
				comp.Loc1_address2.sendKeys(sheet6.getRow(i).getCell(locadd).toString());
				Thread.sleep(2000);
			}

			if(i==14)
			{
				comp.Loc1_pincode_address.sendKeys(sheet6.getRow(i).getCell(locadd).toString());
				Thread.sleep(2000);
				break;
			}

		}

		Thread.sleep(3000);
		comp.Loc1_Next_to_GST_User.click();
		softassert.assertAll();

	}


	public void addGSTNLoc01_Neg() throws Exception

	{
		int loc =3 ;
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet6 = (XSSFSheet) wb.getSheetAt(5);
		Thread.sleep(3000);
		comp.Loc1_GSTN_username.sendKeys(sheet6.getRow(15).getCell(loc).toString());
		Thread.sleep(2000);
		comp.Loc1_Next_GSTR3B.click();
		Thread.sleep(3000);
		comp.Loc1_Next_HSN.click();
		Thread.sleep(3000);
		softassert.assertAll();
	}


	public void HSN_ITC_Loc01_Neg() throws Exception
	{

		if(comp.Loc1_HSN_checkbox.isSelected())
		{

			softassert.assertTrue(comp.Loc1_HSN_checkbox.isSelected(), "1.The CheckBox is not selected");

		}

		else
		{
			softassert.assertFalse(comp.Loc1_HSN_checkbox.isSelected(), "2.The CheckBox is not selected");

		}



		if(comp.Loc1_ITC_checkbox.isSelected())
		{
			softassert.assertTrue(comp.Loc1_ITC_checkbox.isSelected(), "3.The CheckBox is not selected");

		}

		else
		{
			softassert.assertFalse(comp.Loc1_ITC_checkbox.isSelected(), "4.The CheckBox is not selected");

		}
		softassert.assertAll();

	}


	public void Save_Loc01_Neg() throws Exception
	{

		Thread.sleep(4000);
		comp.Save_AddLoc_1.click();
		Thread.sleep(5000);
		softassert.assertEquals(comp.Save_indicatesError_Loc01.getText().trim(), "- Indicates error" ,  "Verify indicates error is not present on Add Company");
		Thread.sleep(4000);
		softassert.assertAll();
	}

	public void Check_LOC_01_Errors_01() throws Exception
	{
		comp.error_1_LOC01.click();

		Thread.sleep(4000);

		softassert.assertEquals(comp.Loc01_Turnover_ErrorMsg.getText().trim(), "Please enter an amount of max 15 digits" , "Loc01 TurnOver error msg is not present");

		softassert.assertEquals(comp.Loc01_Email_ErrorMsg.getText().trim(), "Invalid email" , "Loc01 Email error msg is not present");

		softassert.assertEquals(comp.Loc01_TradeName_ErrorMsg.getText().trim(), "Invalid Trade name" , "Loc01 Trade name error msg is not present");

		softassert.assertEquals(comp.Loc01_State_ErrorMsg.getText().trim(), "State is mandatory" , "Loc01 State error msg is not present");

		softassert.assertEquals(comp.Loc01_GSTIN_ErrorMsg.getText().trim(), "Invalid GSTIN" , "Loc01 GSTIN error msg is not present");

		softassert.assertEquals(comp.Loc01_TurnoverApril2017_ErrorMsg.getText().trim(), "Please enter an amount of max 15 digits" , "Loc01 TurnOver2017 error msg is not present");

		softassert.assertEquals(comp.Loc01_RegisteredMobile_ErrorMsg.getText().trim(), "Invalid mobile no" , "Loc01 Mobile error msg is not present");

		softassert.assertAll();
		Thread.sleep(3000);

	}


	public void Check_LOC_01_Errors_02() throws Exception
	{
		comp.error_3_LOC01.click();

		Thread.sleep(5000);

		softassert.assertEquals(comp.AddLine1_Loc01_ErrorMsg.getText().trim(), "Please enter a valid building/flat no." , "Loc01 Address 1 error msg is not present");

		softassert.assertEquals(comp.AddLine2_Loc01_ErrorMsg.getText().trim(), "Please enter a valid floor no." , "Loc01 Address 2 error msg is not present");

		softassert.assertEquals(comp.AddressType_Loc01_ErrorMsg.getText().trim(), "Address Type is mandatory" , "Loc01 Address Type Dropdown error msg is not present");

		softassert.assertEquals(comp.PinCode_Loc01_ErrorMsg.getText().trim(), "Please enter a valid PIN code" , "Loc01 PinCode error msg is not present");

		softassert.assertEquals(comp.AddState_Loc01_ErrorMsg.getText().trim(), "State is mandatory" , "Loc01 State error msg of Address Sub Tab is not present");

		softassert.assertEquals(comp.NatureofBA_Loc01_ErrorMsg.getText().trim(), "This field is mandatory" , "Loc01 Nature of BA error msg is not present");

		softassert.assertAll();
	}

	public void Check_LOC_01_Errors_03() throws Exception
	{

		comp.error_4_LOC01.click();

		Thread.sleep(4000);

		softassert.assertEquals(comp.GSTIN_Username_Loc01_ErrorMsg.getText().trim(), "Please enter a valid Name" , "Loc01 GSTIN_Username  error msg is not present");

		softassert.assertAll();
	}

	public void Check_LOC_01_Errors_04() throws Exception
	{

		comp.error_5_LOC01.click();

		Thread.sleep(4000);


		for(int i =0 ; i < comp.List_months_Loc01_GSTR3B_ErrorMsg.size() ; i++)
		{

			softassert.assertEquals(comp.List_months_Loc01_GSTR3B_ErrorMsg.get(i).getText().trim(), "This field is mandatory" , "LOC01 GSTR3B error msg is not present");

		}

		softassert.assertAll();	
	}


	public void Comp_NavigateOtherConfig_Neg02() throws Exception
	{
		Thread.sleep(2000);
		comp.Addcompany.click();
		Thread.sleep(3000);
		comp.OtherConfig_Comp_Neg_02.click();
		Thread.sleep(3000);
	}


	public void Check_Mandatory_Errors01() throws Exception
	{
		comp.error_1.click();

		Thread.sleep(4000);

		softassert.assertEquals(comp.CompName_ErrorMsg.getText().trim(), "Business Name is mandatory" , "The mandatory CompanyName error msg is not present");

		softassert.assertEquals(comp.Turnover_ErrorMsg.getText().trim(), "Previous year turnover is mandatory" , "The mandatory TurnOver error msg is not present");

		softassert.assertEquals(comp.Email_ErrorMsg.getText().trim(), "Email is mandatory" , "The mandatory Email error msg is not present");

		softassert.assertEquals(comp.TradeName_ErrorMsg.getText().trim(), "Trade name is mandatory" , "The mandatory Trade name error msg is not present");

		softassert.assertEquals(comp.State_ErrorMsg.getText().trim(), "State is mandatory" , "The mandatory CompanyName error msg is not present");

		softassert.assertEquals(comp.GSTIN_ErrorMsg.getText().trim(), "Invalid GSTIN" , "The mandatory GSTIN error msg is not present");

		softassert.assertEquals(comp.PAN_ErrorMsg.getText().trim(), "PAN is mandatory" , "The mandatory PAN error msg is not present");

		softassert.assertEquals(comp.Turnover_2017__ErrorMsg.getText().trim(), "Previous month turnover is mandatory" , "The mandatory TurnOver2017 error msg is not present");

		softassert.assertEquals(comp.Registered_Mobile_ErrorMsg.getText().trim(), "Mobile no is mandatory" , "The mandatory Mobile error msg is not present");

		softassert.assertEquals(comp.ConstBussiness_ErrorMsg.getText().trim(), "Constitution of Business is mandatory" , "The mandatory ConstBussiness error msg is not present");

		softassert.assertAll();

		Thread.sleep(3000);

	}


	public void Check_Mandatory_Errors_02() throws Exception
	{

		comp.error_3.click();

		Thread.sleep(5000);

		softassert.assertEquals(comp.AddLine1_ErrorMsg.getText().trim(), "Address Line 1 is mandatory" , "Mandatory for Address 1 error msg is not present");

		softassert.assertEquals(comp.AddLine2_ErrorMsg.getText().trim(), "Address Line 2 is mandatory" , "Mandatory for Address 2 error msg is not present");

		softassert.assertEquals(comp.AddressType_ErrorMsg.getText().trim(), "Address Type is mandatory" , "Mandatory for Address Type Dropdown error msg is not present");

		softassert.assertEquals(comp.PinCode_ErrorMsg.getText().trim(), "PIN Code is mandatory" , "Mandatory for PinCode error msg is not present");

		softassert.assertEquals(comp.AddState_ErrorMsg.getText().trim(), "State is mandatory" , "Mandatory for State error msg of Address Sub Tab is not present");

		softassert.assertEquals(comp.NatureofBA_ErrorMsg.getText().trim(), "This field is mandatory" , "Mandatory for Nature of BA error msg is not present");

		softassert.assertAll();
	}

	public void Check_Mandatory_Errors_03() throws Exception
	{

		comp.error_4.click();

		Thread.sleep(4000);

		softassert.assertEquals(comp.GSTIN_Username_ErrorMsg.getText().trim(), "User Name is mandatory" , "Mandatory msg for GSTIN_Username  error msg is not present");

		softassert.assertAll();
	}

	public void Check_Mandatory_Errors_04() throws Exception
	{

		comp.error_5.click();

		Thread.sleep(4000);


		for(int i =0 ; i < comp.List_months_GSTR3B_ErrorMsg.size() ; i++)
		{

			softassert.assertEquals(comp.List_months_GSTR3B_ErrorMsg.get(i).getText().trim(), "This field is mandatory" , "Mandatory msg for GSTR3B error msg is not present");

		}

		softassert.assertAll();	
	}



	public void Loc_NavigateOtherConfig_Neg02() throws Exception
	{
		Thread.sleep(2000);
		comp.AddLocation.click();
		Thread.sleep(3000);
		comp.OtherConfig_Loc_Neg_02.click();
		Thread.sleep(3000);
	}

	public void Mandatory_Check_LOC_01_Errors_01() throws Exception
	{
		comp.error_1_LOC01.click();

		Thread.sleep(4000);

		softassert.assertEquals(comp.Loc01_Company_MandatoryMsg.getText().trim(), "Company is mandatory" , "Mandatory msg Loc01 TurnOver error msg is not present");

		softassert.assertEquals(comp.Loc01_Turnover_ErrorMsg.getText().trim(), "Previous year turnover is mandatory" , "Mandatory msg Loc01 TurnOver error msg is not present");

		softassert.assertEquals(comp.Loc01_Email_ErrorMsg.getText().trim(), "Email is mandatory" , "Mandatory msg Loc01 Email error msg is not present");

		softassert.assertEquals(comp.Loc01_TradeName_ErrorMsg.getText().trim(), "Trade name is mandatory" , "Mandatory msg Loc01 Trade name error msg is not present");

		softassert.assertEquals(comp.Loc01_State_ErrorMsg.getText().trim(), "State is mandatory" , "Mandatory msg Loc01 State error msg is not present");

		softassert.assertEquals(comp.Loc01_GSTIN_ErrorMsg.getText().trim(), "Invalid GSTIN" , "Mandatory msg Loc01 GSTIN error msg is not present");

		softassert.assertEquals(comp.Loc01_TurnoverApril2017_ErrorMsg.getText().trim(), "Previous month turnover is mandatory" , "Mandatory msg Loc01 TurnOver2017 error msg is not present");

		softassert.assertEquals(comp.Loc01_RegisteredMobile_ErrorMsg.getText().trim(), "Mobile no is mandatory" , "Mandatory msg Loc01 Mobile error msg is not present");

		softassert.assertAll();

		Thread.sleep(3000);

	}


	public void Mandatory_Check_LOC_01_Errors_02() throws Exception
	{
		comp.error_3_LOC01.click();

		Thread.sleep(5000);

		softassert.assertEquals(comp.AddLine1_Loc01_ErrorMsg.getText().trim(), "Address Line 1 is mandatory" , "Mandatory msg Loc01 Address 1 error msg is not present");

		softassert.assertEquals(comp.AddLine2_Loc01_ErrorMsg.getText().trim(), "Address Line 2 is mandatory" , "Mandatory msg Loc01 Address 2 error msg is not present");

		softassert.assertEquals(comp.AddressType_Loc01_ErrorMsg.getText().trim(), "Address Type is mandatory" , "Mandatory msg Loc01 Address Type Dropdown error msg is not present");

		softassert.assertEquals(comp.PinCode_Loc01_ErrorMsg.getText().trim(), "PIN Code is mandatory" , "Mandatory msg Loc01 PinCode error msg is not present");

		softassert.assertEquals(comp.AddState_Loc01_ErrorMsg.getText().trim(), "State is mandatory" , "Mandatory msg Loc01 State error msg of Address Sub Tab is not present");

		softassert.assertEquals(comp.NatureofBA_Loc01_ErrorMsg.getText().trim(), "This field is mandatory" , "Mandatory msg Loc01 Nature of BA error msg is not present");

		softassert.assertAll();
	}

	public void Mandatory_Check_LOC_01_Errors_03() throws Exception
	{

		comp.error_4_LOC01.click();

		Thread.sleep(4000);

		softassert.assertEquals(comp.GSTIN_Username_Loc01_ErrorMsg.getText().trim(), "User Name is mandatory" , "Mandatory msg Loc01 GSTIN_Username  error msg is not present");

		softassert.assertAll();
	}

	public void Mandatory_Check_LOC_01_Errors_04() throws Exception
	{

		comp.error_5_LOC01.click();

		Thread.sleep(4000);


		for(int i =0 ; i < comp.List_months_Loc01_GSTR3B_ErrorMsg.size() ; i++)
		{

			softassert.assertEquals(comp.List_months_Loc01_GSTR3B_ErrorMsg.get(i).getText().trim(), "This field is mandatory" , "Mandatory msg LOC01 GSTR3B error msg is not present");

		}

		softassert.assertAll();	
	}

	public void VerifyGSTNError_Company() throws Exception
	{
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		Thread.sleep(2000);
		comp.Save_AddCompany.click();
		Thread.sleep(5000);		
		softassert.assertEquals(comp.Comp01_Pan_exist_error.getText().trim(), "PAN number already exists. If you want use same PAN number create as Location!" , "PAN number ,GSTN already exist");
		softassert.assertAll();
	}

	public void AddCompany_PanError() throws Exception 

	{
		int companyone=4;
		comp.Addcompany.click();
		Thread.sleep(3000);
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet3 = (XSSFSheet) wb.getSheetAt(2);
		sheet2 = (XSSFSheet) wb.getSheetAt(1);
		System.out.println("The total row number is : "+sheet3.getLastRowNum());
		
		String GSTIN = null;
		for(int i=0 ; i <sheet3.getLastRowNum() ; i++)

		{

			String value  = sheet3.getRow(i).getCell(companynames).toString();
			System.out.println("The value is : "+ value);

			if(value.equalsIgnoreCase("Account/Group Name"))
			{
				Thread.sleep(2000);				
			}

			else if(value.equalsIgnoreCase("Company Name"))
			{
				String companyName  = sheet3.getRow(i).getCell(companyone).toString();
				comp.AddCompanyName.sendKeys(companyName);
				Thread.sleep(2000);
			}


			else if(value.equalsIgnoreCase("Turnover_2016_2017"))
			{
				String Turnover1  = sheet3.getRow(i).getCell(companyone).toString();
				Thread.sleep(2000);
				comp.TurnOver16_17.sendKeys(Turnover1);
				Thread.sleep(2000);
			}


			else if(value.equalsIgnoreCase("Registered Email-id"))
			{
				String Emailid  = sheet3.getRow(i).getCell(companyone).toString();
				Thread.sleep(2000);
				comp.Registered_Email_id.sendKeys(Emailid);
				Thread.sleep(3000);
			}

			else if(value.equalsIgnoreCase("Trade Name/Alias"))
			{
				String Tradename  = sheet3.getRow(i).getCell(companyone).toString();
				comp.TradeName.sendKeys(Tradename);
				Thread.sleep(2000);
			}

			else if(value.equals("State_Type"))
			{
				System.out.println("Entered State Loop");
				Thread.sleep(4000);
				comp.State_Dropdown.click();
				Thread.sleep(5000);

				for(int c=0 ; c<comp.ListofStates.size() ; c++)
				{
					if(comp.ListofStates.get(c).getText().equalsIgnoreCase("Maharashtra"))
					{
						Thread.sleep(5000);
						comp.ListofStates.get(c).click();
						Thread.sleep(8000);
						break;
					}

				}
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("GSTIN_Type"))
			{
				GSTIN  = sheet3.getRow(i).getCell(companyone).toString();
				Thread.sleep(3000);
				System.out.println("The GSTIN is  : "+GSTIN);
				Thread.sleep(4000);
				comp.GSTIN.sendKeys(GSTIN);
				Thread.sleep(3000);
			}
			else if(value.equalsIgnoreCase("Pan_Type"))
			{
				String Pan  = sheet3.getRow(i).getCell(companyone).toString();
				Thread.sleep(4000);
				System.out.println("The PAN from GSTIN "+GSTIN+" is "+Pan);
				comp.PAN.sendKeys(Pan);
				Thread.sleep(3000);
			}
			else if(value.equalsIgnoreCase("TurnoverType"))
			{
				Thread.sleep(2000);
				String TurnOver_AprilJune2017  = sheet3.getRow(i).getCell(companyone).toString();
				Thread.sleep(5000);
				comp.TurnOver_AprilJune2017.sendKeys(TurnOver_AprilJune2017);
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Registered_Mobile"))
			{
				String Mobile  = sheet3.getRow(i).getCell(companyone).toString();
				Thread.sleep(3000);
				comp.Mobile.sendKeys(Mobile);
				Thread.sleep(3000);
			}

			else if(value.equals("Constitution_of_Business"))
			{
				System.out.println("Entered Constitution of Business");
				Thread.sleep(3000);
				comp.ConstOfBussiness.click();
				Thread.sleep(3000);

				for(int a=0 ; a<comp.ListConstOfBussiness.size() ; a++)
				{
					Thread.sleep(3000);
					if(comp.ListConstOfBussiness.get(a).getText().equalsIgnoreCase(sheet3.getRow(i).getCell(companyone).toString()))
					{
						Thread.sleep(5000);
						comp.ListConstOfBussiness.get(a).click();
						Thread.sleep(3000);
						break;
					}

				}
				Thread.sleep(2000);
			}
			else if(value.equals("City_District"))
			{
				System.out.println("Entered City/District loop");
				Thread.sleep(3000);
				comp.City_District_Dropdown.click();
				Thread.sleep(3000);

				for(int j=0 ; j<comp.ListofCity_District.size() ; j++)
				{
					if(comp.ListofCity_District.get(j).getText().equalsIgnoreCase(sheet3.getRow(i).getCell(companyone).toString()))
					{
						Thread.sleep(2000);
						comp.ListofCity_District.get(j).click();
						Thread.sleep(3000);
						break;
					}

				}
				Thread.sleep(2000);
				break;
			}
		}

		softassert.assertAll();
	}

	public void AddAddress_PanError() throws Exception 
	{
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		comp.Next.click();
		Thread.sleep(3000);
		comp.NextRegistration.click();
		Thread.sleep(3000);
		sheet4 = (XSSFSheet) wb.getSheetAt(3);
		for (int i =0 ; i< sheet4.getLastRowNum() ; i++)
		{
			if(i==0)
			{
				comp.address1.sendKeys(sheet4.getRow(i).getCell(1).toString());
				Thread.sleep(3000);
			}

			if(i==1)
			{
				comp.address2.sendKeys(sheet4.getRow(i).getCell(1).toString());
				Thread.sleep(3000);
			}

			if(i==2)
			{
				comp.pincode_address.sendKeys(sheet4.getRow(i).getCell(1).toString());
				Thread.sleep(3000);
				break;
			}

		}

		Thread.sleep(2000);
		comp.dropdown_address.click();
		Thread.sleep(2000);
		comp.ListddlAddress.get(0).click();


		Thread.sleep(2000);
		comp.dropdown_Premises.click();
		Thread.sleep(2000);
		comp.ListddlPremises.get(0).click();


		Thread.sleep(2000);
		comp.dropdown_States.click();
		Thread.sleep(2000);
		comp.ListddlStates.get(20).click();


		Thread.sleep(2000);
		comp.dropdown_City.click();
		Thread.sleep(2000);
		comp.ListddlCity.get(233).click();


		Thread.sleep(2000);
		comp.dropdown_BussinessActvty.click();
		Thread.sleep(2000);
		comp.ListBussinessActvty.get(0).click();

		Thread.sleep(4000);
		comp.Next_Address.click();
		Thread.sleep(3000);
		softassert.assertAll();

	}


	public void addGSTN_PanError() throws Exception

	{
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet4 = (XSSFSheet) wb.getSheetAt(3);
		comp.GSTN_username.sendKeys(sheet4.getRow(3).getCell(1).toString());
		Thread.sleep(3000);
		comp.GSTN_username1.click();
		Thread.sleep(3000);
		comp.GSTN_users.get(0).click();
		Thread.sleep(4000);
		softassert.assertTrue(comp.Name_GSTN.isDisplayed(), "The Name is not Displayed");
		softassert.assertTrue(comp.Number_GSTN.isDisplayed(), "The Number is not Displayed");
		softassert.assertTrue(comp.PAN_GSTN.isDisplayed(), "The PAN is not Displayed");
		Thread.sleep(2000);
		comp.Next_GSTN.click();
		Thread.sleep(3000);
		/*
		 * GSTR3B
		 */

		for(int i =0 ; i< comp.GSTR3B_List.size() ; i++)
		{

			Thread.sleep(3000);
			comp.GSTR3B_List.get(i).click();
			Thread.sleep(3000);
			comp.GSTR3B_Compile.get(i).click();
			Thread.sleep(3000);
		}

		Thread.sleep(3000);		
		comp.Next_GSTR3B.click();
		Thread.sleep(5000);
		softassert.assertAll();

	}

	public void HSN_ITC_PanError() throws Exception
	{

		if(comp.HSN_checkbox.isSelected())
		{

			softassert.assertTrue(comp.HSN_checkbox.isSelected(), "The CheckBox is not selected");

		}

		else
		{
			softassert.assertFalse(comp.HSN_checkbox.isSelected(), "The CheckBox is not selected");

		}

		if(comp.ITC_checkbox.isSelected())
		{
			softassert.assertTrue(comp.ITC_checkbox.isSelected(), "The CheckBox is not selected");

		}

		else
		{
			softassert.assertFalse(comp.ITC_checkbox.isSelected(), "The CheckBox is not selected");

		}

		softassert.assertAll();
	}


	public void VerifyPanError_Company() throws Exception
	{
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		Thread.sleep(2000);
		comp.Save_AddCompany.click();
		Thread.sleep(5000);		
		softassert.assertEquals(comp.Comp01_Pan_exist_error.getText().trim(), "GSTIN and PAN number do not match !" , "PAN number does not match with the GSTIN");
		softassert.assertAll();
	}
	
	
	public void AddCompany_MisMatch() throws Exception 

	{
		int companyone=5;
		comp.Addcompany.click();
		Thread.sleep(3000);
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet3 = (XSSFSheet) wb.getSheetAt(2);
		sheet2 = (XSSFSheet) wb.getSheetAt(1);
		System.out.println("The total row number is : "+sheet3.getLastRowNum());
		
		String GSTIN = null;
		for(int i=0 ; i <sheet3.getLastRowNum() ; i++)

		{

			String value  = sheet3.getRow(i).getCell(companynames).toString();
			System.out.println("The value is : "+ value);

			if(value.equalsIgnoreCase("Account/Group Name"))
			{
				Thread.sleep(2000);				
			}

			else if(value.equalsIgnoreCase("Company Name"))
			{
				String companyName  = sheet3.getRow(i).getCell(companyone).toString();
				comp.AddCompanyName.sendKeys(companyName);
				Thread.sleep(2000);
			}


			else if(value.equalsIgnoreCase("Turnover_2016_2017"))
			{
				String Turnover1  = sheet3.getRow(i).getCell(companyone).toString();
				Thread.sleep(2000);
				comp.TurnOver16_17.sendKeys(Turnover1);
				Thread.sleep(2000);
			}


			else if(value.equalsIgnoreCase("Registered Email-id"))
			{
				String Emailid  = sheet3.getRow(i).getCell(companyone).toString();
				Thread.sleep(2000);
				comp.Registered_Email_id.sendKeys(Emailid);
				Thread.sleep(3000);
			}

			else if(value.equalsIgnoreCase("Trade Name/Alias"))
			{
				String Tradename  = sheet3.getRow(i).getCell(companyone).toString();
				comp.TradeName.sendKeys(Tradename);
				Thread.sleep(2000);
			}

			else if(value.equals("State_Type"))
			{
				System.out.println("Entered State Loop");
				Thread.sleep(4000);
				comp.State_Dropdown.click();
				Thread.sleep(5000);

				for(int c=0 ; c<comp.ListofStates.size() ; c++)
				{
					if(comp.ListofStates.get(c).getText().equalsIgnoreCase(sheet3.getRow(i).getCell(companyone).toString()))
					{
						Thread.sleep(5000);
						comp.ListofStates.get(c).click();
						Thread.sleep(8000);
						break;
					}

				}
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("GSTIN_Type"))
			{
				GSTIN  = sheet3.getRow(i).getCell(companyone).toString();
				Thread.sleep(3000);
				System.out.println("The GSTIN is  : "+GSTIN);
				Thread.sleep(4000);
				comp.GSTIN.sendKeys(GSTIN);
				Thread.sleep(3000);
			}
			else if(value.equalsIgnoreCase("Pan_Type"))
			{
				String Pan  = sheet3.getRow(i).getCell(companyone).toString();
				Thread.sleep(4000);
				System.out.println("The PAN from GSTIN "+GSTIN+" is "+Pan);
				comp.PAN.sendKeys(Pan);
				Thread.sleep(3000);
			}
			else if(value.equalsIgnoreCase("TurnoverType"))
			{
				Thread.sleep(2000);
				String TurnOver_AprilJune2017  = sheet3.getRow(i).getCell(companyone).toString();
				Thread.sleep(5000);
				comp.TurnOver_AprilJune2017.sendKeys(TurnOver_AprilJune2017);
				Thread.sleep(2000);
			}

			else if(value.equalsIgnoreCase("Registered_Mobile"))
			{
				String Mobile  = sheet3.getRow(i).getCell(companyone).toString();
				Thread.sleep(3000);
				comp.Mobile.sendKeys(Mobile);
				Thread.sleep(3000);
			}

			else if(value.equals("Constitution_of_Business"))
			{
				System.out.println("Entered Constitution of Business");
				Thread.sleep(3000);
				comp.ConstOfBussiness.click();
				Thread.sleep(3000);

				for(int a=0 ; a<comp.ListConstOfBussiness.size() ; a++)
				{
					Thread.sleep(3000);
					if(comp.ListConstOfBussiness.get(a).getText().equalsIgnoreCase(sheet3.getRow(i).getCell(companyone).toString()))
					{
						Thread.sleep(5000);
						comp.ListConstOfBussiness.get(a).click();
						Thread.sleep(3000);
						break;
					}

				}
				Thread.sleep(2000);
			}
			else if(value.equals("City_District"))
			{
				System.out.println("Entered City/District loop");
				Thread.sleep(3000);
				comp.City_District_Dropdown.click();
				Thread.sleep(3000);

				for(int j=0 ; j<comp.ListofCity_District.size() ; j++)
				{
					if(comp.ListofCity_District.get(j).getText().equalsIgnoreCase(sheet3.getRow(i).getCell(companyone).toString()))
					{
						Thread.sleep(2000);
						comp.ListofCity_District.get(j).click();
						Thread.sleep(3000);
						break;
					}

				}
				Thread.sleep(2000);
				break;
			}
		}

		softassert.assertAll();
	}

	public void AddAddress_MisMatch() throws Exception 
	{
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		comp.Next.click();
		Thread.sleep(3000);
		comp.NextRegistration.click();
		Thread.sleep(3000);
		sheet4 = (XSSFSheet) wb.getSheetAt(3);
		for (int i =0 ; i< sheet4.getLastRowNum() ; i++)
		{
			if(i==0)
			{
				comp.address1.sendKeys(sheet4.getRow(i).getCell(1).toString());
				Thread.sleep(3000);
			}

			if(i==1)
			{
				comp.address2.sendKeys(sheet4.getRow(i).getCell(1).toString());
				Thread.sleep(3000);
			}

			if(i==2)
			{
				comp.pincode_address.sendKeys(sheet4.getRow(i).getCell(1).toString());
				Thread.sleep(3000);
				break;
			}

		}

		Thread.sleep(2000);
		comp.dropdown_address.click();
		Thread.sleep(2000);
		comp.ListddlAddress.get(0).click();


		Thread.sleep(2000);
		comp.dropdown_Premises.click();
		Thread.sleep(2000);
		comp.ListddlPremises.get(0).click();


		Thread.sleep(2000);
		comp.dropdown_States.click();
		Thread.sleep(2000);
		comp.ListddlStates.get(20).click();


		Thread.sleep(2000);
		comp.dropdown_City.click();
		Thread.sleep(2000);
		comp.ListddlCity.get(233).click();


		Thread.sleep(2000);
		comp.dropdown_BussinessActvty.click();
		Thread.sleep(2000);
		comp.ListBussinessActvty.get(0).click();

		Thread.sleep(4000);
		comp.Next_Address.click();
		Thread.sleep(3000);
		softassert.assertAll();

	}


	public void addGSTN_MisMatch() throws Exception

	{
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet4 = (XSSFSheet) wb.getSheetAt(3);
		comp.GSTN_username.sendKeys(sheet4.getRow(3).getCell(1).toString());
		Thread.sleep(3000);
		comp.GSTN_username1.click();
		Thread.sleep(3000);
		comp.GSTN_users.get(0).click();
		Thread.sleep(4000);
		softassert.assertTrue(comp.Name_GSTN.isDisplayed(), "The Name is not Displayed");
		softassert.assertTrue(comp.Number_GSTN.isDisplayed(), "The Number is not Displayed");
		softassert.assertTrue(comp.PAN_GSTN.isDisplayed(), "The PAN is not Displayed");
		Thread.sleep(2000);
		comp.Next_GSTN.click();
		Thread.sleep(3000);
		/*
		 * GSTR3B
		 */

		for(int i =0 ; i< comp.GSTR3B_List.size() ; i++)
		{

			Thread.sleep(3000);
			comp.GSTR3B_List.get(i).click();
			Thread.sleep(3000);
			comp.GSTR3B_Compile.get(i).click();
			Thread.sleep(3000);
		}

		Thread.sleep(3000);		
		comp.Next_GSTR3B.click();
		Thread.sleep(5000);
		softassert.assertAll();

	}

	public void HSN_ITC_MisMatch() throws Exception
	{

		if(comp.HSN_checkbox.isSelected())
		{

			softassert.assertTrue(comp.HSN_checkbox.isSelected(), "The CheckBox is not selected");

		}

		else
		{
			softassert.assertFalse(comp.HSN_checkbox.isSelected(), "The CheckBox is not selected");

		}

		if(comp.ITC_checkbox.isSelected())
		{
			softassert.assertTrue(comp.ITC_checkbox.isSelected(), "The CheckBox is not selected");

		}

		else
		{
			softassert.assertFalse(comp.ITC_checkbox.isSelected(), "The CheckBox is not selected");

		}

		softassert.assertAll();
	}


	public void VerifyMisMatch_Company() throws Exception
	{
		src = new File("C:\\ExcelData\\TestDataExcel.xlsx");
		fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		Thread.sleep(2000);
		comp.Save_AddCompany.click();
		Thread.sleep(5000);		
		softassert.assertEquals(comp.Comp01_Pan_exist_error.getText().trim(), "GSTIN and PAN number do not match !" , "PAN number does not match with the GSTIN");
		softassert.assertAll();
	}
}

