package Group;

import org.testng.annotations.Test;

public class TestCase2_CreateGroup extends MethodPage {

	@Test
	public void TestCase2() throws Exception
	{
		MethodPage mp = new MethodPage();

		//Login
		mp.Login();

		//Select Group from Sidebar
		mp.SelectGroup();

		//		//Click on Add Group button
		mp.AddGroup();
		//
		//		//Fill out details of basic user 
		mp.BasicInfo();
		//
		//Fill out details of client admin
		mp.adminUser();

		//Selection of records 
		mp.ValidateRecord("5000");

		//Click on Group Added
		mp.ViewGroup();

		//Verify the details for the added Group
		mp.VerifyViewGroup();

		softassert.assertAll();



















	}

}
